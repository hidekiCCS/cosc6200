//
// Large Scale Computing
// 1D Heat/Mass Transfer
// Solve for
// d2c/dx2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
//  Conjugate Gradient Method with Eigen3
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>


// main
int main(int argc, char **argv) {

  const int num = 7; // number of node points is 7
  const double a = 1; // phi at left end is 1
  const double b = 2; // phi at right end is 2
  const double d = 0.1; // Diffusion coefficient is 0.1

  double *phi = new double[num];
  // Initialize
  for (int n = 0; n < num ; n++) {
    phi[n] = 0.0;
  }
  phi[0] = a;
  phi[num-1] = b;
  
  // assuming a uniform gird
  double dx = 1.0 / (num - 1);

  // declare Sparse Matrix
  int size = num - 2; // size of inner points is 5
  Eigen::SparseMatrix<double> matA(size, size);
  // vector for RHS
  Eigen::VectorXd rhs(size);
  // vector for solution
  Eigen::VectorXd sol( size);

  // set up Matrix and RHS
  rhs(0) = -dx * dx / d - a;
  rhs(1) = -dx * dx / d;
  rhs(2) = -dx * dx / d;
  rhs(3) = -dx * dx / d;
  rhs(4) = -dx * dx / d - b;      
  matA.coeffRef(0, 0) = -2.0;
  matA.coeffRef(1, 1) = -2.0;
  matA.coeffRef(2, 2) = -2.0;
  matA.coeffRef(3, 3) = -2.0;
  matA.coeffRef(4, 4) = -2.0;
  matA.coeffRef(0, 1) = 1.0;
  matA.coeffRef(1, 2) = 1.0;
  matA.coeffRef(2, 3) = 1.0;
  matA.coeffRef(3, 4) = 1.0;
  matA.coeffRef(1, 0) = 1.0;
  matA.coeffRef(2, 1) = 1.0;
  matA.coeffRef(3, 2) = 1.0;
  matA.coeffRef(4, 3) = 1.0;

  // solve linear system with CG (Conjugate Gradient Method)
  const double tol = 1.0e-8;
  Eigen::ConjugateGradient < Eigen::SparseMatrix<double> > solver;
  solver.setTolerance(tol);
  solver.compute(matA);
  sol = solver.solve(rhs);

  std::cout << "#iterations:     " << solver.iterations() << std::endl;
  std::cout << "estimated error: " << solver.error() << std::endl;

  // restore solution
  for (int i = 1; i < num - 1; i++) {
    phi[i] = sol[i-1];
  }

  // Output Result
  std::ofstream ofile;
  ofile.open("result.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
    double x = static_cast<double>(i) / (num - 1);
    ofile << x << " " << phi[i] << std::endl;
  }
  ofile.close();

  std::cout << "Done\n";
  return 0;
}
