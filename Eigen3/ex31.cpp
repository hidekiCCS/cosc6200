// Eigen3 test
// ex31.cpp
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>

int main(int argc, char **argv) {
  Eigen::Matrix3d A;
  Eigen::Vector3d b;
  A << 1,2,3,  4,5,6,  7,8,10;
  b << 3, 3, 4;
  std::cout << "Here is the matrix A:\n" << A << std::endl;
  std::cout << "Here is the vector b:\n" << b << std::endl;
  Eigen::ColPivHouseholderQR<Eigen::Matrix3d> dec(A);
  Eigen::Vector3d x = dec.solve(b);
  std::cout << "The solution is:\n" << x << std::endl;

  return 0;
}
