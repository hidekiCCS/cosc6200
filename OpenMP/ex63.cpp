/*
  Large Scale Computing
  ex63.cpp : OMP sample program

  THIS PROGRAM RUNS on CPU
*/

#include <iostream>
#include <omp.h>

#define NN 16
#define SIZE (1 << NN)

void vecAdd(int n, double *A, double *B, double *C){
#pragma omp parallel for 
  for (int i = 0 ; i < n ; i++){
    C[i] = A[i] + B[i];
  }
}

int main( int argc, char** argv){
  /* set vectors */
  double aHost[SIZE],bHost[SIZE],cHost[SIZE];
  for (int i = 0 ; i < SIZE ; i++){
    aHost[i] = i;
    bHost[i] = SIZE - i;
    cHost[i] = 0.0;
  }
  
  /* comput A + B */
  double answer[SIZE];
  for (int i = 0 ; i < SIZE ; i++){
    answer[i] = aHost[i] + bHost[i]; 
  }

  /* call function  */
  int n = 2;
  for (int i = 0 ; i < NN ; i++){
    double start = omp_get_wtime();
    vecAdd(n,aHost,bHost,cHost);
    double milliseconds = (omp_get_wtime() - start) * 1000.0;
    
    std::cout << n << " elements : Processing time: " << milliseconds  << "(ms)\n";

    /* Check result */
    for (int j = 0 ; j < n ; j++){
      if (cHost[j] != answer[j]){
	std::cout << "answer is wrong!!\n";
	break;
      }
    }

    /* clear */
    for (int j = 0 ; j < n ; j++){
      cHost[j] = 0;
    }
    
    n = n * 2;
  }
}
