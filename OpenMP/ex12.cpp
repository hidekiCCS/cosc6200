//
//  Large Scale Computing
//  OpenMP Sample ex12.cpp
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <omp.h>

int main (int argc, char *argv[]) {
  std::cout << "Start\n";

#pragma omp parallel 
  {
    int id = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    if (id == 0){
      std::cout << nthreads << " threads work togather.\n";
    }
    // open file
    std::ostringstream fileName;
    fileName << "resEx12_" << std::setfill('0') << std::setw(2) << id << ".dat";
    std::ofstream ofile;
    ofile.open((fileName.str()).c_str());
//#pragma omp for schedule(dynamic,10)
#pragma omp for schedule(dynamic,10) collapse(2)
    for (int i = 0 ; i < 100 ; i++){
      for (int j = 0 ; j < 100 ; j++){
	ofile << i << " " << j << " " << id << std::endl;
      }
    }
    ofile.close();
  } // end of parallel
  std::cout << "End\n";

  return 0;
}
