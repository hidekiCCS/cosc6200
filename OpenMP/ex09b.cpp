/*
  Large Scale Computing
  OpenMP Sample ex09.cpp

  Reduction
 */
#include <iostream>
#include <algorithm>
#include <omp.h>

#define SIZE 10
int main (int argc, char *argv[]) {
  int id;
  int a[SIZE];
  
  std::cout << "Start\n";

  std::fill(a,a+SIZE,0); // This is Important !!! 
#pragma omp parallel private(id) reduction(+:a[0:SIZE])
  {
    id = omp_get_thread_num();
    std::fill(a,a+SIZE,id + 1);
    std::cout << "Thread" << id << "has a=" << a[0] << std::endl;
  }
  for (int i = 0 ; i < SIZE ; i++)
    std::cout << "End sum a[" << i << "]=" << a[i] << std::endl;

  return 0;
}
