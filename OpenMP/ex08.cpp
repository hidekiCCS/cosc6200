//
//  Large Scale Computing
//  OpenMP Sample ex08.cpp
//

#include <iostream>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id;

  std::cout << "Start\n";
  int a = -1;
  int b = -2;

#pragma omp parallel private(id,b) 
  {
    id = omp_get_thread_num();
    if (id == 0) a = 10; // thread0 do this
    if (id == 0) b = 10; // thread0 do this
    if (id == 1) b = 0;  // thread1 do this

    std::cout << "Thread" << id << "has a=" << a << std::endl;
    std::cout << "Thread" << id << "has b=" << b << std::endl;
  }
  std::cout << "End\n";
  
  return 0;
}
