//
//  Large Scale Computing
//  OpenMP Sample ex13.cpp
//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <omp.h>

int main (int argc, char *argv[]) {
  int nthreads;

  int64_t n;
  std::cout << "number of terms : ";
  std::cin >> n;
  if (std::cin.fail()){
    std::cout << "BYE\n";
    return 0;
  }
  std::cout << "Start n=" << n << "\n";

  double x = 0.0;
  double start = omp_get_wtime();
  #pragma omp parallel reduction(+:x)
  {
#pragma omp for
    for (int64_t i = 0 ; i < n ; i += 2){
      x += 1.0 / (2.0 * i + 1.0);
      x -= 1.0 / (2.0 * i + 3.0);
    }
#pragma omp master
    nthreads = omp_get_num_threads();
  }
  double tcost = omp_get_wtime() - start;
  std::cout << std::setprecision(16);
  std::cout << std::scientific;
  std::cout << nthreads << " threads compute ";
  std::cout << "Pi=" << 4.0 * x << "\n";
  std::cout << "Time cost = " << tcost << "(sec)\n";

  return 0;
}
