/*
  Large Scale Computing
  OpenMP Sample ex09.cpp

  Reduction
 */
#include <iostream>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id;

  std::cout << "Start\n";

  int a = 0; // This is Important !!! 
#pragma omp parallel private(id) reduction(+:a)
  {
    id = omp_get_thread_num();
    a = id + 1;
    std::cout << "Thread" << id << "has a=" << a << std::endl;
  }
  std::cout << "End sum a=" << a << std::endl;

  return 0;
}
