#include <iostream>
#include <openacc.h>
// ------------------------------------------------------------------

#pragma acc routine seq
extern int  successor( int   m );
#pragma acc routine seq
extern void increment( int* pm );


int main( void ) {
  int i;

  int numdev = acc_get_num_devices(acc_device_nvidia);
  std::cout << "Number of Devices = " << numdev << std::endl;
  // set current gpu to a specific device number
#pragma acc set device_type(acc_device_nvidia) device_num(0)

#pragma acc kernels present_or_copy(i)
  {
    i = successor( 123 );
    increment( &i );
  }
 
  std::cout << "i = " << i << std::endl;

  i = successor( i );
  increment( &i );
  std::cout << "i = " << i << std::endl;

  return 0;
}

// ------------------------------------------------------------------

#pragma acc routine seq
int successor( int m )  {
  return m+1;    // ++m works and is more compact, but seems cavalier
}

// ------------------------------------------------------------------
#pragma acc routine seq
void increment(   int* pm )  {
  (*pm)++;
  return;
}

