// OpenACC test
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <openacc.h>

int main(){

  int numdev = acc_get_num_devices(acc_device_nvidia);
  std::cout << "Number of Devices = " << numdev << std::endl;
    
  // set current gpu to a specific device number
#pragma acc set device_type(acc_device_nvidia) device_num(0)

  //Let num = 10000
  const int num = 10000;

  //Allocate two arrays of size num, A_i and B_i
  double *aArray = new double[num];
  double *bArray = new double[num];

  //Rank 0 assigns random values to those arrays.
  for (int i = 0 ; i < num ; i++){
    aArray[i] = static_cast<double>(rand()) / RAND_MAX;
    bArray[i] = static_cast<double>(rand()) / RAND_MAX;
  }

  //Allocate array of size num, C_i
  double *cArray = new double[num];
#pragma acc data copyin(aArray[0:num], bArray[0:num]) copyout(cArray[0:num])
  {
#pragma acc kernels present(aArray,bArray,cArray)
#pragma acc loop independent
    for (int i = 0 ; i < num ; i++){
      double sum = 0.0;
#pragma acc loop independent reduction(+:sum)
      for (int j = 0 ; j < num ; j++){
	sum += aArray[i] * bArray[j];
      }
      cArray[i] = sum;
    }

    //Compute ||C|| . Host gets the results.
    double cNorm = 0.0;
#pragma acc kernels
#pragma acc loop independent reduction(+:cNorm)
    for (int i = 0 ; i < num ; i++){
      cNorm += cArray[i] * cArray[i];
    }

    cNorm = std::sqrt(cNorm);
    std::cout << "||C||=" << cNorm << std::endl;
  }


  delete [] cArray;
  delete [] bArray;
  delete [] aArray;
  return 0;
}
