/*
 * ex82.cpp
 *
 *  Created on: Nov 9, 2015
 *      Author: fuji
*/
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <Eigen/Core>

#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#ifndef VIENNACL_WITH_OPENCL
#error "You have to compile with -DVIENNACL_WITH_OPENCL"
#endif
#ifndef VIENNACL_WITH_EIGEN
#error "You have to compile with -DVIENNACL_WITH_EIGEN"
#endif

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/coordinate_matrix.hpp"
#include "viennacl/compressed_matrix.hpp"
#include "viennacl/linalg/norm_2.hpp"
#include "viennacl/tools/timer.hpp"

// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv){
	if (argc < 3){
		std::vector<viennacl::ocl::platform > pfs = viennacl::ocl::get_platforms();
		for (int i = 0 ; i < pfs.size() ; i++){
			std::vector<viennacl::ocl::device> devices = pfs[i].devices(CL_DEVICE_TYPE_ALL);
			for (int j = 0 ; j < devices.size() ; j++){
				std::cout << "Platform" << i << " Device" << j << " " << devices[j].name() << std::endl;
			}
		}
		std::cout << argv[0] << " Platform # Device #\n";
		return 0;
	}
	int platformNum = std::atoi(argv[1]);
	int deviceNum = std::atoi(argv[2]);
	viennacl::ocl::platform platform = viennacl::ocl::get_platforms()[platformNum];
	viennacl::ocl::device device = platform.devices(CL_DEVICE_TYPE_ALL)[deviceNum];
	std::cout << "Using " << device.name() << std::endl;
	viennacl::ocl::setup_context(0, device);

	/* call Eigen3 norm2 */
	const size_t size= 50000000;
	Eigen::VectorXd eigen_vec(size);
	for (int i = 0 ; i < size ; i++) eigen_vec[i] = (i % 100) / 50.0;
	double ts = tsecond();
	double nrm = eigen_vec.norm();
	double te = tsecond();
	std::cout << "Eigen3 NORM=" << nrm << std::endl;
	std::cout << "Time cost for Eigen3 norm = " << 1000.0 * (te - ts)  << "(msec)\n";

	viennacl::tools::timer timer;
	double exec_time;
	timer.start();
	viennacl::vector<double> vcl_vec(size);
	viennacl::copy(eigen_vec, vcl_vec);
	exec_time = timer.get();
	std::cout << "Time cost for Transfer = " << 1000.0 * exec_time << "(msec)\n";

	timer.start();
	nrm = viennacl::linalg::norm_2(vcl_vec);
	exec_time = timer.get();
	std::cout << "ViennaCL NORM=" << nrm << std::endl;
	std::cout << "Time cost for 1st ViennaCL norm = " << 1000.0 * exec_time << "(msec)\n";

	timer.start();
	nrm = viennacl::linalg::norm_2(vcl_vec);
	exec_time = timer.get();
	std::cout << "ViennaCL NORM=" << nrm << std::endl;
	std::cout << "Time cost for 2nd ViennaCL norm = " << 1000.0 * exec_time << "(msec)\n";

	return 0;
}
