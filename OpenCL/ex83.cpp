/*
 * ex83.cpp
 *
 *  Created on: Nov 9, 2015
 *      Author: fuji
*/
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <Eigen/Core>

#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#ifndef VIENNACL_WITH_OPENCL
#error "You have to compile with -DVIENNACL_WITH_OPENCL"
#endif
#ifndef VIENNACL_WITH_EIGEN
#error "You have to compile with -DVIENNACL_WITH_EIGEN"
#endif

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/coordinate_matrix.hpp"
#include "viennacl/compressed_matrix.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/linalg/norm_2.hpp"
#include "viennacl/tools/timer.hpp"

// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv){
	const size_t size= 5000;
	if (argc < 3){
		std::vector<viennacl::ocl::platform > pfs = viennacl::ocl::get_platforms();
		for (int i = 0 ; i < pfs.size() ; i++){
			std::vector<viennacl::ocl::device> devices = pfs[i].devices(CL_DEVICE_TYPE_ALL);
			for (int j = 0 ; j < devices.size() ; j++){
				std::cout << "Platform" << i << " Device" << j << " " << devices[j].name() << std::endl;
			}
		}
		std::cout << argv[0] << " Platform # Device #\n";
		return 0;
	}
	int platformNum = std::atoi(argv[1]);
	int deviceNum = std::atoi(argv[2]);
	viennacl::ocl::platform platform = viennacl::ocl::get_platforms()[platformNum];
	viennacl::ocl::device device = platform.devices(CL_DEVICE_TYPE_ALL)[deviceNum];
	std::cout << "Using " << device.name() << std::endl;
	viennacl::ocl::setup_context(0, device);

	/* setup Eigen3  */
	Eigen::VectorXd eigen_xv(size);
	Eigen::VectorXd eigen_bv(size);
	Eigen::MatrixXd eigen_mA(size,size);

	/* setup matrix A */
	for (int i = 0 ; i < size ; i++){
		for (int j = 0 ; j < size ; j++){
			eigen_mA(i,j) = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}
	/* setup vector x */
	for (int i = 0 ; i < size ; i++) eigen_xv(i) = i;

	/// Eigen
	double ts = tsecond();
	eigen_bv = eigen_mA * eigen_xv;
	double te = tsecond();
	std::cout << "|b|=" << eigen_bv.norm()  << std::endl;
	std::cout << "Time cost for Eigen3 [b=Ax] = " << 1000.0 * (te - ts)  << "(msec)\n";

	viennacl::tools::timer timer;
	double exec_time;
	timer.start();
	viennacl::vector<double> vcl_xv(size);
	viennacl::vector<double> vcl_bv(size);
	viennacl::copy(eigen_xv, vcl_xv);
	viennacl::matrix<double>  vcl_mA(size, size);
	viennacl::copy(eigen_mA, vcl_mA);
	exec_time = timer.get();
	std::cout << "Time cost for Transfer = " << 1000.0 * exec_time << "(msec)\n";

	timer.start();
	vcl_bv = viennacl::linalg::prod(vcl_mA, vcl_xv);
	exec_time = timer.get();
	std::cout << "ViennaCL NORM=" << viennacl::linalg::norm_2(vcl_bv) << std::endl;
	std::cout << "Time cost for 1st ViennaCL [b=Ax] = " << 1000.0 * exec_time << "(msec)\n";

	timer.start();
	vcl_bv = viennacl::linalg::prod(vcl_mA, vcl_xv);
	exec_time = timer.get();
	std::cout << "ViennaCL NORM=" << viennacl::linalg::norm_2(vcl_bv) << std::endl;
	std::cout << "Time cost for 2nd ViennaCL [b=Ax] = " << 1000.0 * exec_time << "(msec)\n";

	return 0;
}
