/*
 Large Scale Computing
 ex81.cpp : OpenCL sample program

 THIS PROGRAM RUNS on GPU&CPU
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#ifdef __APPLE__
#include <OpenCL/cl.hpp> // this doesn't work
#else
#include <CL/cl.hpp>
#endif

#define SIZE 10

#define MAX_PLATFORM_IDS 8 //max platform_id
#define BUFFER_SIZE 4096 //information buffer size
#define MAX_DEVICE_IDS 8 //max devices

void check_device(void){
	cl_uint status;
	cl_platform_id platform_ids[MAX_PLATFORM_IDS];
	cl_uint num_platforms;
	status = clGetPlatformIDs(MAX_PLATFORM_IDS, platform_ids, &num_platforms);
	std::cout << "Number of platforms : " << num_platforms << std::endl;

	for (cl_uint pid = 0; pid < num_platforms; pid++) {
		char param_value[BUFFER_SIZE];
		size_t param_value_size_ret;
		std::cout << "Platform " << pid << ":\n";
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_NAME,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tName : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VENDOR,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tVender : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VERSION,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tVersion : " << param_value << std::endl;
		cl_device_id device_id[MAX_DEVICE_IDS];
		cl_uint num_devices;
		status = clGetDeviceIDs(platform_ids[pid], CL_DEVICE_TYPE_ALL,
				MAX_DEVICE_IDS, device_id, &num_devices);
		std::cout << "\tNumber of devices : " << num_devices << std::endl;
		for (cl_uint did = 0; did < num_devices; did++) {
			std::cout << "\tDevice " << did << ":\n";
			clGetDeviceInfo(device_id[did], CL_DEVICE_NAME, BUFFER_SIZE,
					param_value, &param_value_size_ret);
			std::cout << "\t\tName : " << param_value << std::endl;
			clGetDeviceInfo(device_id[did], CL_DEVICE_MAX_COMPUTE_UNITS,
					BUFFER_SIZE, param_value, &param_value_size_ret);
			std::cout << "\t\tMax Compute Unit : " << *(cl_uint *) param_value
					<< std::endl;
		}
	}
}

int main(int argc, char** argv) {
	//get all platforms (drivers)
	std::vector<cl::Platform> all_platforms;
	cl::Platform::get(&all_platforms);
	if (all_platforms.size() == 0) {
		std::cout << " No platforms found. Check OpenCL installation!\n";
		exit(1);
	}
	if (argc < 3){
		check_device();
		std::cout << argv[0] << " [Platform Number] [Device Number]\n";
		return 0;
	}
	int pnum = std::atoi(argv[1]);
	int dnum = std::atoi(argv[2]);

	cl::Platform default_platform = all_platforms[pnum];
	std::cout << "Using platform: " << default_platform.getInfo<CL_PLATFORM_NAME>() << "\n";

	//get default device of the default platform
	std::vector<cl::Device> all_devices;
	default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
	if (all_devices.size() == 0) {
		std::cout << " No devices found. Check OpenCL installation!\n";
		exit(1);
	}
	cl::Device default_device = all_devices[dnum];
	std::cout << "Using device: " << default_device.getInfo<CL_DEVICE_NAME>() << "\n";

	// context
	cl::Context context( { default_device });

	// kernel
	std::ifstream infile("ex81_kernel.cl");
	std::stringstream buffer;
	buffer << infile.rdbuf();
	std::string kernel_code = buffer.str();
	cl::Program::Sources sources;
	sources.push_back( { kernel_code.c_str(), kernel_code.length() });
	std::cout << kernel_code << std::endl;
	// build
	cl::Program program(context, sources);
	if (program.build() != CL_SUCCESS) {
		std::cout << " Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << "\n";
		exit(1);
	}
	// kernel
	cl::Kernel vecAdd(program, "vecAddCL");

	//create queue to which we will push commands for the device.
	cl::CommandQueue queue(context, default_device);

	/* set vectors */
	double aHost[SIZE], bHost[SIZE], cHost[SIZE];
	for (int i = 0; i < SIZE; i++) {
		aHost[i] = i;
		bHost[i] = SIZE - i;
		cHost[i] = 0.0;
	}

	/* allocate space on GPU Device*/
	cl::Buffer aDevice(context, CL_MEM_READ_WRITE, sizeof(double) * SIZE);
	cl::Buffer bDevice(context, CL_MEM_READ_WRITE, sizeof(double) * SIZE);
	cl::Buffer cDevice(context, CL_MEM_READ_WRITE, sizeof(double) * SIZE);

	/* send array data to GPU Device */
	queue.enqueueWriteBuffer(aDevice, CL_TRUE, 0, sizeof(double) * SIZE, aHost);
	queue.enqueueWriteBuffer(bDevice, CL_TRUE, 0, sizeof(double) * SIZE, bHost);

	/* call function on GPU */
	// arguments
	int n = SIZE;
	vecAdd.setArg(0, aDevice);
	vecAdd.setArg(1, bDevice);
	vecAdd.setArg(2, cDevice);
	vecAdd.setArg(3, sizeof(int), &n);
	// run
	cl::Event event;
    // Run the kernel on specific ND range
    cl::NDRange global(SIZE+1);
    cl::NDRange local(1);
    queue.enqueueNDRangeKernel(vecAdd, cl::NullRange, global, local);

    // wait for completion
    queue.finish();

	/* copy data from GPU Device */
	queue.enqueueReadBuffer(cDevice, CL_TRUE, 0, sizeof(double) * SIZE, cHost);

	/* print */
	for (int i = 0; i < SIZE; i++) {
		std::cout << "C[" << i << "]=" << cHost[i] << std::endl;
	}

	return 0;
}
