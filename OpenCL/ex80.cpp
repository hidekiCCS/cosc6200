/*
 * ex80.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: fuji
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <numeric>
#include <sys/time.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#include <CL/cl.h>
#endif

#define MAX_PLATFORM_IDS 8 //max platform_id
#define BUFFER_SIZE 4096 //information buffer size
#define MAX_DEVICE_IDS 8 //max devices

int main(int argc, char **argv) {
	cl_uint status;
	cl_platform_id platform_ids[MAX_PLATFORM_IDS];
	cl_uint num_platforms;
	status = clGetPlatformIDs(MAX_PLATFORM_IDS, platform_ids, &num_platforms);
	std::cout << "Number of platforms : " << num_platforms << std::endl;

	for (cl_uint pid = 0; pid < num_platforms; pid++) {
		char param_value[BUFFER_SIZE];
		size_t param_value_size_ret;
		std::cout << "Platform " << pid << ":\n";
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_NAME,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "Name : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VENDOR,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "Vender : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VERSION,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "Version : " << param_value << std::endl;
		cl_device_id device_id[MAX_DEVICE_IDS];
		cl_uint num_devices;
		status = clGetDeviceIDs(platform_ids[pid], CL_DEVICE_TYPE_ALL,
				MAX_DEVICE_IDS, device_id, &num_devices);
		std::cout << "Number of devices : " << num_devices << std::endl;
		for (cl_uint did = 0; did < num_devices; did++) {
			std::cout << "Device " << did << ":\n";
			clGetDeviceInfo(device_id[did], CL_DEVICE_NAME, BUFFER_SIZE,
					param_value, &param_value_size_ret);
			std::cout << "Name : " << param_value << std::endl;
			clGetDeviceInfo(device_id[did], CL_DEVICE_MAX_COMPUTE_UNITS,
					BUFFER_SIZE, param_value, &param_value_size_ret);
			std::cout << "Max Compute Unit : " << *(cl_uint *) param_value
					<< std::endl;
			clGetDeviceInfo(device_id[did], CL_DEVICE_MAX_WORK_GROUP_SIZE,
					BUFFER_SIZE, param_value, &param_value_size_ret);
			std::cout << "Max Work Group Size : " << *(size_t *) param_value
					<< std::endl;
			clGetDeviceInfo(device_id[did], CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
					BUFFER_SIZE, param_value, &param_value_size_ret);
			std::cout << "Device Native Vector Size of Float : "
					<< *(cl_uint *) param_value << std::endl;
			clGetDeviceInfo(device_id[did],
					CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, BUFFER_SIZE,
					param_value, &param_value_size_ret);
			std::cout << "Device Native Vector Size of Double : "
					<< *(cl_ulong *) param_value << std::endl;
			clGetDeviceInfo(device_id[did],
					CL_DEVICE_GLOBAL_MEM_SIZE, BUFFER_SIZE,
					param_value, &param_value_size_ret);
			std::cout << "Device Main Memory Size : "
					<< *(cl_uint *) param_value << std::endl;
		}
	}

	return 0;
}
