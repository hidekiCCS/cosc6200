#pragma OPENCL EXTENSION cl_khr_fp64 : enable
__kernel void vecAddCL(global const double *a, global const double *b, global double *c, int n)
{
	int i = get_global_id(0);

	if (i < n) {
		c[i] = a[i] + b[i];
	}
}
