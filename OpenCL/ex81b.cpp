/*
  Large Scale Computing
  ex81.cpp : OpenCL sample program

  THIS PROGRAM RUNS on GPU&CPU
*/

#include <iostream>
#include <cstdio>
#include <cstdlib>
#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#include <CL/opencl.h>
#endif

#define SIZE 10
#define MAX_PLATFORM_IDS 8 //max platform_id
#define BUFFER_SIZE 4096 //information buffer size
#define MAX_DEVICE_IDS 8 //max device

void check_device(void){
	cl_uint status;
	cl_platform_id platform_ids[MAX_PLATFORM_IDS];
	cl_uint num_platforms;
	status = clGetPlatformIDs(MAX_PLATFORM_IDS, platform_ids, &num_platforms);
	std::cout << "Number of platforms : " << num_platforms << std::endl;

	for (cl_uint pid = 0; pid < num_platforms; pid++) {
		char param_value[BUFFER_SIZE];
		size_t param_value_size_ret;
		std::cout << "Platform " << pid << ":\n";
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_NAME,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tName : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VENDOR,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tVender : " << param_value << std::endl;
		status = clGetPlatformInfo(platform_ids[pid], CL_PLATFORM_VERSION,
				BUFFER_SIZE, param_value, &param_value_size_ret);
		std::cout << "\tVersion : " << param_value << std::endl;
		cl_device_id device_id[MAX_DEVICE_IDS];
		cl_uint num_devices;
		status = clGetDeviceIDs(platform_ids[pid], CL_DEVICE_TYPE_ALL,
				MAX_DEVICE_IDS, device_id, &num_devices);
		std::cout << "\tNumber of devices : " << num_devices << std::endl;
		for (cl_uint did = 0; did < num_devices; did++) {
			std::cout << "\tDevice " << did << ":\n";
			clGetDeviceInfo(device_id[did], CL_DEVICE_NAME, BUFFER_SIZE,
					param_value, &param_value_size_ret);
			std::cout << "\t\tName : " << param_value << std::endl;
			clGetDeviceInfo(device_id[did], CL_DEVICE_MAX_COMPUTE_UNITS,
					BUFFER_SIZE, param_value, &param_value_size_ret);
			std::cout << "\t\tMax Compute Unit : " << *(cl_uint *) param_value
					<< std::endl;
		}
	}
}
int main( int argc, char** argv){
	// platform
	cl_platform_id   platform_id[MAX_PLATFORM_IDS];
	cl_device_id     device_id[MAX_DEVICE_IDS];
	cl_uint num_platforms;
	cl_uint num_devices;

	// platform
	clGetPlatformIDs(MAX_PLATFORM_IDS, platform_id, &num_platforms);
	if (num_platforms == 0) {
		std::cout << " No platforms found. Check OpenCL installation!\n";
		exit(1);
	}
	if (argc < 3){
		check_device();
		std::cout << argv[0] << " [Platform Number] [Device Number]\n";
		return 0;
	}
	cl_uint pnum = std::atoi(argv[1]);
	cl_uint dnum = std::atoi(argv[2]);
	// device
	clGetDeviceIDs(platform_id[pnum], CL_DEVICE_TYPE_ALL, MAX_DEVICE_IDS, device_id, &num_devices);

	// device name (option)
	char str[1024];
	size_t ret_size;
	clGetDeviceInfo(device_id[dnum], CL_DEVICE_NAME, sizeof(str), str, &ret_size);
	std::cout << str << std::endl;

	// context
	cl_int ret;
	cl_context context = clCreateContext(NULL, 1, &device_id[dnum], NULL, NULL, &ret);
	if (ret != CL_SUCCESS) {
		std::cout << "clCreateContext() error\n";
		return -1;
	}
	// command queue
	cl_command_queue command_queue = clCreateCommandQueue(context, device_id[dnum], 0, &ret);
	if (ret != CL_SUCCESS) {
		std::cout << "clCreateCommandQueue() error\n";
		return -1;
	}

	// setup kernel
	char *source_str = new char[BUFFER_SIZE];
	std::FILE* fp = std::fopen("ex81_kernel.cl", "r");
	size_t source_size = std::fread(source_str, 1, BUFFER_SIZE, fp);
	std::fclose(fp);
	std::cout << source_str << std::endl;
	// program
	cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
	if (ret != CL_SUCCESS) {
		std::cout << "clCreateProgramWithSource() error " << ret << std::endl;
		if (ret == CL_INVALID_CONTEXT){
			std::cout <<"CL_INVALID_CONTEXT\n";
		}
		return -1;
	}
	// build
	if (clBuildProgram(program, 1, &device_id[dnum], NULL, NULL, NULL) != CL_SUCCESS) {
		std::cout << "clBuildProgram() error\n";
		return -1;
	}
	// kernel
	cl_kernel kernel = clCreateKernel(program, "vecAddCL", &ret);
	if (ret != CL_SUCCESS) {
		std::cout << "clCreateKernel() error\n";
		return -1;
	}

	/* set vectors */
	double aHost[SIZE],bHost[SIZE],cHost[SIZE];
	for (int i = 0 ; i < SIZE ; i++){
		aHost[i] = i;
		bHost[i] = SIZE - i;
		cHost[i] = 0.0;
	}

	/* allocate space on GPU Device*/
	cl_mem aDevice,bDevice,cDevice;
	aDevice = clCreateBuffer(context, CL_MEM_READ_WRITE, SIZE * sizeof(double), NULL, &ret);
	bDevice = clCreateBuffer(context, CL_MEM_READ_WRITE, SIZE * sizeof(double), NULL, &ret);
	cDevice = clCreateBuffer(context, CL_MEM_READ_WRITE, SIZE * sizeof(double), NULL, &ret);
  
	/* send array data to GPU Device */
	clEnqueueWriteBuffer(command_queue, aDevice, CL_TRUE, 0, SIZE * sizeof(double), aHost, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, bDevice, CL_TRUE, 0, SIZE * sizeof(double), bHost, 0, NULL, NULL);

	/* call function on GPU */
	// arguments
	int n = SIZE;
	clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&aDevice);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&bDevice);
	clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&cDevice);
	clSetKernelArg(kernel, 3, sizeof(int),    (void *)&n);
	// run
	// work item
	size_t local_item_size = SIZE + 1;
	size_t global_item_size = 1;
	clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &global_item_size, &local_item_size, 0, NULL, NULL);

	/* copy data from GPU Device */
	clEnqueueReadBuffer(command_queue, cDevice, CL_TRUE, 0, SIZE * sizeof(double), cHost, 0, NULL, NULL);
  
	/* print */
	for (int i = 0 ; i < SIZE ; i++){
		std::cout << "C[" << i << "]=" << cHost[i] << std::endl;
	}

	// release
	clFlush(command_queue);
	clFinish(command_queue);
	clReleaseMemObject(aDevice);
	clReleaseMemObject(bDevice);
	clReleaseMemObject(cDevice);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);
	return 0;
}
