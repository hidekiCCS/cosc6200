//
//  Large Scale Computing
//  Heat/Mass Transfer
//  ex66.cpp
//  Parallel Gauss-Seidel Method
//  using CUDA

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cuda_runtime.h>

#define MAXTHREADS_BLOCK 512 /* maximum threads in a block */

__global__ void RBGS_step(int num, double *phi, double *rhs, int rb) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
	if ((n > 0) && (n < num - 1) && (n % 2 == rb)) {
		phi[n] = -0.5 * (rhs[n] - phi[n + 1] - phi[n - 1]);
	}
}

__global__ void residual(int num, double *phi, double *rhs, double *rsd) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
	if ((n > 0) && (n < num - 1)) {
		double r = rhs[n] - phi[n + 1] - phi[n - 1] + 2.0 * phi[n];
		rsd[n] = r * r;
	}
	if ((n == 0) || (n == num - 1)) {
		rsd[n] = 0;
	}
}

__global__ void reduce_sum_device(int cn, double *g_data) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x; // global ID
	int nl = threadIdx.x;   // thread ID in block
	__shared__ double s_data[512]; // use shared memory

	/* copy to shared memory */
	if (n >= cn)
		s_data[nl] = 0.0;
	else
		s_data[nl] = g_data[n];

	__syncthreads(); // wait until all threads in block copy data

	if (nl < 256)
		s_data[nl] += s_data[nl + 256];
	__syncthreads();
	if (nl < 128)
		s_data[nl] += s_data[nl + 128];
	__syncthreads();
	if (nl < 64)
		s_data[nl] += s_data[nl + 64];
	__syncthreads();
	if (nl < 32)
		s_data[nl] += s_data[nl + 32];
	__syncthreads();
	if (nl < 16)
		s_data[nl] += s_data[nl + 16];
	__syncthreads();
	if (nl < 8)
		s_data[nl] += s_data[nl + 8];
	__syncthreads();
	if (nl < 4)
		s_data[nl] += s_data[nl + 4];
	__syncthreads();
	if (nl < 2)
		s_data[nl] += s_data[nl + 2];
	__syncthreads();
	if (nl < 1)
		s_data[nl] += s_data[nl + 1];
	__syncthreads();

	/* store on global memory */
	if (nl == 0)
		g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

__host__ double reduce_sum(int num, double *g_data) {
	// num : size of array g_data[]
	// This host kernel computes $\Sigma^{num-1}_{i=0} g\_data[i]$
	// The result is stored in g_data[0]
	int n = num;
	while (n != 1) {
		int bl = n / 512 + 1;    // Number of Blocks
		int bx = min(bl, 512);    // Blocks in x-coordinate
		int by = bl / 512 + 1;   // Blocks in y-coordinate
		dim3 dimblock(bx, by, 1);  // z-coordinate is 1
		reduce_sum_device<<<dimblock,512>>>(n,g_data);

		n = bl; // each block recudes to 1
	}

	double ret;
	cudaMemcpy(&ret, g_data, sizeof(double), cudaMemcpyDeviceToHost);
	return ret;
}

int main(int argc, char **argv) {
	if (argc < 5) {
		std::cout << "Number of Grid Points [N]\n";
		std::cout << "Boundary Value at Left [A]\n";
		std::cout << "Boundary Value at Right [B]\n";
		std::cout << "Diffusion Coefficient [D]\n";
		std::cout << "Input [N] [A] [B] [D] \n";
		return 0;
	}

	//  set parameters
	int num = std::atoi(argv[1]);
	double a = std::atof(argv[2]);
	double b = std::atof(argv[3]);
	double d = std::atof(argv[4]);

	// CUDA Thread Block Grid
	int bl = num / MAXTHREADS_BLOCK + 1;
	int bx = min(bl, 512);
	int by = bl / 512 + 1;
	dim3 dimblockGS(bx, by, 1);

	// Allocation Array
	double *phi = new double[num];
	double *rhs = new double[num];

	// Setup
	double dx = 1.0 / (num - 1);
	for (int i = 0; i < num; i++) {
		rhs[i] = -dx * dx / d * (dx * i);
		phi[i] = 0.0;
	}

	// Boundary Conditions
	phi[0] = a;
	phi[num - 1] = b;

	// Alloc space on Device
	double *phiDev, *rhsDev, *rsdDev;
	cudaMalloc((void **) &phiDev, num * sizeof(double));
	cudaMalloc((void **) &rhsDev, num * sizeof(double));
	cudaMalloc((void **) &rsdDev, num * sizeof(double));

	// Copy Host to Device
	cudaMemcpy(phiDev, phi, num * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(rhsDev, rhs, num * sizeof(double), cudaMemcpyHostToDevice);

	//
	// Solve with Red & Black Gauss-Seidel Method
	int itc = 0;
	const double tol = 1.0e-8;
	while (1) {
		// update phi with RBGS
		for (int rb = 0; rb < 2; rb++) {
			RBGS_step<<<dimblockGS,MAXTHREADS_BLOCK>>>(num,phiDev,rhsDev,rb);
		}

		// Check Convergence
		residual<<<dimblockGS,MAXTHREADS_BLOCK>>>(num,phiDev,rhsDev,rsdDev);
		double merr = reduce_sum(num, rsdDev);

		itc++;
		if (merr < tol * tol) {
			break;
		}

		if ((itc % 1000) == 0) {
			std::cout << "itc=" << itc << " err=" << std::sqrt(merr) << std::endl;
		}
	}
	std::cout << "Number of Iteration=" << itc << std::endl;

	/* get resuts from device */
	cudaMemcpy(phi, phiDev, num * sizeof(double), cudaMemcpyDeviceToHost);

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx66.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		ofile << dx * i << " " << phi[i] << std::endl;
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
}
