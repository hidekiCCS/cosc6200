//
//  Large Scale Computing
//  2D Convective Heat/Mass Transfer
//  ex84.cpp
//  Solve for
//  duphi/dx dvphi/dy = d2phi/dx2 + d2phi/dy2
//  the boundary conditions:
//  phi = 0 along y=0, phi = 1 along y = 1
//  dphi/dx=0 along x=+-0.5
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Sparse>
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#define VIENNACL_WITH_OPENCL 1
#define VIENNACL_WITH_EIGEN 1

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/compressed_matrix.hpp"
#include "viennacl/linalg/bicgstab.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/linalg/norm_2.hpp"
#include "viennacl/tools/timer.hpp"
#include "viennacl/linalg/gmres.hpp"
#include "viennacl/linalg/ilu.hpp"

// index for all points
int gIdx(int i, int j, int num) {
	i = std::max(i, 0);
	i = std::min(i, num - 1);
	j = std::max(j, 0);
	j = std::min(j, num - 1);
	return i + num * j;
}

// X-coordinate (-0.5 <= x <= 0.5)
double xCord(double ix, int num) {
	return -0.5 + ix / (num - 1);
}

// Y-coordinate (0 <= y <= 1)
double yCord(double iy, int num) {
	return iy / (num - 1);
}

////////////////////////////////////////////////////////
// main
int main(int argc, char **argv) {
	if (argc < 6) {
		std::vector<viennacl::ocl::platform> pfs = viennacl::ocl::get_platforms();
		for (int i = 0; i < pfs.size(); i++) {
			std::vector<viennacl::ocl::device> devices = pfs[i].devices(CL_DEVICE_TYPE_ALL);
			for (int j = 0; j < devices.size(); j++) {
				std::cout << "Platform" << i << " Device" << j << " " << devices[j].name() << std::endl;
			}
		}
		std::cout << argv[0] << " [Platform #] [Device #] [SIZE] [D] [U0]\n";
		return 0;
	}
	// Determine Platform and Device
	int platformNum = std::atoi(argv[1]);
	int deviceNum = std::atoi(argv[2]);
	viennacl::ocl::platform platform = viennacl::ocl::get_platforms()[platformNum];
	viennacl::ocl::device device = platform.devices(CL_DEVICE_TYPE_ALL)[deviceNum];
	std::cout << "Using " << device.name() << std::endl;
	viennacl::ocl::setup_context(0, device);
	viennacl::ocl::current_device().double_support();

	// set parameters
	size_t num = std::atoi(argv[3]);
	double d = std::atof(argv[4]);
	double u0 = std::atof(argv[5]);
	std::cout << "D=" << d << " U0=" << u0 << std::endl;

	// assuming dx = dy : domain is 1x1
	double dl = 1.0 / (double) (num - 1);

	// Declare Eigen Matrix and Arrays
	Eigen::SparseMatrix<double> eigen_matA(num * num, num * num);
	eigen_matA.reserve(Eigen::VectorXi::Constant(num * num, 5));
	// vector for RHS
	Eigen::VectorXd eigen_rhs(num * num);
	// vector for solution
	Eigen::VectorXd eigen_phi(num * num);

	// set system
	for (int j = 0; j < num; j++) {
		for (int i = 0; i < num; i++) {
			// (u,v) = u0(x,-y)
			double ue = u0 * xCord(i + 0.5, num); // u at e-point
			double uw = u0 * xCord(i - 0.5, num); // u at w-point
			double vn = -u0 * yCord(j + 0.5, num); // v at n-point
			double vs = -u0 * yCord(j - 0.5, num); // v at s-point

			// up-wind scheme
			double ae = d / dl + std::max(-ue, 0.0);
			double aw = d / dl + std::max(uw, 0.0);
			double an = d / dl + std::max(-vn, 0.0);
			double as = d / dl + std::max(vs, 0.0);

			// Boundary Condition at x=0.5 and x=-0.5
			if (i == 0)
				aw = 0.0;
			if (i == num - 1)
				ae = 0.0;

			// -ap = (diagonal element)
			double ap = ae + aw + an + as;

			// set elements
			int n = gIdx(i, j, num);
			int ie = gIdx(i + 1, j, num);
			int iw = gIdx(i - 1, j, num);
			int in = gIdx(i, j + 1, num);
			int is = gIdx(i, j - 1, num);
			eigen_matA.coeffRef(n, n) = -ap;
			eigen_rhs(n) = 0.0;
			if ((j > 0) && (j < num - 1)) {
				if (i > 0)
					eigen_matA.coeffRef(n, iw) = aw;
				if (i < num - 1)
					eigen_matA.coeffRef(n, ie) = ae;
				eigen_matA.coeffRef(n, in) = an;
				eigen_matA.coeffRef(n, is) = as;
			} else {
				// top or bottom
				eigen_matA.coeffRef(n, n) = -1.0;
				if (j == num - 1)
					eigen_rhs(n) = -1.0;
			}
		}
	}

	// Declare ViennaCL Vector and Matrix
	viennacl::vector<double> vcl_phi(num * num);
	viennacl::vector<double> vcl_rhs(num * num);
	viennacl::vector<double> vcl_res(num * num);
	viennacl::compressed_matrix<double> vcl_matA(num * num, num * num);

	/// Transfer
	viennacl::tools::timer timer;
	timer.start();
	viennacl::copy(eigen_rhs, vcl_rhs);
	viennacl::copy(vcl_rhs, vcl_res);
	viennacl::copy(eigen_matA, vcl_matA);
	double exec_time = timer.get();
	std::cout << "Data Transfer : " << 1000.0 * exec_time << " (msec)\n";

	/// Solve
	timer.start();
	viennacl::linalg::ilut_precond< viennacl::compressed_matrix<double> > vcl_ilut(vcl_matA, viennacl::linalg::ilut_tag());
	viennacl::linalg::gmres_tag solver(1.0e-8, 1000, 20);
	vcl_phi = viennacl::linalg::solve(vcl_matA, vcl_rhs, solver, vcl_ilut);
	vcl_res -= viennacl::linalg::prod(vcl_matA, vcl_phi);
	std::cout << "Residual: " << viennacl::linalg::norm_2(vcl_res)  << std::endl;
	std::cout << "Estimated rel. residual: " << solver.error() << std::endl;
	std::cout << "Iterations: " << solver.iters() << std::endl;
	exec_time = timer.get();
	std::cout << "Solver : " << 1000.0 * exec_time << " (msec)\n";

	timer.start();
	vcl_phi = viennacl::linalg::solve(vcl_matA, vcl_rhs, solver, vcl_ilut);
	exec_time = timer.get();
	std::cout << "Solver : " << 1000.0 * exec_time << " (msec)\n";

	/// Transfer
	timer.start();
	viennacl::copy(vcl_phi, eigen_phi);
	exec_time = timer.get();
	std::cout << "Data Transfer : " << 1000.0 * exec_time << " (msec)\n";

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx84.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = xCord(i, num);
			double y = yCord(j, num);
			ofile << x << " " << y << " " << eigen_phi[gIdx(i, j, num)] << std::endl;
		}
	}
	ofile.close();

	timer.start();
	Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::IncompleteLUT<double> > eigen_solver;
	eigen_solver.compute(eigen_matA);
	eigen_phi = eigen_solver.solve(eigen_rhs);
	exec_time = timer.get();
	std::cout << "Eigen Solver : " << 1000.0 * exec_time << " (msec)\n";

	std::cout << "Done\n";
	return 0;
}
