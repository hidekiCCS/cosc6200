//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex05b.cpp  
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <omp.h>

/// Allocate 2D array
double **makeDoubleArray2D(double *&v,int s1,int s2){
  v = new double[s1 * s2];
  double **a = new double*[s1];
  for(int i = 0; i < s1; ++i)
    a[i] = v + s2 * i;

  return a;
}

/// Destroy 2D array
void destroyDoubleArray2D(double *&v,double **m){
  delete [] m;
  delete [] v;
}

int main(int argc, char **argv){
  if (argc < 2) {
    std::cout << argv[0] << " [size]\n";
    return 0;
  }

  int num = std::atoi(argv[1]);
  std::cout << "Size=" << num << std::endl;

  if (num <= 3) {
    std::cout << "Too small\n";
    return 0;
  }
  
  double *phi;  // k array
  double *phi0; // k+1 array
  double **phi2D = makeDoubleArray2D(phi,num,num);
  double **phi02D = makeDoubleArray2D(phi0,num,num);

  /* Letting dx = dy : domain is 2x2 */
  double dx = 2.0 / (num - 1);

  /* Initialize */
  std::fill(phi, phi + num * num, 0.0);
  std::fill(phi0, phi0 + num * num, 0.0);
  
  /* computing for phi with Jacobi Method */
  int k = 0;
  const double tol = 1.0e-8;
  double start = clock();
  while(1){
    for (int i = 1 ; i < num - 1; i++){
      for (int j = 1 ; j < num - 1; j++){
	phi2D[i][j] = -0.25 * (-dx * dx - phi02D[i+1][j] - phi02D[i-1][j] - phi02D[i][j+1] - phi02D[i][j-1]);
      }
    }
    double err = 0.0;
    for (int i = 1 ; i < num - 1; i++){
      for (int j = 1 ; j < num - 1; j++){
	double r = -dx * dx - phi2D[i+1][j] - phi2D[i-1][j] - phi2D[i][j+1] - phi2D[i][j-1] + 4 * phi2D[i][j];
	err += r * r;
	phi02D[i][j] = phi2D[i][j];
      }
    }
    k++;
    if (err < tol * tol) break;
    if (k % 1000 == 0){
      std::cout << "# of Iteration=" << k << " err=" << std::sqrt(err) << std::endl;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "# of Iteration=" << k << std::endl;
  std::cout << "Time cost = " << tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("ex05b.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0 ;i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      double x = -1.0 + 2.0 * i / (num - 1);
      double y = -1.0 + 2.0 * j / (num - 1);
      ofile << x << " " << y << " " << phi2D[i][j] << std::endl;
    }
  }
  ofile.close();

  // cleanup
  destroyDoubleArray2D(phi,phi2D);
  destroyDoubleArray2D(phi0,phi02D);

  std::cout << "Done\n";
  return 0;
}
