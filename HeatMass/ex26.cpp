//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex23.cpp
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
// Conjugare Gradient Method
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number of points]\n";
		return 0;
	}

	int num = std::atoi(argv[1]);
	std::cout << "Number of Points=" << num << std::endl;

	double *phi = new double[num * num];

	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (num - 1);

	/* Initialize */
	for (int n = 0; n < num * num; n++) {
		phi[n] = 0.0;
	}

	/* computing for phi with Conjugate Gradient Method */
	double start = clock();
	double *r = new double[num * num];
	double *p = new double[num * num];
	double *Ap = new double[num * num];

	/* r0 = b - Ax0 */
	for (int n = 0; n < num * num; n++) {
		int i = n % num; /* i is residual */
		int j = n / num; /* j is division */
		if (i == 0)
			continue; /*skip left end*/
		if (i == num - 1)
			continue; /*skip right end*/
		if (j == 0)
			continue; /*skip bottom end*/
		if (j == num - 1)
			continue; /*skip top end*/

		/// $r= -\delta x^2 + 4\phi_{i,j} - phi_{i+1,j} - \phi_{i,j+1} - \phi_{i-1,j} - \phi_{i,j-1}
		r[n] = -dx * dx;
		r[n] -= -4.0 * phi[n];
		r[n] -= phi[n + 1];
		r[n] -= phi[n + num];
		r[n] -= phi[n - 1];
		r[n] -= phi[n - num];
	}

	double rr = 0.0;
	for (int n = 0; n < num * num; n++) {
		p[n] = r[n]; /* p = r */
		rr += r[n] * r[n]; /* rr = r.r */
	}

	int k = 0;
	const double tol = 1.0e-8;
	while (rr > tol * tol) {
		// Ap = A*p
		for (int n = 0; n < num * num; n++) {
			Ap[n] = 0.0;
			int i = n % num; /* i is residual */
			int j = n / num; /* j is division */
			if (i == 0)
				continue; /*skip left end*/
			if (i == num - 1)
				continue; /*skip right end*/
			if (j == 0)
				continue; /*skip bottom end*/
			if (j == num - 1)
				continue; /*skip top end*/

			/* computing A*p */
			Ap[n] = -4.0 * p[n];
			Ap[n] += p[n + 1];
			Ap[n] += p[n + num];
			Ap[n] += p[n - 1];
			Ap[n] += p[n - num];
		}

		// alpha = r.r / p.Ap
		double pAp = 0.0;
		for (int n = 0; n < num * num; n++) {
			pAp += p[n] * Ap[n];
		}
		double alpha = rr / pAp;

		//Beta
		double rr1 = 0.0;
		for (int n = 0; n < num * num; n++) {
			phi[n] += alpha * p[n];
			r[n] -= alpha * Ap[n];
			rr1 += r[n] * r[n];
		}

		double beta = rr1 / rr;

		for (int n = 0; n < num * num; n++) {
			p[n] = r[n] + beta * p[n];
		}

		rr = rr1;
		k++;
		if (k % 1000 == 0) {
			std::cout << "# of Iteration=" << k << " err=" << std::sqrt(rr)
					<< std::endl;
		}
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "# of Iteration=" << k << std::endl;
	std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

	// Output Result
	std::ofstream ofile;
	ofile.open("res26.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
}
