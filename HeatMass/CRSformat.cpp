//  CSRformat
//
//  Created by Hideki Fujioka on 10/7/18.
//  Copyright  2018 Hideki Fujioka. All rights reserved.

#include "CRSformat.h"

// constructor
CRSformat::CRSformat(int rowSize,int columnSize)
:rowSize(rowSize),columnSize(columnSize),nzval(NULL),colind(NULL),rowptr(NULL){
    elements.resize(rowSize);
}

CRSformat::~CRSformat(){
    delete [] nzval;
    delete [] colind;
    delete [] rowptr;
}
// add element
void CRSformat::addElement(int row,int col,double elm){
    if (row < 0 || row >= rowSize){
        std::cout << "Row index out of range\n";
        return;
    }
    if (col < 0 || col >= columnSize){
        std::cout << "Column index out of range\n";
        return;
    }
    elements[row].push_back(std::pair<int,double>(col,elm));
    std::sort(elements[row].begin(),elements[row].end(),
              [](std::pair<int,double> a,std::pair<int,double> b){
                  return a.first < b.first;
              });
    auto last = std::unique(elements[row].begin(),elements[row].end(),
              [](std::pair<int,double> a,std::pair<int,double> b){
                  return a.first == b.first;
              });
    elements[row].erase(last,elements[row].end());
}

// get the number of non-zero elements
int CRSformat::getNNZ(void) const {
    int nnz = 0;
    for (int i = 0 ; i < rowSize ; i++){
        nnz += elements[i].size();
    }
    return nnz;
}

// get non-zero elements
const double *CRSformat::getNZVAL(void){
    if (nzval != NULL){
        delete [] nzval;
    }
    int nnz = getNNZ();
    nzval = new double[nnz];
    int n = 0;
    for (int i = 0 ; i < rowSize ; i++){
        for (int j = 0 ; j < elements[i].size(); j++){
            nzval[n] = elements[i][j].second;
            n++;
        }
    }
    return nzval;
}

/// get Column Index
const int *CRSformat::getColind(void){
    if (colind != NULL){
        delete [] colind;
    }
    int nnz = getNNZ();
    colind = new int[nnz];
    int n = 0;
    for (int i = 0 ; i < rowSize ; i++){
        for (int j = 0 ; j < elements[i].size(); j++){
            colind[n] = elements[i][j].first;
            n++;
        }
    }
    return colind;
}

/// get Row Pointer
const int *CRSformat::getRowptr(void){
    if (rowptr != NULL){
        delete [] rowptr;
    }
    rowptr = new int[rowSize+1];
    rowptr[0] = 0;
    for (int i = 1 ; i <= rowSize ; i++){
        rowptr[i] = rowptr[i-1] + static_cast<int>(elements[i-1].size());
    }
    return rowptr;
}

