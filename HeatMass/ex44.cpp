//
//  Large Scale Computing
//  2D Convective Heat/Mass Transfer
//  ex44.cpp
//  Solve for
//  duphi/dx dvphi/dy = d2phi/dx2 + d2phi/dy2
//  the boundary conditions:
//  phi = 0 along y=0, phi = 1 along y = 1
//  dphi/dx=0 along x=+-0.5
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <sys/time.h>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
extern "C" {
#include <cblas.h>
  int dgbsv_(int *, int *, int *, int *, double *, int *, int *, double *, int *, int *);
}
#endif
#endif

// timing method
double tsecond() {
  struct timeval tm;
  double t;
  static int base_sec = 0, base_usec = 0;

  gettimeofday(&tm, NULL);
  if (base_sec == 0 && base_usec == 0) {
    base_sec = tm.tv_sec;
    base_usec = tm.tv_usec;
    t = 0.0;
  } else {
    t = (double) (tm.tv_sec - base_sec)
      + ((double) (tm.tv_usec - base_usec)) / 1.0e6;
  }
  return t;
}

// index for all points
int gIdx(int i,int j,int num){
  i = std::max(i,0);
  i = std::min(i,num-1);
  j = std::max(j,0);
  j = std::min(j,num-1);
  return i + num * j;
}

// X-coordinate (-0.5 <= x <= 0.5)
double xCord(double ix,int num){
  return -0.5 + ix / (num - 1);
}

// Y-coordinate (0 <= y <= 1)
double yCord(double iy,int num){
  return iy / (num - 1);		
}

/// DGBSV Wrapper Class
class dgbsv_wrapper{
public:
  dgbsv_wrapper(int size, int kl, int ku):size(size),kl(kl),ku(ku){
    ldab = 2 * kl + ku + 1;
#ifdef MKL_ILP64
    mat = (double *)mkl_malloc(ldab * size * sizeof(double), 64);
    rhs = (double *)mkl_malloc(size * sizeof(double), 64);
    ipiv = new MKL_INT[size];
#else
    mat = new double[ldab * size];
    rhs = new double[size];
    ipiv = new int[size];
#endif
    for (int i = 0 ; i < ldab * size ; i++) mat[i] = 0.0;
    for (int i = 0 ; i < size ; i++) rhs[i] = 0.0;
  }
  ~dgbsv_wrapper(){
    delete [] mat;
    delete [] rhs;
    delete [] ipiv;
  }
  double getMat(int row, int col){
    return mat[index(row,col)];
  }
  void setMat(int row, int col, double val){
    mat[index(row,col)] = val;
  }
  double getRhs(int row){
    return rhs[row];
  }
  void setRhs(int row, double val){
    rhs[row] = val;
  }
  int index(int row, int col){
    return kl + ku + row - col + ldab * col;
  }
  int call_dgbsv(){
#ifdef MKL_ILP64
    MKL_INT nrhs = 1;
    MKL_INT info;
#else
    int nrhs = 1;
    int info;
#endif
    dgbsv_(&size, &kl, &ku, &nrhs, mat, &ldab, ipiv, rhs, &size, &info);
    return info;
  }

private:
  double *mat;
  double *rhs;
#ifdef MKL_ILP64
  MKL_INT *ipiv;
  MKL_INT size;
  MKL_INT ldab;
  MKL_INT kl;
  MKL_INT ku;
#else
  int *ipiv;
  int size;
  int ldab;
  int kl;
  int ku;
#endif
};


////////////////////////////////////////////////////////
// main
int main(int argc, char **argv){
  if (argc < 4){
    printf("Usage:%s [size] [D] [U0]\n",argv[0]);
    exit(-1);
  }
  
  // set parameters 
  int num = std::atoi(argv[1]);
  double d = std::atof(argv[2]);
  double u0 = std::atof(argv[3]);
  std::cout << "D=" << d << " U0=" << u0 << std::endl;

  // assuming dx = dy : domain is 1x1
  double dl = 1.0 / (double)(num - 1);
  
  // Declare DGESV WRAPPER
  dgbsv_wrapper lsys(num * num, num , num);

  // set system
  for (int j = 0 ; j < num ; j++){
    for (int i = 0 ; i < num ; i++){
      // (u,v) = u0(x,-y) 
      double ue = u0 * xCord(i+0.5,num); // u at e-point
      double uw = u0 * xCord(i-0.5,num); // u at w-point
      double vn = -u0 * yCord(j+0.5,num); // v at n-point
      double vs = -u0 * yCord(j-0.5,num); // v at s-point
		  
      // up-wind scheme
      double ae = d / dl + std::max(-ue,0.0);
      double aw = d / dl + std::max( uw,0.0);
      double an = d / dl + std::max(-vn,0.0);
      double as = d / dl + std::max( vs,0.0);
		  
      // Boundary Condition at x=0.5 and x=-0.5
      if (i == 0) aw = 0.0;
      if (i == num-1) ae = 0.0;

      // -ap = (diagonal element)
      double ap = ae + aw + an + as;

      // set elements
      int n = gIdx(i,j,num);
      int ie = gIdx(i+1,j,num);
      int iw = gIdx(i-1,j,num);
      int in = gIdx(i,j+1,num);
      int is = gIdx(i,j-1,num);
      lsys.setMat(n,n, -ap);
      lsys.setRhs(n,0.0);
      if ((j > 0) && (j < num -1)){
	if (i > 0)  lsys.setMat(n,iw,aw);
	if (i < num-1)  lsys.setMat(n,ie,ae);
	lsys.setMat(n,in,an);
	lsys.setMat(n,is,as);
      }else{
	// top or bottom
	lsys.setMat(n,n, -1.0);
	if (j == num -1) lsys.setRhs(n,-1.0);
      }
    }
  }

  // Solve with LAPACK DGBSV
  double start = tsecond();
  int info = lsys.call_dgbsv();
  double tcost = (tsecond() - start);
  if (info == 0) {
    std::cout << "successfully done\n";
  }
  if (info < 0) {
    std::cout << "the " << -info << "-th argument had an illegal value\n";
    return -1;
  }
  if (info > 0) {
    std::cout << "U(" << info << "," << info << ") is exactly zero.\n";
    return -1;
  }
  
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("resEx44.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0 ;i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      double x = xCord(i,num);
      double y = yCord(j,num);
      ofile << x << " " << y << " " << lsys.getRhs(gIdx(i,j,num)) << std::endl;
    }
  }
  ofile.close();
   
  std::cout << "Done\n";
  return 0;
}
