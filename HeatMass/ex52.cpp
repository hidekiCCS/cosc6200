//
//  Large Scale Computing
//  1D Heat/Mass Transfer with Jacobi Method
//  ex52.cpp 
//  Red-Black Gauss-Seidel
//
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <mpi.h>

int main(int argc, char **argv) {
  // Initialize MPI Library
  MPI::Init(argc, argv);

  // get myid and # of processors 
  int numproc = MPI::COMM_WORLD.Get_size();
  int myid = MPI::COMM_WORLD.Get_rank();

  if (argc < 5) {
    if (myid == 0) {
      std::cout << argv[0] << " [NUM] [A] [B] [D]\n";
    }
    MPI::Finalize();
    return 0;
  }

  //set parameters from commandline
  int num = std::atoi(argv[1]);
  double a = std::atof(argv[2]);
  double b = std::atof(argv[3]);
  double d = std::atof(argv[4]);

  if (myid == 0) {
    std::cout << "num=" << num << " A=" << a << " B=" << b << " D=" << d << std::endl;
  }

  /* divide domain into # of processes */
  int mystart = (num / numproc) * myid;
  int myend;
  if (num % numproc > myid) {
    mystart += myid;
    myend = mystart + (num / numproc) + 1;
  } else {
    mystart += num % numproc;
    myend = mystart + (num / numproc);
  }
  int mysize = myend - mystart;

  // wait until all processors come here 
  MPI::COMM_WORLD.Barrier();
  std::cout << "P" << myid << ":" << mystart << "~" << myend << " size=" << mysize << std::endl;

  /* Allocation of Arrays */
  double *phi = new double[mysize + 2];
  double *phi0 = new double[mysize + 2];
  double *rhs = new double[mysize + 2];

  // set RHS 
  double dx = 1.0 / (num - 1);
  for (int i = mystart; i < myend; i++) {
    rhs[i - mystart + 1] = -dx * dx / d * (dx * i);
  }

  // initialize phi
  for (int i = 0; i < mysize + 2; i++) {
    phi[i] = phi0[i] = 0;
  }

  // Boundary Conditions
  if (myid == 0) {
    phi[1] = phi0[1] = a;
  }
  if (myid == numproc - 1) {
    phi[mysize] = phi0[mysize] = b;
  }

  // Solve with Red/Black Gauss-Seidel Method
  int itc = 0;
  const double tol = 1.0e-8;
  while (1) {

    /* Red/Black Loop */
    for (int rb = 0; rb < 2; rb++) {
      /* update phi */
      for (int i = mystart; i < myend; i++) {
	if ((i == 0) || (i == (num - 1)) || (i % 2 == rb)) {
	  /* skip both ends */
	  /* skip r/b */
	  continue;
	}
	int ii = i - mystart + 1;
	phi[ii] = -0.5 * (rhs[ii] - phi[ii + 1] - phi[ii - 1]);
      }
      // send & receive with non-blocking communication
      MPI::Request commL;
      MPI::Request commR;
      for (int left_proc = 0; left_proc < numproc - 1; left_proc++) {
	int right_proc = left_proc + 1;
	if (myid == left_proc) {
	  /* Red/Black */
	  if (myend % 2 == rb) {
	    commL = MPI::COMM_WORLD.Isend(&phi[mysize], 1, MPI::DOUBLE, right_proc, 11);
	  } else {
	    commL = MPI::COMM_WORLD.Irecv(&phi[mysize + 1], 1, MPI::DOUBLE, right_proc, 22);
	  }
	}
	if (myid == right_proc) {
	  /* Red/Black */
	  if (mystart % 2 == rb) {
	    commR = MPI::COMM_WORLD.Irecv(&phi[0], 1, MPI::DOUBLE, left_proc, 11);
	  } else {
	    commR = MPI::COMM_WORLD.Isend(&phi[1], 1, MPI::DOUBLE, left_proc, 22);
	  }
	}
      }
      /* what to do? */

      /* Wait until send/receive complete */
      if (myid < numproc - 1) {
	commL.Wait();
      }
      if (myid > 0) {
	commR.Wait();
      }
    }
    itc++;
		
    // Check Convergence
    double merr = 0.0;
    for (int i = mystart; i < myend; i++) {
      int ii = i - mystart + 1;
      if ((i == 0) || (i == num - 1)) {
	// skip both sides
	continue;
      }
      double r = rhs[ii] - phi[ii + 1] - phi[ii - 1] + 2.0 * phi[ii];
      merr += r * r;
    }

    /* if converged exit */
    /* sum up merr and store in err on all processes */
    double err = 0.0;
    MPI::COMM_WORLD.Allreduce(&merr, &err, 1, MPI::DOUBLE, MPI::SUM);
    if (err < tol * tol) {
      break;
    }

    if ((myid == 0) && (itc % 10000 == 0)) {
      std::cout << "itc=" << itc << " err=" << std::sqrt(err) << std::endl;
    }
  }
  if (myid == 0) {
    std::cout << "Number of Iteration=" << itc << std::endl;
  }

  // Output Result
  std::ostringstream fileName;
  fileName << "resEx52_" << std::setfill('0') << std::setw(2) << myid << ".dat";
  std::ofstream ofile;
  ofile.open((fileName.str()).c_str());
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = mystart; i < myend; i++) {
    int ii = i - mystart + 1;
    ofile << dx * i << " " << phi[ii] << std::endl;
  }
  ofile.close();

  // wait until all processors come here 
  MPI::COMM_WORLD.Barrier();
  std::cout << "P" << myid << " Done\n";
  MPI::Finalize();
  return 0;
}
