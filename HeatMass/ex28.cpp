//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex28.cpp Use cg.h cg.cpp
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
//  Conjugate Gradient Method
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "cg.h"   // use cg

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number of points]\n";
		return 0;
	}

	int num = std::atoi(argv[1]);
	std::cout << "Number of Points=" << num << std::endl;

	/// allocate memory space
	double *phi = new double[num * num];
	double *sol = new double[(num - 2) * (num - 2)]; // Inner points are (num - 2) * (num - 2)
	double *rhs = new double[(num - 2) * (num - 2)];
	double *nzval = new double[5 * (num - 2) * (num - 2)];
	int *colind = new int[5 * (num - 2) * (num - 2)];
	int *rowptr = new int[(num - 2) * (num - 2) + 1];

	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (num - 1);

	/* Initialize */
	for (int n = 0; n < num * num; n++) {
		phi[n] = 0.0;
	}

	double start = clock();
	/* Setup Matrix A and RHS */
	int nnz = 0;
	for (int j = 1; j < num - 1; j++) {
		for (int i = 1; i < num - 1; i++) {

			int n = i + num * j; // index of grid points
			int nn = (i - 1) + (num - 2) * (j - 1); // index of vector elements

			/* initial guess */
			sol[nn] = phi[n];

			/* set general RHS */
			rhs[nn] = -dx * dx;

			/* set first nonzero column of row index(i,j) */
			rowptr[nn] = nnz;

			/* sounth */
			if (j == 1) {
				rhs[nn] -= phi[n - num];
			} else {
				nzval[nnz] = 1.0;
				colind[nnz] = nn - (num - 2); // i,j-1
				nnz++;
			}

			/* west */
			if (i == 1) {
				rhs[nn] -= phi[n - 1];
			} else {
				nzval[nnz] = 1.0;
				colind[nnz] = nn - 1; // i-1,j
				nnz++;
			}

			/* diagonal Element */
			nzval[nnz] = -4.0;
			colind[nnz] = nn; // i,j
			nnz++;

			/* east */
			if (i == num - 2) {
				rhs[nn] -= phi[n + 1];
			} else {
				nzval[nnz] = 1.0;
				colind[nnz] = nn + 1; // i+1,j
				nnz++;
			}

			/* north */
			if (j == num - 2) {
				rhs[nn] -= phi[n + num];
			} else {
				nzval[nnz] = 1.0;
				colind[nnz] = nn + (num - 2);  // i.j+1
				nnz++;
			}
		}
	}

	/* last element of rowptr si nnz */
	rowptr[(num - 2) * (num - 2)] = nnz;

	/* solve with CG */
	const double tol = 1.0e-8;
	int k = solve_cg((num - 2) * (num - 2), nnz, nzval, colind, rowptr, sol, rhs, tol);
	if (k < 0) {
		std::cout << "calculation failed\n";
		return 0;
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "# of Iteration=" << k << std::endl;
	std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

	// restore
	for (int j = 1; j < num - 1; j++) {
		for (int i = 1; i < num - 1; i++) {
			int n = i + num * j; // index of grid points
			int nn = (i - 1) + (num - 2) * (j - 1); // index of vector elements
			phi[n] = sol[nn];
		}
	}

	// Output Result
	std::ofstream ofile;
	ofile.open("res28.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
}
