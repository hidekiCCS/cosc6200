//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex79.cpp :
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
// Parallel Jacobi
#pragma offload_attribute (push, target(mic))
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>
#pragma offload_attribute (pop)

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number of points]\n";
		return 0;
	}

	int num = std::atoi(argv[1]);
	std::cout << "Number of Points=" << num << std::endl;

	void *vphi, *vphi0;
#pragma offload target(mic) out(vphi, vphi0)
	{
		posix_memalign((void **) &vphi, 64, num * num * sizeof(double));
		posix_memalign((void **) &vphi0, 64, num * num * sizeof(double));
	}

	void *vrhs, *vap, *vae, *vaw, *van, *vas;
#pragma offload target(mic) out(vrhs,vap,vae,vaw,van,vas)
	{
		posix_memalign((void **) &vrhs, 64, num * num * sizeof(double));
		posix_memalign((void **) &vap, 64, num * num * sizeof(double));
		posix_memalign((void **) &vae, 64, num * num * sizeof(double));
		posix_memalign((void **) &vaw, 64, num * num * sizeof(double));
		posix_memalign((void **) &van, 64, num * num * sizeof(double));
		posix_memalign((void **) &vas, 64, num * num * sizeof(double));
	}

	void *vie, *viw, *vin, *vis;
#pragma offload target(mic) out(vie, viw, vin, vis)
	{
		posix_memalign((void **) &vie, 64, num * num * sizeof(int));
		posix_memalign((void **) &viw, 64, num * num * sizeof(int));
		posix_memalign((void **) &vin, 64, num * num * sizeof(int));
		posix_memalign((void **) &vis, 64, num * num * sizeof(int));
	}
	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (num - 1);

	/* Initialize */
#pragma offload target(mic) in(vphi, vphi0, num)
	{
		double *phi = (double *) vphi;
		double *phi0 = (double *) vphi0;
#pragma omp parallel for schedule(runtime)
		for (int n = 0; n < num * num; n++) {
			phi[n] = 0.0;
			phi0[n] = 0.0;
		}
	}
#pragma offload target(mic) in(vrhs,vap,vae,vaw,van,vas),in(vie, viw, vin, vis),in(vphi,num,dx)
	{
		double *phi = (double *) vphi;
		double *rhs = (double *) vrhs;
		double *ap = (double *) vap;
		double *ae = (double *) vae;
		double *aw = (double *) vaw;
		double *an = (double *) van;
		double *as = (double *) vas;
		int *ie = (int *) vie;
		int *iw = (int *) viw;
		int *in = (int *) vin;
		int *is = (int *) vis;
#pragma omp parallel for schedule(guided,64)
		for (int n = 0; n < num * num; n++) {
			rhs[n] = -dx * dx;
			ap[n] = -4;
			ae[n] = 1;
			aw[n] = 1;
			an[n] = 1;
			as[n] = 1;
			ie[n] = n + 1;
			iw[n] = n - 1;
			in[n] = n + num;
			is[n] = n - num;
			if (((n % num) == 0) || ((n % num) == (num - 1)) || ((n / num) == 0) || ((n / num) == (num - 1))) {
				rhs[n] = phi[n] * ap[n];
				ae[n] = aw[n] = an[n] = as[n] = 0;
				ie[n] = iw[n] = in[n] = is[n] = n;
			}
		}
	}

	/* computing for phi with Jacobi Method */
	int itc = 0;
	const double tol = 1.0e-8;
	double start = clock();
	double omp_start = omp_get_wtime();
#pragma offload target(mic) \
	in (tol) \
	in(vrhs,vap,vae,vaw,van,vas),in(vie, viw, vin, vis),\
	in(vphi, vphi0)
	{
		double *phi = (double *) vphi;
		double *phi0 = (double *) vphi0;
		double *rhs = (double *) vrhs;
		double *ap = (double *) vap;
		double *ae = (double *) vae;
		double *aw = (double *) vaw;
		double *an = (double *) van;
		double *as = (double *) vas;
		int *ie = (int *) vie;
		int *iw = (int *) viw;
		int *in = (int *) vin;
		int *is = (int *) vis;

		double err;
		while (1) {
#pragma omp parallel for schedule(guided,num)
			for (int n = 0; n < num * num; n++) {
				phi[n] = (rhs[n] - ae[n] * phi0[ie[n]] - aw[n] * phi0[iw[n]] - an[n] * phi0[in[n]] - as[n] * phi0[is[n]]) / ap[n];
			}
			err = 0.0;
#pragma omp parallel for schedule(guided,num) reduction(+:err)
			for (int n = 0; n < num * num; n++) {
				double r = rhs[n] - ae[n] * phi[ie[n]] - aw[n] * phi[iw[n]] - an[n] * phi[in[n]] - as[n] * phi[is[n]] - ap[n] * phi[n];
				err += r * r;
				phi0[n] = phi[n];
			}
			itc++;
			if (err < tol * tol)
				break;
			if (itc % 1000 == 0) {
				std::cout << "# of Iteration=" << itc << " err=" << std::sqrt(err) << std::endl;
			}
		}
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	double omp_tcost = omp_get_wtime() - omp_start;
	std::cout << "Number of Iteration=" << itc << std::endl;
	std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";
	std::cout << "Time cost (WALL CLOCK)= " << omp_tcost << "(sec)\n";

	// data transfer
	double *phi = new double[num * num];
#pragma offload target(mic) out(phi:length(num*num) alloc_if(1) free_if(1)), in(vphi)
	{
		double *mic_phi = (double *) vphi;
#pragma omp parallel for schedule(runtime)
		for (int n = 0; n < num * num; n++) {
			phi[n] = mic_phi[n];
		}
	}

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx79.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();

	// deallocate
#pragma offload target(mic) \
		in(vrhs,vap,vae,vaw,van,vas),in(vie, viw, vin, vis),\
		in(vphi, vphi0)
	{
		free(vrhs);
		free(vap);
		free(vae);
		free(vaw);
		free(van);
		free(vas);
		free(vie);
		free(viw);
		free(vin);
		free(vis);
		free(vphi);
		free(vphi0);
	}
	std::cout << "Done\n";
	return 0;
	printf("Done\n");
}
