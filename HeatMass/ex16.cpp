//
//  Large Scale Computing
//  Heat/Mass Transfer
//  ex16.cpp
// Paralell Gauss-Seidel Method (Red-Black Ordering)
// OpenMP
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

int main(int argc, char **argv) {
  int i, num;
  double a, b, d;
  //  set parameters
  std::cout << "Number of Grid Points [N]\n";
  std::cout << "Boundary Value at Left [A]\n";
  std::cout << "Boundary Value at Right [B]\n";
  std::cout << "Diffusion Coefficient [D]\n";
  std::cout << "Input [N] [A] [B] [D] : ";
  std::cin >> num >> a >> b >> d;

  if (std::cin.fail()) {
    std::cout << "BYE\n";
    return 0;
  }

  // Allocation Array
  double *phi = new double[num];
  double *rhs = new double[num];

  // Setup
  double dx = 1.0 / (num - 1);
  for (int i = 0; i < num; i++) {
    rhs[i] = -dx * dx / d * (dx * i);
    phi[i] = 0.0;
  }

  // Boundary Conditions
  phi[0] = a;
  phi[num - 1] = b;

  //
  // Solve with Red-Black Gauss-Seidel Method
  int itc = 0;
  const double tol = 1.0e-8;
  double start = clock();
  double omp_start = omp_get_wtime();
  double merr = 0.0;
  while (1) {
      // update phi for Red Points
#pragma omp parallel for
      for (i = 1; i < num - 1; i+=2) {
	phi[i] = -0.5 * (rhs[i] - phi[i + 1] - phi[i - 1]);
      }

      // update phi for Black Points
#pragma omp parallel for 
      for (i = 2; i < num - 1; i+=2) {
	phi[i] = -0.5 * (rhs[i] - phi[i + 1] - phi[i - 1]);
      }
      
      // Check Convergence
      merr = 0.0;
#pragma omp parallel for reduction(+:merr) 
      for (i = 1; i < num - 1; i++) {
	double r = rhs[i] - phi[i + 1] - phi[i - 1] + 2.0 * phi[i];
	merr += r * r;
      }
      itc++;
      if ((itc % 10000) == 0) {
	std::cout << "itc=" << itc << " err=" << std::sqrt(merr) <<  std::endl;
      }
      if (merr < tol * tol){
	break;
      }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  double omp_tcost = omp_get_wtime() - omp_start;
  std::cout << "Number of Iteration=" << itc << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";
  std::cout << "Time cost (WALL CLOCK)= " << omp_tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("ex16.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
    ofile << dx * i << " " << phi[i] << std::endl;
  }
  ofile.close();

  std::cout << "Done\n";
  return 0;
}
