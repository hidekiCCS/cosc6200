//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex69.cpp
// Solve d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
//  Conjugate Gradient Method with CUBLAS
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <sys/time.h>
#include "cg_cublas.h"


// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number of points]\n";
		return 0;
	}

	int num = std::atoi(argv[1]);
	std::cout << "Number of Points=" << num << std::endl;

	double *phi = new double[num * num];
	double *rhs = new double[num * num];
	double *nzval = new double[5 * num * num];
	int *colind = new int[5 * num * num];
	int *rowptr = new int[num * num + 1];

	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (num - 1);

	/* Initialize */
	for (int n = 0; n < num * num; n++) {
		phi[n] = 0.0;
	}

	const double bigNumber = 1.0e+4;
	/* Setup Matrix A and RHS */
	int nnz = 0;
	for (int j = 0; j < num; j++) {
		for (int i = 0; i < num; i++) {
			int n = i + num * j;

			// set general RHS
			rhs[n] = -dx * dx;

			// set first nonzero column of row index(i,j)
			rowptr[n] = nnz;

			/* South */
			if (j > 0) {
				nzval[nnz] = 1.0;
				colind[nnz] = n - num; // i,j-1
				nnz++;
			}

			/* west */
			if (i > 0) {
				nzval[nnz] = 1.0;
				colind[nnz] = n - 1; // i-1,j
				nnz++;
			}

			/* diagonal Element */
			nzval[nnz] = -4.0;
			colind[nnz] = n; // i,j
			// boundary points
			if ((i == 0) || (i == num - 1) || (j == 0) || (j == num - 1)) {
				nzval[nnz] = bigNumber;
				rhs[n] = bigNumber * phi[n];
			}
			nnz++;

			/* east */
			if (i < num - 1) {
				nzval[nnz] = 1.0;
				colind[nnz] = n + 1; // i+1,j
				nnz++;
			}

			/* north */
			if (j < num - 1) {
				nzval[nnz] = 1.0;
				colind[nnz] = n + num;  // i.j+1
				nnz++;
			}
		}
	}

	/* last element of rowptr is nnz */
	rowptr[num * num] = nnz;

	/* solve with CG */
	const double tol = 1.0e-8;
	double start = tsecond();
	int k = solve_cg_cuda(num * num, nnz, nzval, colind, rowptr, phi, rhs, tol);
	if (k < 0) {
		std::cout << "calculation failed\n";
		return 0;
	}
	double tcost = tsecond() - start;
	std::cout << "# of Iteration=" << k << std::endl;
	std::cout << "Time cost = " << tcost << "(sec)\n";

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx69.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
}
