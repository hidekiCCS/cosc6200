/*
 Large Scale Computing
 Conjugate Gradinet Method with CUBLAS & CUSPARSE
 Harwell-Boeing Format
 */
#include<iostream>
#include<cstdlib>
#include<cuda_runtime.h>
#include<cublas_v2.h>
#include<cusparse.h>

/* CG use cublas & cusparse */

int solve_cg_cuda_host(const int n, const int nnz, const double *nzvalDev,
		const int *colindDev, const int *rowptrDev, double *xDev,
		const double *bDev, const double tol) {
	double *rDev;
	double *pDev;
	double *ApDev;
	double rr, pAp, rr1, alpha, beta;
	int count;
	double normb;

	const double ngt = -1.0;
	const double one = 1.0;
	const double zero = 0.0;

	/* Initialize cublas */
	cublasHandle_t cublasH;
	cublasCreate(&cublasH);

	/* initialize cusparse library */
	cusparseHandle_t cusparseH;
	cusparseCreate(&cusparseH);

	/* Check if the solution is trivial. */
	cublasDnrm2(cublasH, n, bDev, 1, &normb);
	std::cout << "normb=" << normb << " tol=" << tol << std::endl;
	if (normb <= 0.0) {
		cublasDscal(cublasH, n, &zero, xDev, 1);
		return 0;
	}

	/* allocate space on device */
	cudaMalloc((void **) &rDev, n * sizeof(double));
	cudaMalloc((void **) &pDev, n * sizeof(double));
	cudaMalloc((void **) &ApDev, n * sizeof(double));

	/* create and setup matrix descriptor */
	cusparseMatDescr_t descra;
	cusparseCreateMatDescr(&descra);
	cusparseSetMatType(descra, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descra, CUSPARSE_INDEX_BASE_ZERO);

	/* compute r0 = b - Ax */
	cublasDcopy(cublasH, n, bDev, 1, rDev, 1); /* r = b */
	cusparseDcsrmv(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &ngt,
			descra, nzvalDev, rowptrDev, colindDev, xDev, &one, rDev);

	rr = 0.0;
	cublasDcopy(cublasH, n, rDev, 1, pDev, 1); /* p = r */
	cublasDdot(cublasH, n, rDev, 1, rDev, 1, &rr); /* rr = r.r */
	std::cout << "rr=" << rr << " tol=" << tol * tol * normb * normb
			<< std::endl;

	/* CG iteration */
	count = 0;
	while (rr > tol * tol * normb * normb) {

		/* Ap = A*p */
		cusparseDcsrmv(cusparseH, CUSPARSE_OPERATION_NON_TRANSPOSE, n, n, nnz, &one,
				descra, nzvalDev, rowptrDev, colindDev, pDev, &zero, ApDev);

		/* alpha = r.r / p.Ap */
		cublasDdot(cublasH, n, pDev, 1, ApDev, 1, &pAp); /* pAp = p.Ap */
		alpha = rr / pAp;

		/* Beta */
		rr1 = 0.0;
		cublasDaxpy(cublasH, n, &alpha, pDev, 1, xDev, 1); /* x += alpha * p */
		double nalpha = -alpha;
		cublasDaxpy(cublasH, n, &nalpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
		cublasDdot(cublasH, n, rDev, 1, rDev, 1, &rr1); /* rr1 = r.r */
		beta = rr1 / rr;

		/* p = r + beta * p  no blas routine :( */
		cublasDscal(cublasH, n, &beta, pDev, 1); /* p *= beta */
		cublasDaxpy(cublasH, n, &one, rDev, 1, pDev, 1); /* p += r */

		rr = rr1;
		count++;
	}

	/* Deallocate Working Spaces */
	cudaFree(rDev);
	cudaFree(pDev);
	cudaFree(ApDev);
	cusparseDestroy(cusparseH);
	cusparseDestroyMatDescr(descra);
	cublasDestroy(cublasH);

	return count;
}

// Frontend function
int solve_cg_cuda(const int n, const int nnz, const double *nzval, const int *colind, const int *rowptr,
		const double *phi, const double *rhs, const double tol) {
	double *xDev;
	double *nzvalDev;
	double *bDev;
	int *colindDev;
	int *rowptrDev;
	int count;

	/* allocate space on device */
	cudaMalloc((void **) &nzvalDev, nnz * sizeof(double));
	cudaMalloc((void **) &xDev, n * sizeof(double));
	cudaMalloc((void **) &bDev, n * sizeof(double));
	cudaMalloc((void **) &colindDev, nnz * sizeof(int));
	cudaMalloc((void **) &rowptrDev, (n + 1) * sizeof(int));

	/* copy data to Device */
	cudaMemcpy(nzvalDev, nzval, nnz * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(xDev, phi, n * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(bDev, rhs, n * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(colindDev, colind, nnz * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(rowptrDev, rowptr, (n + 1) * sizeof(int),
			cudaMemcpyHostToDevice);

	/* SOLVE */
	count = solve_cg_cuda_host(n, nnz, nzvalDev, colindDev, rowptrDev, xDev,
			bDev, tol);
	/* copy solution to Host */
	cudaMemcpy((void *)phi, xDev, n * sizeof(double), cudaMemcpyDeviceToHost);

	return count;
}
