#!/bin/bash
#SBATCH --qos=normal		# Quality of Service
#SBATCH --job-name=ex57_1       # Job Name
#SBATCH --time=01:00:00		# WallTime
#SBATCH --nodes=16 		# Number of Nodes
#SBATCH --ntasks-per-node=1 	# Number of tasks (MPI presseces)
#SBATCH --cpus-per-task=1 	# Number of processors per task OpenMP threads()
#SBATCH --gres=mic:0  		# Number of Co-Processors

module load intel-psxe
module load petsc/3.5.4
module load gnuplot/5.0.1

pwd

echo "DIR=" $SLURM_SUBMIT_DIR
echo "TASKS_PER_NODE=" $SLURM_TASKS_PER_NODE
echo "NNODES=" $SLURM_NNODES
echo "NTASKS" $SLURM_NTASKS
echo "JOB_CPUS_PER_NODE" $SLURM_JOB_CPUS_PER_NODE
echo $SLURM_NODELIST

echo "16x1"
time mpirun ./ex57 -n 1000 -p 16 -q 1
gnuplot plotEx57.gpt
ps2pdf13 temp.eps ex57_16x1.pdf

echo "8x2"
time mpirun ./ex57 -n 1000 -p 8 -q 2
gnuplot plotEx57.gpt
ps2pdf13 temp.eps ex57_8x2.pdf

echo "4x4"
time mpirun ./ex57 -n 1000 -p 4 -q 4
gnuplot plotEx57.gpt
ps2pdf13 temp.eps ex57_4x4.pdf
