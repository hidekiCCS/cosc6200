//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex05.cpp : Numerical Analysis
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

int main(int argc, char **argv){
  int num;
  std::cout << "Number of Points : ";
  std::cin >> num;

  if (std::cin.fail() || (num <= 3)) {
    std::cout << "BYE\n";
    return 0;
  }
  double *phi = new double[num * num];
  double *phi0 = new double[num * num];  

  /*assuming dx = dy : domain is 2x2 */
  double dx = 2.0 / (num - 1);

  /* Initialize */
  for (int n = 0 ; n < num * num ; n++){
    phi[n] = 0.0;
    phi0[n] = 0.0;
  }
  
  /* computing for phi with Jacobi Method */
  int k = 0;
  const double tol = 1.0e-8;
  double start = clock();
  while(1){
    for (int n = 0 ; n < num * num; n++){
      if (n % num == 0) continue;
      if (n % num == num-1) continue;
      if (n / num == 0) continue;
      if (n / num == num-1) continue;
      int ie = n + 1;
      int iw = n - 1;
      int in = n + num;
      int is = n - num;
      phi[n] = 0.25 * (dx * dx + phi0[ie] + phi0[iw] + phi0[in] + phi0[is]);
    }
    double err = 0.0;
    for (int n = 0 ; n < num * num; n++){
      if (n % num == 0) continue;
      if (n % num == num-1) continue;
      if (n / num == 0) continue;
      if (n / num == num-1) continue;
      int ie = n + 1;
      int iw = n - 1;
      int in = n + num;
      int is = n - num;
      double r = -dx * dx - phi[ie] - phi[iw] - phi[in] - phi[is] + 4 * phi[n];
      err += r * r;
      phi0[n] = phi[n];
    }
    k++;
    if (err < tol * tol) break;
    if (k % 1000 == 0){
      std::cout << "# of Iteration=" << k << " err=" << std::sqrt(err) << std::endl;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "# of Iteration=" << k << std::endl;
  std::cout << "Time cost = " << tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("ex06.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0 ;i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      double x = -1.0 + 2.0 * i / (num - 1);
      double y = -1.0 + 2.0 * j / (num - 1);
      ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
    }
  }
  ofile.close();
  
  std::cout << "Done\n";
  return 0;
  printf("Done\n");
}
