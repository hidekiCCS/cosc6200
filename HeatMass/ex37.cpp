//
//  Large Scale Computing
//  2D Convective Heat/Mass Transfer
//  ex37.cpp
//  Solve for
//  duphi/dx dvphi/dy = d2phi/dx2 + d2phi/dy2
//  the boundary conditions:
//  phi = 0 along y=0, phi = 1 along y = 1
//  dphi/dx=0 along x=+-0.5
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>

// index for all points
int gIdx(int i,int j,int num){
  i = std::max(i,0);
  i = std::min(i,num-1);
  j = std::max(j,0);
  j = std::min(j,num-1);
  return i + num * j;
}

// X-coordinate (-0.5 <= x <= 0.5)
double xCord(double ix,int num){
	return -0.5 + ix / (num - 1);
}

// Y-coordinate (0 <= y <= 1)
double yCord(double iy,int num){
	return iy / (num - 1);		
}

// main
int main(int argc, char **argv){
  if (argc < 4){
    printf("Usage:%s [size] [D] [U0]\n",argv[0]);
    exit(-1);
  }
  
  // set parameters 
  int num = std::atoi(argv[1]);
  double d = std::atof(argv[2]);
  double u0 = std::atof(argv[3]);
  std::cout << "D=" << d << " U0=" << u0 << std::endl;

  // assuming dx = dy : domain is 1x1
  double dl = 1.0 / (double)(num - 1);
  
  //  Allocation for phi
  double *phi = new double[num * num];
  
  // initialize & boundary conditions
  for (int n = 0 ; n < num * num ; n++){
    phi[n] = 0.0;
  }
  for (int i = 0 ; i < num ; i++){
    phi[gIdx(i,num-1,num)] = 1.0; // fixed phi=1 at y = 1
  }

  // Setup Matrix A and RHS 
  double *ap = new double[num * num];
  double *ae = new double[num * num];
  double *aw = new double[num * num];
  double *an = new double[num * num];
  double *as = new double[num * num];

  for (int j = 1 ; j < num - 1 ; j++){
	  for (int i = 0 ; i < num ; i++){
		  // (u,v) = u0(x,-y) 
		  double ue = u0 * xCord(i+0.5,num); // u at e-point
		  double uw = u0 * xCord(i-0.5,num); // u at w-point
		  double vn = -u0 * yCord(j+0.5,num); // v at n-point
		  double vs = -u0 * yCord(j-0.5,num); // v at s-point
		  
		  // up-wind scheme
		  int n = gIdx(i,j,num);
		  ae[n] = d / dl + std::max(-ue,0.0);
		  aw[n] = d / dl + std::max( uw,0.0);
		  an[n] = d / dl + std::max(-vn,0.0);
		  as[n] = d / dl + std::max( vs,0.0);
		  
		  // Boundary Condition at x=0.5 and x=-0.5
		  if (i == 0) aw[n] = 0.0;
		  if (i == num-1) ae[n] = 0.0;

		  // -ap = (diagonal element)
		  ap[n] = ae[n] + aw[n] + an[n] + as[n];
	  }
  }
  
  
  // Solve with Gauss-Seidel Method
  int itc = 0;
  const double tol = 1.0e-8;
  double start = clock();
  while(1){
    // update phi
	for (int j = 1 ; j < num - 1 ; j++){
		for (int i = 0 ; i < num  ; i++){
			int n = gIdx(i,j,num);
			int ie = gIdx(i+1,j,num);
			int iw = gIdx(i-1,j,num);
			int in = gIdx(i,j+1,num);
			int is = gIdx(i,j-1,num);
			phi[n] =  (ae[n] *  phi[ie] + aw[n] *  phi[iw] + an[n] * phi[in] + as[n] * phi[is]) / ap[n];
		}
	}
	
	  
	// check convergence
    double err = 0.0;
    for (int j = 1 ; j < num - 1 ; j++){
    	for (int i = 1 ; i < num - 1 ; i++){
    		int n = gIdx(i,j,num);
    		int ie = gIdx(i+1,j,num);
    		int iw = gIdx(i-1,j,num);
    		int in = gIdx(i,j+1,num);
    		int is = gIdx(i,j-1,num);
    		double r = (ae[n] *  phi[ie] + aw[n] *  phi[iw] + an[n] * phi[in] + as[n] * phi[is] - ap[n] * phi[n]);
    		err += r * r;
    	}
    }
    itc++;
    if (err < tol * tol) break;
    if ((itc % 100) == 0){
    	std::cout << "itc=" << itc << " err=" << err << std::endl;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "Number of Iteration=" << itc << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

  // Output Result
   std::ofstream ofile;
   ofile.open("resEx37.dat");
   ofile << std::setprecision(16);
   ofile << std::scientific;
   for (int i = 0 ;i < num ; i++){
     for (int j = 0 ; j < num ; j++){
       double x = xCord(i,num);
       double y = yCord(j,num);
       ofile << x << " " << y << " " << phi[gIdx(i,j,num)] << std::endl;
     }
   }
   ofile.close();
   
   delete [] phi;
   delete [] ap;
   delete [] ae;
   delete [] aw;
   delete [] an;
   delete [] as;
   
   std::cout << "Done\n";
   return 0;
}
