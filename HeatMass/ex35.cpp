//
//  Large Scale Computing
//  Convective Heat/Mass Transfer
//  ex35.cpp : Gauss-Seidel 
//
//  duphi/dx = d2phi/dx2
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
 
int main(int argc, char **argv){
  if (argc < 6){
    printf("Usage:%s [NUM] [A] [B] [D] [U]\n",argv[0]);
    return 0;
  }
  
  //set parameters 
  int num = std::atoi(argv[1]);
  double a = std::atof(argv[2]);
  double b = std::atof(argv[3]);
  double d = std::atof(argv[4]);
  double u = std::atof(argv[5]);

  std::cout << "num=" << num 
		  << " A=" << a << " B=" << b 
		  << " D=" << d << " U=" << u << std::endl;
  
  // Memory Allocation
  double *phi = new double[num];
  
  // Setup 
  double dx = 1.0 / (num - 1);
  for (int i = 0 ; i < num ; i++){
    phi[i] = 0.0;
  }
  double ae = d / dx + std::max(-u,0.0);
  double aw = d / dx + std::max(u,0.0);
  double ap = ae + aw;

  // Boundary Condition
  phi[0] = a;
  phi[num-1] = b;

  // Solve with Gauss-Seidel Method
  int itc = 0;
  const double tol = 1.0e-8;
  double start = clock();
  while(1){
    // update phi
	for (int i = 1 ; i < (num - 1) ; i++){
		phi[i] =  (ae *  phi[i + 1] + aw *  phi[i - 1]) / ap;
	}
	  
	// check convergence
    double err = 0.0;
    for (int i = 1 ; i < (num - 1) ; i++){
    	double r =  (ae *  phi[i + 1] + aw *  phi[i - 1] - ap * phi[i]);
    	err += r * r;
    }
    itc++;
    if (err < tol * tol) break;
    if ((itc % 100) == 0){
    	std::cout << "itc=" << itc << " err=" << err << std::endl;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "Number of Iteration=" << itc << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("resEx35.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
     ofile << dx * i << " " << phi[i] << std::endl;
  }
  ofile.close();

  delete [] phi;
  std::cout << "Done\n";
  return 0;
}
