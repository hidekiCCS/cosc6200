//
//  Large Scale Computing
//  1D Heat/Mass Transfer with Jacobi Method
//  ex51.cpp 
//  use MPI
//
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <mpi.h>

int main(int argc, char **argv) {
	// Initialize MPI Library
	MPI_Init(&argc,&argv);

	// get myid and # of processors 
	int numproc;
	int myid;
	MPI_Comm_size(MPI_COMM_WORLD,&numproc);
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);

	if (argc < 5) {
		if (myid == 0) {
			std::cout << argv[0] << " [NUM] [A] [B] [D]\n";
		}
		MPI_Finalize();
		return 0;
	}

	//set parameters from commandline
	int num = std::atoi(argv[1]);
	double a = std::atof(argv[2]);
	double b = std::atof(argv[3]);
	double d = std::atof(argv[4]);

	if (myid == 0) {
		std::cout << "num=" << num << " A=" << a << " B=" << b << " D=" << d << std::endl;
	}

	/* divide domain into # of processes */
	int mystart = (num / numproc) * myid;
	int myend;
	if (num % numproc > myid) {
		mystart += myid;
		myend = mystart + (num / numproc) + 1;
	} else {
		mystart += num % numproc;
		myend = mystart + (num / numproc);
	}
	int mysize = myend - mystart;

	// wait until all processors come here 
	MPI_Barrier(MPI_COMM_WORLD);
	std::cout << "P" << myid << ":" << mystart << "~" << myend << " size=" << mysize << std::endl;

	/* Allocation of Arrays */
	double *phi = new double[mysize + 2];
	double *phi0 = new double[mysize + 2];
	double *rhs = new double[mysize + 2];

	// set RHS 
	double dx = 1.0 / (num - 1);
	for (int i = mystart; i < myend; i++) {
		rhs[i - mystart + 1] = -dx * dx / d * (dx * i);
	}

	// initialize phi
	for (int i = 0; i < mysize + 2; i++) {
		phi[i] = phi0[i] = 0;
	}

	// Boundary Conditions
	if (myid == 0) {
		phi[1] = phi0[1] = a;
	}
	if (myid == numproc - 1) {
		phi[mysize] = phi0[mysize] = b;
	}

	// Solve with Jacobi Method
	int itc = 0;
	const double tol = 1.0e-8;
	while (1) {
		// update phi 
		for (int i = mystart; i < myend; i++) {
			if ((i == 0) || (i == num - 1)) {
				// skip both sides
				continue;
			}
			int ii = i - mystart + 1;
			phi[ii] = -0.5 * (rhs[ii] - phi0[ii + 1] - phi0[ii - 1]);
		}

		// send & receive with non-blocking communication
		MPI_Request SendL;
		MPI_Request SendR;
		MPI_Request RecvL;
		MPI_Request RecvR;
		for (int left_proc = 0; left_proc < numproc - 1; left_proc++) {
			int right_proc = left_proc + 1;
			if (myid == left_proc) {
				MPI_Isend(&phi[mysize], 1, MPI_DOUBLE, right_proc, 11, MPI_COMM_WORLD,&SendR);
				MPI_Irecv(&phi0[mysize + 1], 1, MPI_DOUBLE, right_proc, 22, MPI_COMM_WORLD,&RecvR);
			}
			if (myid == right_proc) {
				MPI_Irecv(&phi0[0], 1, MPI_DOUBLE, left_proc, 11, MPI_COMM_WORLD,&RecvL);
				MPI_Isend(&phi[1], 1, MPI_DOUBLE, left_proc, 22, MPI_COMM_WORLD,&SendL);
			}
		}

		// update phi0
		for (int i = mystart; i < myend; i++) {
			int ii = i - mystart + 1;
			phi0[ii] = phi[ii];
		}

		itc++;

		/* Wait until send/receive complete */
		MPI_Status status;
		if (myid < numproc - 1) {
			MPI_Wait(&SendR,&status);
			MPI_Wait(&RecvR,&status);
		}
		if (myid > 0) {
			MPI_Wait(&RecvL,&status);
			MPI_Wait(&SendL,&status);
		}

		// Check Convergence
		double merr = 0.0;
		for (int i = mystart; i < myend; i++) {
			int ii = i - mystart + 1;
			if ((i == 0) || (i == num - 1)) {
				// skip both sides
				continue;
			}

			double r = rhs[ii] - phi0[ii + 1] - phi0[ii - 1] + 2.0 * phi0[ii];
			merr += r * r;
		}

		/* if converged exit */
		/* sum up merr and store in err on all processes */
		double err = 0.0;
		MPI_Allreduce(&merr, &err, 1, MPI_DOUBLE, MPI_SUM,MPI_COMM_WORLD);
		if (err < tol * tol) {
			break;
		}

		if ((myid == 0) && (itc % 10000 == 0)) {
			std::cout << "itc=" << itc << " err=" << std::sqrt(err) << std::endl;
		}
	}
	if (myid == 0) {
		std::cout << "Number of Iteration=" << itc << std::endl;
	}

	// Output Result
	std::ostringstream fileName;
	fileName << "resEx51_" << std::setfill('0') << std::setw(2) << myid << ".dat";
	std::ofstream ofile;
	ofile.open((fileName.str()).c_str());
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = mystart; i < myend; i++) {
		int ii = i - mystart + 1;
		ofile << dx * i << " " << phi[ii] << std::endl;
	}
	ofile.close();

	// wait until all processors come here 
	MPI_Barrier(MPI_COMM_WORLD);
	std::cout << "P" << myid << " Done\n";
	MPI_Finalize();
	return 0;
}
