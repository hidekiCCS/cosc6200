//
//  CSRformat
//
//  Created by Hideki Fujioka on 10/7/18.
//  Copyright  2018 Hideki Fujioka. All rights reserved.
//

#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

class CRSformat
{
public:
    // constructor
    CRSformat(int rowSize,int columnSize);
    // destructor
    ~CRSformat();
    
    // add element
    void addElement(int row,int col,double elm);
    
    // get the number of non-zero elements
    int getNNZ(void) const;
    
    // get non-zero elements
    const double *getNZVAL(void);
    
    /// get Column Index
    const int *getColind(void);
    
    /// get Row Pointer
    const int *getRowptr(void);
private:
    // Row Size
    int rowSize;
    // Column Size
    int columnSize;
    // element data
    std::vector<std::vector<std::pair<int, double>> > elements;
    
    // CRS formant data
    double *nzval;
    int *colind;
    int *rowptr;
};
