//
//  Large Scale Computing
//  2D Heat/Mass Transfer
//  ex59.cpp
//  Solve for
//  d2c/dx2 + d2c/dy2 + 1 = 0
//  With the boundary conditions of c = 0
//  along lines of x=1,-1, and y=1, and -1
//  use PETSc and Parmetis
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <petsc.h>
#include <parmetis.h>

#if (PARMETIS_MAJOR_VERSION <= 3)
typedef idxtype idx_t;
typedef float real_t;
#endif

int main(int argc, char **argv) {
#if PETSC_VERSION_LE(3,1,0)
	PetscTruth flag;
#else
	PetscBool flag;
#endif
	/* Initialize PETSc and MPI */
	PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

	/* set parameters */
	int num;
#if PETSC_VERSION_LE(3,6,0)
	PetscOptionsGetInt(PETSC_NULL, "-n", &num, &flag);
#else
	PetscOptionsGetInt(NULL, NULL, "-n", &num, &flag);
#endif
	if (!flag) {
		PetscPrintf(PETSC_COMM_WORLD, "Usage:%s -n [NUM] \n", argv[0]);
		PetscFinalize();
		return 0;
	}

	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (double) (num - 1);

	/* get # of process and myid, use MPI commands */
	PetscMPIInt numproc, myid;
	MPI_Comm_size(PETSC_COMM_WORLD, &numproc);
	MPI_Comm_rank(PETSC_COMM_WORLD, &myid);

	// make in graph data
	idx_t *vtxdist = new idx_t[numproc + 1];
	for (int i = 0; i < numproc; i++) {
		vtxdist[i] = i * num * num / numproc;
	}
	idx_t dim = 2;
	vtxdist[numproc] = num * num;
	idx_t *xadj = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
	real_t *xyz = new real_t[dim * (vtxdist[myid + 1] - vtxdist[myid])];
	idx_t *adjency = new idx_t[(vtxdist[myid + 1] - vtxdist[myid]) * 4];

	int count = 0;
	for (int n = vtxdist[myid]; n < vtxdist[myid + 1]; n++) {
		int i = n % num;
		int j = n / num;
		xadj[n - vtxdist[myid]] = count;
		if (i != num - 1) {		// add east point
			adjency[count] = n + 1;
			count++;
		}
		if (i != 0) {			// add west point
			adjency[count] = n - 1;
			count++;
		}
		if (j != num - 1) {		// add north point
			adjency[count] = n + num;
			count++;
		}
		if (j != 0) {			// add north point
			adjency[count] = n - num;
			count++;
		}
		double x = -1.0 + 2.0 * (double) i / (double) (num - 1);
		double y = -1.0 + 2.0 * (double) j / (double) (num - 1);
		xyz[n - vtxdist[myid]] = x;
		xyz[n - vtxdist[myid] + 1] = y;
	}
	xadj[vtxdist[myid + 1] - vtxdist[myid]] = count;

	// Graph Partitioning
	idx_t wgtflag = 0;
	idx_t numflag = 0;
	idx_t ncon = 1;
	idx_t nparts = numproc;
	real_t *ubvec = new real_t[ncon];
	real_t *tpwgts = new real_t[ncon * nparts];
	for (int i = 0; i < ncon * nparts; i++) {
		tpwgts[i] = 1.0 / (real_t) nparts;
	}
	idx_t *part = new idx_t[vtxdist[myid + 1] - vtxdist[myid] + 1];
	idx_t edgecut;
	idx_t options[10];
	idx_t ncommonnodes = 2;
	ubvec[0] = 1.05;
	MPI_Comm comm;
	MPI_Comm_dup(PETSC_COMM_WORLD, &comm);
	ParMETIS_V3_PartGeomKway(vtxdist, xadj, adjency, NULL, NULL, &wgtflag, &numflag, &dim, xyz, &ncon, &nparts, tpwgts, ubvec, options, &edgecut, part, &comm);
	//ParMETIS_V3_PartKway(vtxdist, xadj, adjency, NULL, NULL, &wgtflag, &numflag, &ncon, &nparts, tpwgts, ubvec, options, &edgecut, part, &comm);
    //ParMETIS_V3_PartMeshKway(vtxdist, xadj, adjency, NULL, &wgtflag, &numflag, &ncon, &ncommonnodes, &nparts, tpwgts, ubvec, options, &edgecut, part, &comm);

	// distribute over processes
	int *proc_own = new int[num * num];
	int *proc_ownR = new int[num * num];
	for (int n = 0; n < num * num; n++) {
		proc_own[n] = 0;
	}
	for (int n = vtxdist[myid]; n < vtxdist[myid + 1]; n++) {
		proc_own[n] = part[n - vtxdist[myid]];
	}
	MPI_Allreduce(proc_own, proc_ownR, num * num, MPI_INT, MPI_MAX, PETSC_COMM_WORLD);
	delete[] proc_own;
	delete[] vtxdist;
	delete[] xadj;
	delete[] adjency;

	int mySize = 0;
	for (int i = 0; i < num * num; i++) {
		if (proc_ownR[i] == myid) {
			mySize++;
		}
	}
	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Proc%d: %d\n", myid, mySize);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* define vector */
	Vec phi;
	Vec rhs;
	VecCreateMPI(PETSC_COMM_WORLD, mySize, num * num, &phi);
	VecDuplicate(phi, &rhs);
	PetscInt mystart, myend;
	VecGetOwnershipRange(rhs, &mystart, &myend);
	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Proc%d: Vec phi %d~%d\n", myid, mystart, myend);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* define matrix */
	Mat A;
#if PETSC_VERSION_LE(3,2,0)
	MatCreateMPIAIJ(PETSC_COMM_WORLD,
#else
	MatCreateAIJ(PETSC_COMM_WORLD,
#endif
			myend - mystart, myend - mystart, // Local Size
			num * num, num * num, // Global Size
			PETSC_DECIDE, NULL, /* number of non-zero (PETSc to decide)*/
			PETSC_DECIDE, NULL, &A);

	/* make domain grid indices - vector index */
	int *gIndexBuf = new int[num * num];
	for (int i = 0; i < num * num; i++) {
		gIndexBuf[i] = 0;
	}
	int n = mystart;
	for (int i = 0; i < num * num; i++) {
		if (proc_ownR[i] == myid) {
			gIndexBuf[i] = n;
			n++;
		}
	}
	int *gIndexBuf2 = new int[num * num];
	MPI_Allreduce(gIndexBuf, gIndexBuf2, num * num, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	for (int i = 0; i < num * num; i++) {
		gIndexBuf[i] = gIndexBuf2[i];
	}
	delete[] gIndexBuf2;

	/* set matrix & rhs */
	for (int n = 0; n < num * num; n++) {
		if (proc_ownR[n] == myid) {
			int ip = gIndexBuf[n];
			double ae = 1;
			double aw = 1;
			double an = 1;
			double as = 1;
			double ap = ae + aw + an + as;
			int i = n % num;
			int j = n / num;

			if (i == 0) { /* left */
				MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);
				VecSetValue(rhs, ip, 0.0, INSERT_VALUES);
				continue;
			}
			if (i == (num - 1)) { /* right */
				MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);
				VecSetValue(rhs, ip, 0.0, INSERT_VALUES);
				continue;
			}
			if (j == 0) { /* bottom */
				MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);
				VecSetValue(rhs, ip, 0.0, INSERT_VALUES);
				continue;
			}
			if (j == (num - 1)) { /* top */
				MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);
				VecSetValue(rhs, ip, 0.0, INSERT_VALUES);
				continue;
			}

			int ie = gIndexBuf[n + 1];
			int iw = gIndexBuf[n - 1];
			int in = gIndexBuf[n + num];
			int is = gIndexBuf[n - num];
			MatSetValue(A, ip, ip, -ap, INSERT_VALUES);
			MatSetValue(A, ip, ie, ae, INSERT_VALUES);
			MatSetValue(A, ip, iw, aw, INSERT_VALUES);
			MatSetValue(A, ip, in, an, INSERT_VALUES);
			MatSetValue(A, ip, is, as, INSERT_VALUES);
			VecSetValue(rhs, ip, -dx * dx, INSERT_VALUES);
		}
	}

	/* assemble */
	MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(rhs);
	VecAssemblyEnd(rhs);

	/* SOLVE LINEAR SYSTEM */
	PetscLogDouble kspBegin, kspEnd;
	kspBegin = MPI_Wtime();
	KSP ksp; /* linear solver context */
	PC pc; /* preconditioner context */
	KSPCreate(PETSC_COMM_WORLD, &ksp); /* create KSP object */
#if PETSC_VERSION_LE(3,5,0)
	KSPSetOperators(ksp, A, A, DIFFERENT_NONZERO_PATTERN);
#else
	KSPSetOperators(ksp, A, A);
#endif
	KSPGetPC(ksp, &pc); /* create pre-conditionar object */
	KSPSetFromOptions(ksp);
	KSPSolve(ksp, rhs, phi);
	kspEnd = MPI_Wtime();
	KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);
	PetscPrintf(PETSC_COMM_WORLD, "Time Cost = %e(s)\n", kspEnd - kspBegin);

	/* Output Result */
	PetscScalar *pphi;
	std::ostringstream fileName;
	fileName << "resEx59_" << std::setfill('0') << std::setw(2) << myid << ".dat";
	std::ofstream ofile;
	ofile.open((fileName.str()).c_str());
	ofile << std::setprecision(16);
	ofile << std::scientific;
	VecGetArray(phi, &pphi);
	for (int n = 0; n < num * num; n++) {
		if (proc_ownR[n] == myid) {
			int i = n % num;
			int j = n / num;
			double x = -1.0 + 2.0 * (double) i / (double) (num - 1);
			double y = -1.0 + 2.0 * (double) j / (double) (num - 1);
			ofile << x << " " << y << " " << pphi[gIndexBuf[n] - mystart] << std::endl;
			n++;
		}
	}
	VecRestoreArray(phi, &pphi);
	ofile.close();

#if PETSC_VERSION_LE(3,1,0)
	VecDestroy(phi);
	VecDestroy(rhs);
	MatDestroy(A);
	KSPDestroy(ksp);
#else
	VecDestroy(&phi);
	VecDestroy(&rhs);
	MatDestroy(&A);
	KSPDestroy(&ksp);
#endif
	PetscFinalize();
	return 0;
}
