/*
 Large Scale Computing
 2D Heat/Mass Transfer
 ex67.cu
 Solve for
 d2c/dx2 + d2c/dy2 + 1 = 0
 With the boundary conditions of c = 0 
 along lines of x=1,-1, and y=1, and -1

 Jacobi Method with CUDA
 */
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cuda_runtime.h>

#define MAXTHREADS_BLOCK 512 /* maximum threads in a block */

__global__ void jacobi_step2d(int num, double *phi, double *phi0, double *rhs) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;

	if (n < num * num) {
		int i = n % num; /* i is residual */
		int j = n / num; /* j is division */
		if ((i == 0) || /*skip left boundary*/
		(i == num - 1) || /*skip right boundary*/
		(j == 0) || /*skip bottom boundary*/
		(j == num - 1)) { /*skip top boundary*/
			phi[n] = 0;
		} else {
			int ie = n + 1;
			int iw = n - 1;
			int in = n + num;
			int is = n - num;
			phi[n] = -0.25
					* (rhs[n] - phi0[ie] - phi0[iw] - phi0[in] - phi0[is]);
		}
	}
}

__global__ void residual2d(int num, double *phi, double *rhs, double *rsd) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
	if (n < num * num) {
		int i = n % num; /* i is residual */
		int j = n / num; /* j is division */
		if ((i == 0) || /*skip left boundary*/
		(i == num - 1) || /*skip right boundary*/
		(j == 0) || /*skip bottom boundary*/
		(j == num - 1)) { /*skip top boundary*/
			rsd[n] = 0.0;
		} else {
			int ie = n + 1;
			int iw = n - 1;
			int in = n + num;
			int is = n - num;
			double r = rhs[n] - phi[ie] - phi[iw] - phi[in] - phi[is]
					+ 4.0 * phi[n];
			rsd[n] = r * r;
		}
	}
}

__global__ void reduce_sum_device(int cn, double *g_data) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x; // global ID
	int nl = threadIdx.x;   // thread ID in block
	__shared__ double s_data[512]; // use shared memory

	/* copy to shared memory */
	if (n >= cn)
		s_data[nl] = 0.0;
	else
		s_data[nl] = g_data[n];

	__syncthreads(); // wait until all threads in block copy data

	if (nl < 256)
		s_data[nl] += s_data[nl + 256];
	__syncthreads();
	if (nl < 128)
		s_data[nl] += s_data[nl + 128];
	__syncthreads();
	if (nl < 64)
		s_data[nl] += s_data[nl + 64];
	__syncthreads();
	if (nl < 32)
		s_data[nl] += s_data[nl + 32];
	__syncthreads();
	if (nl < 16)
		s_data[nl] += s_data[nl + 16];
	__syncthreads();
	if (nl < 8)
		s_data[nl] += s_data[nl + 8];
	__syncthreads();
	if (nl < 4)
		s_data[nl] += s_data[nl + 4];
	__syncthreads();
	if (nl < 2)
		s_data[nl] += s_data[nl + 2];
	__syncthreads();
	if (nl < 1)
		s_data[nl] += s_data[nl + 1];
	__syncthreads();

	/* store on global memory */
	if (nl == 0)
		g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

__host__ double reduce_sum(int si, double *g_data) {
	// si : size of array g_data[]
	// This host kernel computes $\Sigma^{si-1}_{i=0} g\_data[i]$
	// The result is stored in g_data[0]
	int n = si;
	while (n != 1) {
		int bl = n / 512 + 1;    // Number of Blocks
		int bx = min(bl, 512);    // Blocks in x-coordinate
		int by = bl / 512 + 1;   // Blocks in y-coordinate
		dim3 dimblock(bx, by, 1);  // z-coordinate is 1
		reduce_sum_device<<<dimblock,512>>>(n,g_data);

		n = bl; // each block recudes to 1
	}

	double ret;
	cudaMemcpy(&ret, g_data, sizeof(double), cudaMemcpyDeviceToHost);
	return ret;
}

/*
 MAIN 
 */
int main(int argc, char** argv) {
	if (argc < 2) {
		std::cout << "Number of Grid Points [N]\n";
		std::cout << "Input [N]\n";
		return 0;
	}

	//  set parameters
	int num = std::atoi(argv[1]);
	std::cout << "Grid Size = " << num << "x" << num << std::endl;

	// CUDA Thread Block Grid
	int bl = num * num / MAXTHREADS_BLOCK + 1;
	int bx = min(bl, 512);
	int by = bl / 512 + 1;
	dim3 dimblockJacobi(bx, by, 1);

	// Allocation Array
	double *phi = new double[num * num];
	double *rhs = new double[num * num];

	// Setup
	double dx = 2.0 / (num - 1);
	for (int i = 0; i < num * num; i++) {
		rhs[i] = -dx * dx;
		phi[i] = 0.0;
	}

	// Alloc space on Device
	double *phiDev, *phi0Dev, *rhsDev, *rsdDev;
	cudaMalloc((void **) &phiDev, num * num * sizeof(double));
	cudaMalloc((void **) &phi0Dev, num * num * sizeof(double));
	cudaMalloc((void **) &rhsDev, num * num * sizeof(double));
	cudaMalloc((void **) &rsdDev, num * num * sizeof(double));

	// Copy Host to Device
	cudaMemcpy(phiDev, phi, num * num * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(phi0Dev, phi, num * num * sizeof(double),
			cudaMemcpyHostToDevice);
	cudaMemcpy(rhsDev, rhs, num * num * sizeof(double), cudaMemcpyHostToDevice);

	//
	// Solve with Red & Black Gauss-Seidel Method
	int itc = 0;
	const double tol = 1.0e-8;
	while (1) {
		// update phi with RBGS
		for (int rb = 0; rb < 2; rb++) {
			jacobi_step2d<<<dimblockJacobi,MAXTHREADS_BLOCK>>>(num,phiDev,phi0Dev,rhsDev);
		}

		// Check Convergence
		residual2d<<<dimblockJacobi,MAXTHREADS_BLOCK>>>(num,phiDev,rhsDev,rsdDev);
		double merr = reduce_sum(num * num, rsdDev);
		std::swap(phiDev,phi0Dev);

		itc++;
		if (merr < tol * tol) {
			break;
		}

		if ((itc % 10000) == 0) {
			std::cout << "itc=" << itc << " err=" << std::sqrt(merr) << std::endl;
		}
	}
	std::cout << "Number of Iteration=" << itc << std::endl;

	/* get resuts from device */
	cudaMemcpy(phi, phiDev, num * num * sizeof(double), cudaMemcpyDeviceToHost);

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx67.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();
}
