//
//  Large Scale Computing
//  1D Heat/Mass Transfer with Jacobi Method
//  ex51.cpp 
//  use MPI
//
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <mpi.h>

int main(int argc, char **argv) {
	// Initialize MPI Library
	MPI::Init(argc, argv);

	// get myid and # of processors 
	int numproc = MPI::COMM_WORLD.Get_size();
	int myid = MPI::COMM_WORLD.Get_rank();

	if (argc < 5) {
		if (myid == 0) {
			std::cout << argv[0] << " [NUM] [A] [B] [D]\n";
		}
		MPI::Finalize();
		return 0;
	}

	//set parameters from commandline
	int num = std::atoi(argv[1]);
	double a = std::atof(argv[2]);
	double b = std::atof(argv[3]);
	double d = std::atof(argv[4]);

	if (myid == 0) {
		std::cout << "num=" << num << " A=" << a << " B=" << b << " D=" << d << std::endl;
	}

	/* divide domain into # of processes */
	int mystart = (num / numproc) * myid;
	int myend;
	if (num % numproc > myid) {
		mystart += myid;
		myend = mystart + (num / numproc) + 1;
	} else {
		mystart += num % numproc;
		myend = mystart + (num / numproc);
	}
	int mysize = myend - mystart;

	// wait until all processors come here 
	MPI::COMM_WORLD.Barrier();
	std::cout << "P" << myid << ":" << mystart << "~" << myend << " size=" << mysize << std::endl;

	/* Allocation of Arrays */
	double *phi = new double[mysize + 2];
	double *phi0 = new double[mysize + 2];
	double *rhs = new double[mysize + 2];

	// set RHS 
	double dx = 1.0 / (num - 1);
	for (int i = mystart; i < myend; i++) {
		rhs[i - mystart + 1] = -dx * dx / d * (dx * i);
	}

	// initialize phi
	for (int i = 0; i < mysize + 2; i++) {
		phi[i] = phi0[i] = 0;
	}

	// Boundary Conditions
	if (myid == 0) {
		phi[1] = phi0[1] = a;
	}
	if (myid == numproc - 1) {
		phi[mysize] = phi0[mysize] = b;
	}

	// Solve with Jacobi Method
	int itc = 0;
	const double tol = 1.0e-8;
	while (1) {
		// update phi 
		for (int i = mystart; i < myend; i++) {
			if ((i == 0) || (i == num - 1)) {
				// skip both sides
				continue;
			}
			int ii = i - mystart + 1;
			phi[ii] = -0.5 * (rhs[ii] - phi0[ii + 1] - phi0[ii - 1]);
		}

		// send & receive with non-blocking communication
		MPI::Request SendL;
		MPI::Request SendR;
		MPI::Request RecvL;
		MPI::Request RecvR;
		for (int left_proc = 0; left_proc < numproc - 1; left_proc++) {
			int right_proc = left_proc + 1;
			if (myid == left_proc) {
				SendR = MPI::COMM_WORLD.Isend(&phi[mysize], 1, MPI::DOUBLE, right_proc, 11);
				RecvR = MPI::COMM_WORLD.Irecv(&phi0[mysize + 1], 1, MPI::DOUBLE, right_proc, 22);
			}
			if (myid == right_proc) {
				RecvL = MPI::COMM_WORLD.Irecv(&phi0[0], 1, MPI::DOUBLE, left_proc, 11);
				SendL = MPI::COMM_WORLD.Isend(&phi[1], 1, MPI::DOUBLE, left_proc, 22);
			}
		}

		// update phi0
		for (int i = mystart; i < myend; i++) {
			int ii = i - mystart + 1;
			phi0[ii] = phi[ii];
		}

		itc++;

		/* Wait until send/receive complete */
		if (myid < numproc - 1) {
			SendR.Wait();
			RecvR.Wait();
		}
		if (myid > 0) {
			RecvL.Wait();
			SendL.Wait();
		}

		// Check Convergence
		double merr = 0.0;
		for (int i = mystart; i < myend; i++) {
			int ii = i - mystart + 1;
			if ((i == 0) || (i == num - 1)) {
				// skip both sides
				continue;
			}

			double r = rhs[ii] - phi0[ii + 1] - phi0[ii - 1] + 2.0 * phi0[ii];
			merr += r * r;
		}

		/* if converged exit */
		/* sum up merr and store in err on all processes */
		double err = 0.0;
		MPI::COMM_WORLD.Allreduce(&merr, &err, 1, MPI::DOUBLE, MPI::SUM);
		if (err < tol * tol) {
			break;
		}

		if ((myid == 0) && (itc % 10000 == 0)) {
			std::cout << "itc=" << itc << " err=" << std::sqrt(err) << std::endl;
		}
	}
	if (myid == 0) {
		std::cout << "Number of Iteration=" << itc << std::endl;
	}

	// Output Result
	std::ostringstream fileName;
	fileName << "resEx51_" << std::setfill('0') << std::setw(2) << myid << ".dat";
	std::ofstream ofile;
	ofile.open((fileName.str()).c_str());
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = mystart; i < myend; i++) {
		int ii = i - mystart + 1;
		ofile << dx * i << " " << phi[ii] << std::endl;
	}
	ofile.close();

	// wait until all processors come here 
	MPI::COMM_WORLD.Barrier();
	std::cout << "P" << myid << " Done\n";
	MPI::Finalize();
	return 0;
}
