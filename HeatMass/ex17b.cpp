//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex17b.cpp : Parallel Jacobi
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
// Jacobi with OpenMP
//
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

int main(int argc, char **argv){
  int num;
  std::cout << "Number of Points : ";
  std::cin >> num;

  if (std::cin.fail() || (num <= 3)) {
    std::cout << "BYE\n";
    return 0;
  }
  double *phi = new double[num * num];
  double *phi0 = new double[num * num];  

  /*assuming dx = dy : domain is 2x2 */
  double dx = 2.0 / (num - 1);

  /* Initialize */
  for (int n = 0 ; n < num * num ; n++){
    phi[n] = 0.0;
    phi0[n] = 0.0;
  }
  
  /* computing for phi with Jacobi Method */
  int itc = 0;
  const double tol = 1.0e-8;
  double start = clock();
  double omp_start = omp_get_wtime();
  double err;
#pragma omp parallel
  {
    while(1){
#pragma omp for schedule(runtime) collapse(2)
      for (int i = 1 ; i < num - 1; i++){
	for (int j = 1 ; j < num - 1; j++){
	  int n = i + j * num;
	  int ie = i + 1 + j * num;
	  int iw = i - 1 + j * num;
	  int in = i + (j + 1) * num;
	  int is = i + (j - 1) * num;
	  phi[n] = -0.25 * (-dx * dx - phi0[ie] - phi0[iw] - phi0[in] - phi0[is]);
	}
      }
#pragma omp single
      err = 0.0;
      //#pragma omp barrier
#pragma omp for schedule(runtime) collapse(2) reduction(+:err)
      for (int i = 1 ; i < num - 1; i++){
	for (int j = 1 ; j < num - 1; j++){
	  int n = i + j * num;
	  int ie = i + 1 + j * num;
	  int iw = i - 1 + j * num;
	  int in = i + (j + 1) * num;
	  int is = i + (j - 1) * num;
	  double r = -dx * dx - phi[ie] - phi[iw] - phi[in] - phi[is] + 4 * phi[n];
	  err += r * r;
	  phi0[n] = phi[n];
	}
      }
#pragma omp single
      {
	itc++;
	if (itc % 1000 == 0){
	  std::cout << "# of Iteration=" << itc << " err=" << std::sqrt(err) << std::endl;
	}
      }
      if (err < tol * tol) break;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  double omp_tcost = omp_get_wtime() - omp_start;
  std::cout << "Number of Iteration=" << itc << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";
  std::cout << "Time cost (WALL CLOCK)= " << omp_tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("ex17b.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0 ;i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      double x = -1.0 + 2.0 * i / (num - 1);
      double y = -1.0 + 2.0 * j / (num - 1);
      ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
    }
  }
  ofile.close();
  
  std::cout << "Done\n";
  return 0;
  printf("Done\n");
}
