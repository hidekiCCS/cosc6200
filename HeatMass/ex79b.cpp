//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex79b.cpp :
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
// Parallel Jacobi

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number of points]\n";
		return 0;
	}

	int num = std::atoi(argv[1]);
	std::cout << "Number of Points=" << num << std::endl;

	double *phi = new double[num * num];
	double *phi0 = new double[num * num];

	double *rhs = new double[num * num];
	double *ap = new double[num * num];
	double *ae = new double[num * num];
	double *aw = new double[num * num];
	double *an = new double[num * num];
	double *as = new double[num * num];

	int *ie = new int[num * num];
	int *iw = new int[num * num];
	int *in = new int[num * num];
	int *is = new int[num * num];

	/*assuming dx = dy : domain is 2x2 */
	double dx = 2.0 / (num - 1);

	/* Initialize */
#pragma omp parallel for schedule(runtime) 
	for (int n = 0; n < num * num; n++) {
		phi[n] = 0.0;
		phi0[n] = 0.0;
	}

#pragma omp parallel for schedule(runtime) 
	for (int n = 0; n < num * num; n++) {
		rhs[n] = -dx * dx;
		ap[n] = -4;
		ae[n] = 1;
		aw[n] = 1;
		an[n] = 1;
		as[n] = 1;
		ie[n] = n + 1;
		iw[n] = n - 1;
		in[n] = n + num;
		is[n] = n - num;
		if (((n % num) == 0) || ((n % num) == (num - 1)) || ((n / num) == 0) || ((n / num) == (num - 1))) {
			rhs[n] = phi[n] * ap[n];
			ae[n] = aw[n] = an[n] = as[n] = 0;
			ie[n] = iw[n] = in[n] = is[n] = n;
		}
	}
#pragma offload_transfer target(mic) \
		in(phi0:length(num * num) alloc_if(1) free_if(0) ), \
		nocopy(phi:length(num * num) alloc_if(1) free_if(0) ), \
		in(ap:length(num * num) alloc_if(1) free_if(0) ), \
		in(ae:length(num * num) alloc_if(1) free_if(0) ), \
		in(aw:length(num * num) alloc_if(1) free_if(0) ), \
		in(an:length(num * num) alloc_if(1) free_if(0) ), \
		in(as:length(num * num) alloc_if(1) free_if(0) ), \
		in(ie:length(num * num) alloc_if(1) free_if(0) ), \
		in(iw:length(num * num) alloc_if(1) free_if(0) ), \
		in(in:length(num * num) alloc_if(1) free_if(0) ), \
		in(is:length(num * num) alloc_if(1) free_if(0) ), \
		in(rhs:length(num * num) alloc_if(1) free_if(0) )

	/* computing for phi with Jacobi Method */
	int itc = 0;
	const double tol = 1.0e-8;
	double start = clock();
	double omp_start = omp_get_wtime();
	double err;
	while (1) {
#pragma offload target(mic) 		\
		out(err) \
		nocopy(phi0:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(phi:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(ap:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(ae:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(aw:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(an:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(as:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(ie:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(iw:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(in:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(is:length(num * num) alloc_if(0) free_if(0) ),\
		nocopy(rhs:length(num * num) alloc_if(0) free_if(0) )
		{
#pragma omp parallel for schedule(runtime)
			for (int n = 0; n < num * num; n++) {
				phi[n] = (rhs[n] - ae[n] * phi0[ie[n]] - aw[n] * phi0[iw[n]] - an[n] * phi0[in[n]] - as[n] * phi0[is[n]]) / ap[n];
			}
			err = 0.0;
#pragma omp parallel for schedule(runtime) reduction(+:err)
			for (int n = 0; n < num * num; n++) {
				double r = rhs[n] - ae[n] * phi[ie[n]] - aw[n] * phi[iw[n]] - an[n] * phi[in[n]] - as[n] * phi[is[n]] - ap[n] * phi[n];
				err += r * r;
				phi0[n] = phi[n];
			}
		}
		itc++;
		if (err < tol * tol) break;
		if (itc % 1000 == 0) {
			std::cout << "# of Iteration=" << itc << " err=" << std::sqrt(err) << std::endl;
		}
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	double omp_tcost = omp_get_wtime() - omp_start;
	std::cout << "Number of Iteration=" << itc << std::endl;
	std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";
	std::cout << "Time cost (WALL CLOCK)= " << omp_tcost << "(sec)\n";

#pragma offload_transfer target(mic) \
		nocopy(phi0:length(num * num) alloc_if(0) free_if(1) ), \
		out(phi:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(ap:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(ae:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(aw:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(an:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(as:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(ie:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(iw:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(in:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(is:length(num * num) alloc_if(0) free_if(1) ), \
		nocopy(rhs:length(num * num) alloc_if(0) free_if(1) )

	// Output Result
	std::ofstream ofile;
	ofile.open("resEx79b.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j < num; j++) {
			double x = -1.0 + 2.0 * i / (num - 1);
			double y = -1.0 + 2.0 * j / (num - 1);
			ofile << x << " " << y << " " << phi[i + num * j] << std::endl;
		}
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
	printf("Done\n");
}
