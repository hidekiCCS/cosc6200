//
// Large Scale Computing
// 2D Heat/Mass Transfer
// ex04.cpp : Analytical Solution 
// d2c/dx2 + d2c/dy2 + 1 = 0
// With boudary conditions of c = 0 
// along x=1,-1, and y=1, and -1
//
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#define TOL 1.0e-10  /* convergence tolerance */

/// Allocate 2D array
double **makeDoubleArray2D(double *&v,int s1,int s2){
  v = new double[s1 * s2];
  double **a = new double*[s1];
  for(int i = 0; i < s1; ++i)
    a[i] = v + s2 * i;

  return a;
}

/// Destroy 2D array
void destroyDoubleArray2D(double *v,double **m){
  delete [] m;
  delete [] v;
}

int main(int argc, char **argv) {
  int num;
  std::cout << "Number of Points : ";
  std::cin >> num;
  
  if (std::cin.fail() || (num <= 3)) {
    std::cout << "BYE\n";
    return 0;
  }
  double *mem;
  double **phi = makeDoubleArray2D(mem,num,num);

  /* -1 <= x <= 1 and -1 <= y <= 1 */
  for (int i = 0 ; i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      /* (x,y) */
      double x = -1.0 + 2.0 * i / (num - 1);
      double y = -1.0 + 2.0 * j / (num - 1);
      
      /* calculate C */
      int n = 1;
      phi[i][j] = 0.0;
      while(1){
	double ch = std::cosh(n * M_PI * 0.5);
	double dc = std::pow(-1.0,(n - 1) * 0.5) / (n * n * n);
	dc *= (1.0 - std::cosh(n * M_PI * y * 0.5) / ch) 
	  * std::cos(n * M_PI * x * 0.5);
	phi[i][j] += dc;
	n += 2;
	if (dc < TOL) break;
      }
      phi[i][j] *= 16.0 / (M_PI * M_PI * M_PI);
    }
  }

  // Output Result
  std::ofstream ofile;
  ofile.open("ex04.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0 ;i < num ; i++){
    for (int j = 0 ; j < num ; j++){
      double x = -1.0 + 2.0 * i / (num - 1);
      double y = -1.0 + 2.0 * j / (num - 1);
      ofile << x << " " << y << " " << phi[i][j] << std::endl;
    }
  }
  ofile.close();

  // cleanup
  destroyDoubleArray2D(mem,phi);
  
  std::cout << "Done\n";
  return 0;
}
