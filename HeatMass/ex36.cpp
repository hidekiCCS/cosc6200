//
//  Large Scale Computing
//  Convective Heat/Mass Transfer
//  ex36.cpp   
//
//  duphi/dx = d2phi/dx2
//
//  Biconjugate gradient stabilized method with Eigen3
//  with iLU preconditioner 

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>

int main(int argc, char **argv){
  if (argc < 6){
    printf("Usage:%s [NUM] [A] [B] [D] [U]\n",argv[0]);
    return 0;
  }
  
  //set parameters 
  int num = std::atoi(argv[1]);
  double a = std::atof(argv[2]);
  double b = std::atof(argv[3]);
  double d = std::atof(argv[4]);
  double u = std::atof(argv[5]);

  std::cout << "num=" << num 
		  << " A=" << a << " B=" << b 
		  << " D=" << d << " U=" << u << std::endl;
  
  // declare Sparse Matrix
  Eigen::SparseMatrix<double> matA(num, num);
  matA.reserve(Eigen::VectorXi::Constant(num,3));
  // vector for RHS
  Eigen::VectorXd rhs(num);
  // vector for solution
  Eigen::VectorXd phi(num);

  // Initialize
  for (int i = 0 ; i < num ; i++){
    phi(i) = 0.0;
    rhs(i) = 0.0;
  }
  double dx = 1.0 / (num - 1);

  // Setup Matrix
  double ae = d / dx + std::max(-u,0.0);
  double aw = d / dx + std::max(u,0.0);
  double ap = ae + aw;
  for (int i = 1 ; i < num - 1; i++){
	  matA.coeffRef(i, i) = -ap;
	  matA.coeffRef(i, i+1) = ae;
	  matA.coeffRef(i, i-1) = aw;
  }
  
  // Boundary Condition
  matA.coeffRef(0, 0) = -ap;
  rhs(0) = a * matA.coeffRef(0, 0);
  matA.coeffRef(num-1, num-1) = -ap;
  rhs(num-1) = b * matA.coeffRef(num-1, num-1);
  
  // solve linear system with BiCSSTAB with Incomplete LU factorization
  const double tol = 1.0e-8;
  double start = clock();
  Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::IncompleteLUT<double> > solver;
  solver.setTolerance(tol);
  solver.compute(matA);
  phi = solver.solve(rhs);
   
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "Number of Iteration=" << solver.iterations()  << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

  // Output Result
  std::ofstream ofile;
  ofile.open("resEx36.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
     ofile << dx * i << " " << phi(i) << std::endl;
  }
  ofile.close();

  std::cout << "Done\n";
  return 0;
}
