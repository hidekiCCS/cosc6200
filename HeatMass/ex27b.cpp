//
//  Large Scale Computing
//  Heat/Mass Transfer
//  ex27.cpp use cg.h cg.cpp
//
// d^2 phi / dx^2 = x
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "cg.h"
#include "CRSformat.h"

int main(int argc, char **argv) {
  if (argc < 5) {
    std::cout << argv[0] << " [number of points] [A] [B] [D]\n";
    return 0;
  }
  int num = std::atoi(argv[1]);
  double a = std::atof(argv[2]);
  double b = std::atof(argv[3]);
  double d = std::atof(argv[4]);

  //  set parameters
  std::cout << "Number of Grid Points : " << num << std::endl;
  std::cout << "Boundary Value at Left : " << a << std::endl;
  std::cout << "Boundary Value at Right : " << b << std::endl;
  std::cout << "Diffusion Coefficient : " << d << std::endl;

  // Allocate Arrays
  double *phi = new double[num];
  double *sol = new double[num - 2];
  double *rhs = new double[num - 2];

  // Boundary values
  phi[0] = a;
  phi[num - 1] = b;

  /* Setup Matrix A and RHS */
  double start = clock();
  double dx = 1.0 / (num - 1);
  int mSize = (num - 2) * (num - 2);
  CRSformat matA(mSize,mSize);
  for (int i = 1; i < num - 1; i++) {
    int ii = i - 1; // index for vector elements

    /* initial guess */
    sol[ii] = phi[i] = 0.0;

    /* set general RHS */
    rhs[ii] = -dx * dx / d * (dx * i);

    /* west */
    if (i == 1) {
      rhs[ii] -= phi[0];
    } else {
      matA.addElement(ii, ii - 1, 1.0);
    }

    /* diagonal Element */
    matA.addElement(ii, ii, -2.0);    

    /* east */
    if (i == num - 2) {
      rhs[ii] -= phi[num - 1];
    } else {
      matA.addElement(ii, ii + 1, 1.0);      
    }
  }

  /* solve with CG */
  const double tol = 1.0e-8;
  int k = solve_cg(num - 2, matA.getNNZ(), matA.getNZVAL(), matA.getColind(), matA.getRowptr(), sol, rhs, tol);
  if (k < 0) {
    std::cout << "calculation failed\n";
    return 0;
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "# of Iteration=" << k << std::endl;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";

  // restore
  for (int i = 1; i < num - 1; i++) {
    int ii = i - 1; // index of vector elements
    phi[i] = sol[ii];
  }

  // Output Result
  std::ofstream ofile;
  ofile.open("res27.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
    ofile << dx * i << " " << phi[i] << std::endl;
  }
  ofile.close();

  std::cout << "Done\n";

  return 0;
}
