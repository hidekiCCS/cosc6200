/*
  Large Scale Computing
  Conjugate Gradinet Method with CUBLAS & CUSPARSE
  Harwell-Boeing Format
*/
int solve_cg_cuda(const int n, const int nnz,
		const double *nzval, const int *colind, const int *rowptr,
		const double *phi, const double *rhs, const double tol);
