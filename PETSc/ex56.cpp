//
//  Large Scale Computing
//  PETSc Mat test
//  ex56.cpp
//  Matrix-Vector Calculation Krylov
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <petsc.h>

int main(int argc, char **argv) {
	/* Initialize PETSc and MPI */
	PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
	// get n from command line arguments
	PetscInt n;
#if PETSC_VERSION_LE(3,1,0)
	PetscTruth flag;
#else
	PetscBool flag;
#endif
#if PETSC_VERSION_LE(3,6,0)
	PetscOptionsGetInt(PETSC_NULL, "-n", &n, &flag);
#else
	PetscOptionsGetInt(NULL, NULL, "-n", &n, &flag);
#endif
	if (PETSC_FALSE == flag){
		PetscPrintf(PETSC_COMM_WORLD, "%s -n [size]\n", argv[0]);
		PetscFinalize();
		return 0;
	}

	/* get # of process and myid, use MPI commands */
	PetscMPIInt numproc, myid;
	MPI_Comm_size(PETSC_COMM_WORLD, &numproc);
	MPI_Comm_rank(PETSC_COMM_WORLD, &myid);

	PetscPrintf(PETSC_COMM_WORLD, "Number of processors = %d\n", numproc);

	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Hello From %d\n", myid);
#if PETSC_VERSION_LE(3,5,0)
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* define metrix */
	Mat A;
#if PETSC_VERSION_LE(3,2,0)
	MatCreateMPIAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, n, n, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &A);
#else
	MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, n, n, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &A);
#endif

	PetscInt mystart, myend;
	MatGetOwnershipRange(A, &mystart, &myend);
	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "proc[%d] %d ~ %d\n", myid, mystart, myend);
#if PETSC_VERSION_LE(3,5,0)
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* set values */
	int *col = new int[n];
	int *row = new int[myend - mystart];
	double *val = new double[n * (myend - mystart)];
	int k = 0;
	for (int i = mystart; i < myend; i++) {
		row[i - mystart] = i;
		for (int j = 0; j < n ; j++) {
			val[k] = i + 100.0 / (1.0 + std::abs(i - j));
			k++;
		}
	}
	for (int j = 0; j < n ; j++) {
		col[j] = j;
	}
	MatSetValues(A, myend - mystart, row, n, col, val, INSERT_VALUES);
	MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
	// can do something
	MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
	//MatView(A, PETSC_VIEWER_STDOUT_WORLD);

	/* define vector */
	Vec x, b, cx;
	VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, n, &x);
	VecDuplicate(x, &b);
	VecDuplicate(x, &cx);
	k = 0;
	for (int i = mystart; i < myend; i++) {
		row[k] = i;
		val[k] = i;
		k++;
	}
	VecSetValues(x, myend - mystart, row, val, INSERT_VALUES);

	VecAssemblyBegin(x);
	// can do something
	VecAssemblyEnd(x);
	//VecView(x, PETSC_VIEWER_STDOUT_WORLD);

	// copy cx <- x
	VecCopy(x, cx);

	/* b = Ax */
	MatMult(A, x, b);

	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Proc %d Ready to solve Ax=b\n", myid);
#if PETSC_VERSION_LE(3,5,0)
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	 Create the linear solver and set various options
	 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/*
	 Create linear solver context
	 */
	KSP ksp; /* linear solver context */
	KSPCreate(PETSC_COMM_WORLD, &ksp);

	/*
	 Set operators. Here the matrix that defines the linear system
	 also serves as the preconditioning matrix.
	 */
#if PETSC_VERSION_LE(3,5,0)
	KSPSetOperators(ksp, A, A, DIFFERENT_NONZERO_PATTERN);
#else
	KSPSetOperators(ksp, A, A);
#endif

	/*
	 Set linear solver defaults for this problem (optional).
	 - By extracting the KSP and PC contexts from the KSP context,
	 we can then directly call any KSP and PC routines to set
	 various options.
	 - The following four statements are optional; all of these
	 parameters could alternatively be specified at runtime via
	 KSPSetFromOptions();
	 */
	PC pc; /* preconditioner context */
	KSPGetPC(ksp, &pc);
	KSPSetTolerances(ksp, 1.e-12, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);

	/*
	 Set runtime options, e.g.,
	 -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
	 These options will override those specified above as long as
	 KSPSetFromOptions() is called _after_ any other customization
	 routines.
	 */
	KSPSetFromOptions(ksp);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	 Solve the linear system
	 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	/*
	 Solve linear system
	 */
	KSPSolve(ksp, b, x);
	//VecView(x, PETSC_VIEWER_STDOUT_WORLD);
	/*
	 View solver info; we could instead use the option -ksp_view to
	 print this info to the screen at the conclusion of KSPSolve().
	 */
	KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);

	/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	 Check solution and clean up
	 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	PetscScalar nrm;
	VecAYPX(cx, -1.0, x);
	VecNorm(cx, NORM_2, &nrm);
	PetscPrintf(PETSC_COMM_WORLD, "|x - x0| = %e\n", nrm);
	/*
	 Free work space.  All PETSc objects should be destroyed when they
	 are no longer needed.
	 */
#if PETSC_VERSION_LE(3,1,0)
	VecDestroy(x);
	VecDestroy(b);
	MatDestroy(A);
	KSPDestroy(ksp);
#else
	VecDestroy(&x);
	VecDestroy(&b);
	MatDestroy(&A);
	KSPDestroy(&ksp);
#endif

	PetscFinalize();
	return 0;
}

