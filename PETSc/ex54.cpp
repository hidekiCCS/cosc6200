//
//  Large Scale Computing
//  PETSc Vec test
//  norm vec-vec scalar-vec calculations
//  ex54.cpp
//
#include <iostream>
#include <petsc.h>

#define VECSIZE 10

int main(int argc,char **argv){
  /* Initialize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);

  /* get # of process and myid, use MPI commands */
  PetscMPIInt numproc,myid;
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d\n",numproc);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Hello From %d\n",myid);
#if PETSC_VERSION_LE(3,5,0)
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif
  
  /* define vector */
  Vec x,y;
  VecCreateMPI(PETSC_COMM_WORLD,PETSC_DECIDE,VECSIZE,&x);
  VecDuplicate(x,&y);  

  /* get start end */
  PetscInt mystart,myend;
  VecGetOwnershipRange(x,&mystart,&myend); 
  
  /* set values */
  for (int i = mystart ; i < myend ; i++){
    VecSetValue(x,i,(double)i, INSERT_VALUES);    
  }
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);
  
  VecSet(y,1.0);
  VecAssemblyBegin(y);
  VecAssemblyEnd(y);

  /* Display vector */
  VecView(x, PETSC_VIEWER_STDOUT_WORLD);

  /* compute vector norm */
  double nrm;
  VecNorm(x,NORM_2,&nrm);
  PetscPrintf(PETSC_COMM_WORLD,"|x| = %e\n",nrm);  

  /* scale */
  VecScale (x, 1.0 / nrm);
  
  /* y = - x + y */
  VecAXPBY(y,-1.0,1.0,x);

  /* Display vector */
  VecView(y, PETSC_VIEWER_STDOUT_WORLD);

  /* free */
#if PETSC_VERSION_LE(3,1,0)
  VecDestroy(x);
  VecDestroy(y);
#else
  VecDestroy(&x);
  VecDestroy(&y);
#endif

  PetscFinalize();
  return 0;
}
