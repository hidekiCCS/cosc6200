//
//  Large Scale Computing
//  PETSc Vec test
//  ex53.cpp
//
#include <iostream>
#include <petsc.h>

#define VECSIZE 10

static char help[] = "Large Scale Computing PETSc Vec TEST\n";

int main(int argc, char **argv) {
	/* Initialize PETSc and MPI */
	PetscInitialize(&argc, &argv, PETSC_NULL, help);

	/* get # of process and myid, use MPI commands This is C style*/
	PetscMPIInt numproc, myid;
	MPI_Comm_size(PETSC_COMM_WORLD, &numproc);
	MPI_Comm_rank(PETSC_COMM_WORLD, &myid);

	PetscPrintf(PETSC_COMM_WORLD, "Number of processors = %d\n", numproc);

	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Hello From %d\n", myid);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD,stdout);
#endif

	// Declare a vector
	Vec x;
	VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, VECSIZE, &x);
	VecSet(x, 0.0); /* zeros */

	/* the last process chages the first element */
	if (myid == numproc - 1) {
		VecSetValue(x, 0, 5.0, INSERT_VALUES);
	}
	/* the first process chages the last element */
	if (myid == 0) {
		VecSetValue(x, VECSIZE - 1, 2.0, INSERT_VALUES);
	}

	/*
	 Assemble vector, using the 2-step process:
	 VecAssemblyBegin(), VecAssemblyEnd()
	 Computations can be done while messages are in transition
	 by placing code between these two statements.
	 */
	VecAssemblyBegin(x);
	/* may do something */
	VecAssemblyEnd(x);

	/* Display vector */
	VecView(x, PETSC_VIEWER_STDOUT_WORLD);

	/* get start end */
	PetscInt mystart, myend;
	VecGetOwnershipRange(x, &mystart, &myend);

	/* print from all */
	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Start=%d end=%d\n", mystart, myend);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD,stdout);
#endif
#if PETSC_VERSION_LE(3,1,0)
	VecDestroy(x);
#else
	VecDestroy(&x);
#endif

	/* size of variable type */
	PetscPrintf(PETSC_COMM_WORLD, "Size of PetscInt =%d\n", sizeof(PetscInt));
	PetscPrintf(PETSC_COMM_WORLD, "Size of PetscScalar =%d\n", sizeof(PetscScalar));
	PetscPrintf(PETSC_COMM_WORLD, "Size of PetscReal =%d\n", sizeof(PetscReal));
	PetscPrintf(PETSC_COMM_WORLD, "Size of int =%d\n", sizeof(int));
	PetscPrintf(PETSC_COMM_WORLD, "Size of float =%d\n", sizeof(float));
	PetscPrintf(PETSC_COMM_WORLD, "Size of double =%d\n", sizeof(double));

	PetscFinalize(); // This also calls MPI_Finalize();
	return 0;
}

