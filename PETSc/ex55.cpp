//
//  Large Scale Computing
//  PETSc Mat test
//  ex55.cpp
//
#include <iostream>
#include <petsc.h>

#define VECSIZE 4

int main(int argc, char **argv) {
	/* Initialize PETSc and MPI */
	PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

	/* get # of process and myid, use MPI commands */
	PetscMPIInt numproc, myid;
	MPI_Comm_size(PETSC_COMM_WORLD, &numproc);
	MPI_Comm_rank(PETSC_COMM_WORLD, &myid);

	PetscPrintf(PETSC_COMM_WORLD, "Number of processors = %d\n", numproc);

	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "Hello From %d\n", myid);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* define matrix */
	Mat mat;
#if PETSC_VERSION_LE(3,2,0)
	MatCreateMPIAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, VECSIZE, VECSIZE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &mat);
#else
	MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, VECSIZE, VECSIZE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &mat);
#endif

	PetscInt mystart, myend;
	MatGetOwnershipRange(mat, &mystart, &myend);
	PetscSynchronizedPrintf(PETSC_COMM_WORLD, "proc[%d] %d ~ %d\n", myid, mystart, myend);
#if PETSC_VERSION_LE(3,5,0)
	PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
	PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);
#endif

	/* set values */
	for (int i = mystart; i < myend; i++) {
		for (int j = 0; j < VECSIZE; j++) {
			MatSetValue(mat, i, j, (double) myid + 1.0, INSERT_VALUES);
		}
	}
	MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);
	// can do something
	MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);

	PetscPrintf(PETSC_COMM_WORLD, "mat=\n");
	MatView(mat, PETSC_VIEWER_STDOUT_WORLD);

	/* add values */
	for (int j = mystart; j < myend; j++) {
		for (int i = 0; i < VECSIZE; i++) {
			MatSetValue(mat, i, j, (double) myid + 1.0, ADD_VALUES);
		}
	}
	MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);
	// can do something
	MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);

	PetscPrintf(PETSC_COMM_WORLD, "mat+mat=\n");
	MatView(mat, PETSC_VIEWER_STDOUT_WORLD);

	/* define vector */
	Vec x, y;
	VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, VECSIZE, &x);
	VecDuplicate(x, &y);
	VecSet(x, 1.0); /* one */
	VecAssemblyBegin(x);
	// can do something
	VecAssemblyEnd(x);

	/* y = mat x */
	MatMult(mat, x, y);

	/* Display vector */
	PetscPrintf(PETSC_COMM_WORLD, "y=\n");
	VecView(y, PETSC_VIEWER_STDOUT_WORLD);

	/* mat2 = mat mat */
	Mat mat2;
#if PETSC_VERSION_LE(3,2,0)
	MatCreateMPIAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, VECSIZE, VECSIZE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &mat2);
#else
	MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, VECSIZE, VECSIZE, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &mat2);
#endif
	MatMatMult(mat, mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &mat2);

	PetscPrintf(PETSC_COMM_WORLD, "mat2=\n");
	MatView(mat2, PETSC_VIEWER_STDOUT_WORLD);

#if PETSC_VERSION_LE(3,1,0)
	VecDestroy(x);
	VecDestroy(y);
	MatDestroy(mat2);
#else
	VecDestroy(&x);
	VecDestroy(&y);
	MatDestroy(&mat2);
#endif
	PetscFinalize();
	return 0;
}

