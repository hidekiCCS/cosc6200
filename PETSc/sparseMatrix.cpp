//
// Large Scale Computing
// 1D Heat/Mass Transfer
// Solve for
// d2c/dx2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
// Solve Linear system  with PETSC
//
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <petsc.h>


// main
int main(int argc, char **argv) {
  /* Initialize PETSc and MPI */
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  /* get # of process and myid, use MPI commands */
  PetscMPIInt numproc, myid;
  MPI_Comm_size(PETSC_COMM_WORLD, &numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD, &myid);

  // SET  PARAMETERS
  const int num = 7; // number of node points is 7
  const double a = 1; // phi at left end is 1
  const double b = 2; // phi at right end is 2
  const double d = 0.1; // Diffusion coefficient is 0.1

  // assuming a uniform gird
  double dx = 1.0 / (num - 1);

  // declare Sparse Matrix
  int size = num - 2; // size of inner points is 5

  /* define metrix */
  Mat matA;
  MatCreateAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, size, size, PETSC_DECIDE, NULL, PETSC_DECIDE, NULL, &matA);
  
  
  PetscInt mystart, myend;
  MatGetOwnershipRange(matA, &mystart, &myend);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD, "proc[%d] %d ~ %d\n", myid, mystart, myend);
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);

  /* define vector */
  PetscInt mysize = myend - mystart;
  Vec sol, rhs;
  VecCreateMPI(PETSC_COMM_WORLD, mysize, size, &sol);
  VecCreateMPI(PETSC_COMM_WORLD, mysize, size, &rhs);
  
  // set up Matrix and RHS
  // Let RANK0 to do this
  if (myid == 0){
    VecSetValue(rhs, 0, -dx * dx / d - a, INSERT_VALUES);
    VecSetValue(rhs, 1, -dx * dx / d, INSERT_VALUES);
    VecSetValue(rhs, 2, -dx * dx / d, INSERT_VALUES);
    VecSetValue(rhs, 3, -dx * dx / d, INSERT_VALUES);
    VecSetValue(rhs, 4, -dx * dx / d - b, INSERT_VALUES);    

    MatSetValue(matA, 0, 0, -2.0, INSERT_VALUES);
    MatSetValue(matA, 1, 1, -2.0, INSERT_VALUES);
    MatSetValue(matA, 2, 2, -2.0, INSERT_VALUES);
    MatSetValue(matA, 3, 3, -2.0, INSERT_VALUES);
    MatSetValue(matA, 4, 4, -2.0, INSERT_VALUES);

    MatSetValue(matA, 0, 1, 1.0, INSERT_VALUES);
    MatSetValue(matA, 1, 2, 1.0, INSERT_VALUES);
    MatSetValue(matA, 2, 3, 1.0, INSERT_VALUES);
    MatSetValue(matA, 3, 4, 1.0, INSERT_VALUES);
    MatSetValue(matA, 1, 0, 1.0, INSERT_VALUES);
    MatSetValue(matA, 2, 1, 1.0, INSERT_VALUES);
    MatSetValue(matA, 3, 2, 1.0, INSERT_VALUES);
    MatSetValue(matA, 4, 3, 1.0, INSERT_VALUES);            
  }
  VecAssemblyBegin(rhs);
  MatAssemblyBegin(matA, MAT_FINAL_ASSEMBLY);
  //
  // Communications are in background. We can do some computations during this time period but nothing to do :(
  //
  VecAssemblyEnd(rhs);  
  MatAssemblyEnd(matA, MAT_FINAL_ASSEMBLY);

  
  // solve linear system 
  KSP ksp; /* linear solver context */
  KSPCreate(PETSC_COMM_WORLD, &ksp);
  KSPSetOperators(ksp, matA, matA);

  const double tol = 1.0e-8;
  PC pc; /* preconditioner context */
  KSPGetPC(ksp, &pc);
  KSPSetTolerances(ksp, tol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);


  /*
    Solve linear system
  */
  KSPSolve(ksp, rhs, sol);


  /*
    View solver info; we could instead use the option -ksp_view to
    print this info to the screen at the conclusion of KSPSolve().
  */
  KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);

  // Get portion of the vector data
  PetscScalar *sol_array;
  VecGetArray(sol,&sol_array);

  // Output Result  
  std::ostringstream fileName;
  fileName << "sol_" << std::setfill('0') << std::setw(2) << myid << ".dat";
  std::ofstream ofile;
  ofile.open((fileName.str()).c_str());
  ofile << std::setprecision(16);
  ofile << std::scientific;
  if (myid == 0) ofile << 0.0 << " " << a << std::endl; // left end
  for (int i = mystart; i < myend; i++) {
    int ii = i - mystart;
    ofile << dx * (i + 1) << " " << sol_array[ii] << std::endl;
  }
  if (myid == numproc-1) ofile << 1.0 << " " << b << std::endl; // right end  
  ofile.close();

  // Restore  the vector data  
  VecRestoreArray(sol,&sol_array);

  // clean-up
  VecDestroy(&sol);
  VecDestroy(&rhs);
  MatDestroy(&matA);
  KSPDestroy(&ksp);

  PetscFinalize();
  return 0;
}
