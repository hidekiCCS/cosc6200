/*
 * oneDiff1.cpp
 *
 *  Created on: Nov 1, 2017
 *      Author: fuji
 */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <kinsol/kinsol.h>
#include <kinsol/kinsol_dense.h>
#include <kinsol/kinsol_spgmr.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

typedef struct UserData{
  int num;
  double a,b;
  double al;
  double *rhs;
};

static int residuals_function(N_Vector phi_con, N_Vector residuals_con, void *user_data)
{
	realtype *phi = NV_DATA_S(phi_con);
	realtype *residuals = NV_DATA_S(residuals_con);

	UserData *data = (UserData *)user_data;
	int num = data->num;
	residuals[0] = data->a - phi[0];
	residuals[num-1] = data->b - phi[num-1];

	for (int i = 1 ; i < num-1 ; i++){
		residuals[i] = data->rhs[i] - phi[i + 1] - phi[i - 1] + 2.0 * phi[i];
	}
	return 0;
}

int main(int argc, char **argv) {
	int num;
	double a, b, al;
	//  set parameters
	std::cout << "Number of Grid Points [N]\n";
	std::cout << "Boundary Value at Left [A]\n";
	std::cout << "Boundary Value at Right [B]\n";
	std::cout << "Diffusion term factor [alpha]\n";
	std::cout << "Input [N] [A] [B] [alpha] : ";
	std::cin >> num >> a >> b >> al;

	if (std::cin.fail()) {
		std::cout << "BYE\n";
		return 0;
	}

	// Allocation Array
	double *phi = new double[num];
	double *rhs = new double[num];

	// Setup
	double dx = 1.0 / (num - 1);
	for (int i = 0; i < num; i++) {
		rhs[i] = -dx * dx * (dx * i);
		phi[i] = 0.0;
	}

	// Boundary Conditions
	phi[0] = a;
	phi[num - 1] = b;

	// Prepare for Sundials
	UserData *data = new UserData;
	data->num = num;
	data->a = a;
	data->b = b;
	data->al = al;
	data->rhs = rhs;

	void *kmem = KINCreate();
	KINSetUserData(kmem, data);
	realtype fnormtol = 1.0e-8;
	realtype scsteptol = 1.0e-8;
	KINSetFuncNormTol(kmem, fnormtol);
	KINSetScaledStepTol(kmem, scsteptol);
	N_Vector nv_phi = N_VNew_Serial(num);
	for (int i = 0; i < num; i++) {
		NV_Ith_S(nv_phi,i) = phi[i];
	}
	KINInit(kmem,residuals_function,nv_phi);

	// Use GMRES
	int maxl = 15;
	int maxlrst = 2;
	KINSpgmr(kmem, maxl);
	KINSpilsSetMaxRestarts(kmem, maxlrst);

	int mset=1;
	KINSetMaxSetupCalls(kmem, mset);

	// solve
	N_Vector scale = N_VNew_Serial(num);
	N_VConst_Serial(1.0,scale); /* no scaling */
	int flag = KINSol(kmem, nv_phi, KIN_LINESEARCH, scale, scale);

	// extract result
	for (int i = 0; i < num; i++) {
		phi[i] = NV_Ith_S(nv_phi,i);
	}

	// Output Result
	std::ofstream ofile;
	ofile.open("onediff1.dat");
	ofile << std::setprecision(16);
	ofile << std::scientific;
	for (int i = 0; i < num; i++) {
		ofile << dx * i << " " << phi[i] << std::endl;
	}
	ofile.close();

	std::cout << "Done\n";
	return 0;
}
