#include <iostream>
#include <omp.h>
int main (int argc, char *argv[]) {
  int n = 0;
#pragma omp parallel for
  for (int i = 0 ; i <= 100 ; i++){
    n += i;
  }
  std::cout << "n=" << n << std::endl;
}
