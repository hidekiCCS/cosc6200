#include <iostream>
#include <iomanip>
#include <omp.h>

double myFunction(double x){
  return 4 / (1 + x * x);
}

int main(int argc,char **argv){
  if (argc < 2){
    std::cout << argv[0] << "[N]\n";
    return 0;
  }
  
  int n = atoi(argv[1]);

  double p = 0;
#pragma omp parallel
  {
#pragma omp for reduction(+:p)
    for (int i = 0 ; i < n ; i++){
      p += myFunction((i + 0.5) / n) / n;
    }
  }
  std::cout << "P=" << std::setprecision(15) << p << std::endl;

  return 0;
}
