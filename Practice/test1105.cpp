//
#include <iostream>
#include <cstdlib>
#include <cmath>

int main(int argc, char **argv) {
	//Let num = 10000
	const int num = 10000;

	//Allocate two arrays of size num, A and B
	double *aArray = new double[num];
	double *bArray = new double[num];

	//Rank 0 assigns random values to those arrays.
	for (int i = 0; i < num; i++) {
		aArray[i] = static_cast<double>(rand()) / RAND_MAX;
		bArray[i] = static_cast<double>(rand()) / RAND_MAX;
	}

	//Rank 0 sends array elements to all other ranks.
	double *cArray = new double[num];
	for (int i = 0; i < num; i++) {
		cArray[i] = 0.0;
		for (int j = 0; j < num; j++) {
			cArray[i] += aArray[i] * bArray[j];
		}
	}

	//Compute |C| . Rank 0 gets the results.
	double cNorm = 0.0;
	for (int i = 0; i < num; i++) {
		cNorm += cArray[i] * cArray[i];
	}
	cNorm = std::sqrt(cNorm);
	std::cout << "|C|=" << cNorm << std::endl;

	delete[] cArray;
	delete[] bArray;
	delete[] aArray;
	return 0;
}
