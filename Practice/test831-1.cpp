#include <iostream>

int main(int argc, char **argv){
  double vec1[] = {1,2,3};
  double vec2[] = {4,5,6};
  double prod = 0;
  for (int i = 0 ; i < 3 ; i++){
    prod += vec1[i] * vec2[i];
  }
  std::cout << "prod=" << prod << std::endl;
  return 0;
}
