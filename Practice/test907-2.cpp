// 
// Smallest float 
// 
#include <iostream> 
#include <iomanip> 
#include <cmath>  
int main(int argc,char **argv) {
	std::cout << std::scientific;
	std::cout << std::setprecision(7);
	for (int i = 0 ; i < 152 ; i++){
		float a = std::pow(2.0,-i);
		std::cout << "2^" << -i << "=" << a << std::endl;
	}
	return 0;
}
