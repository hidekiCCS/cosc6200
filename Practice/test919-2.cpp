//
//
#include <iostream>
#include <cmath>
#include <omp.h>

int main(int argc, char **argv){
  double start = clock();
  double omp_start = omp_get_wtime();
#pragma omp parallel for schedule(dynamic)
  for (int i = 0 ; i < 1000 ; i++){
    for (int j = 0 ; j < (i + 1) * 1000 ; j++){
      // compute something....
      double a = std::exp(static_cast<double>(i) / j);
    }
  }
    double tcost = (clock() - start) / CLOCKS_PER_SEC;
  double omp_tcost = omp_get_wtime() - omp_start;
  std::cout << "Time cost (CPU) = " << tcost << "(sec)\n";
  std::cout << "Time cost (WALL CLOCK)= " << omp_tcost << "(sec)\n";
  std::cout << "End\n";
  return 0;
}
