//
//  Jacobi
//
//  Created by Hideki Fujioka on 9/11/18.
//
#include <iostream>
#include <cmath>
#include <algorithm>

int main(int argc, const char * argv[]) {
  if (argc < 2) {
    std::cout << argv[0] << " [size]\n";
    return 0;
  }

  int size = std::atoi(argv[1]);
  std::cout << "Size=" << size << std::endl;
    
  // allocation
  double *xvec = new double[size];
  double *bvec = new double[size];
  double *amatBlock = new double[size * size];
    
  // setup matrix A lower elements
  for (int i = 0; i < size; i++) {
    double diag = 0.0;
    for (int j = 0; j < size; j++) {
      double elm = 1 + (rand() % 100) * 0.01;
      amatBlock[i * size + j] = elm;
      diag += elm;
    }
    // |A_ii| >= sum(A_ij) i != j
    amatBlock[i * size + i] = -diag;
  }
    
  // setup vector x elements
  for (int i = 0; i < size; i++) {
    xvec[i] = i;
  }
    
  // compute rhs
  std::fill(bvec,bvec+size,0.0);
  for (int i = 0; i < size ; i++) {
    for (int j = 0; j < size ; j++) {
      bvec[i] += amatBlock[i * size + j] * xvec[j];
    }
  }
    
  // compute sol = A^-1 b with JACOBI
  double *sol = new double[size]; // iteration step k+1
  double *sol0 = new double[size];// iteration step k
  std::fill(sol,sol+size,0.0);
  std::fill(sol0,sol0+size,0.0);
  const double tol = 1.0e-8;
  int count = 0;
  double start = clock();

  while(1){
    // JACOBI step x(k+1) = A_ii^-1 (b - A_ij (i!=j) x(k))
    for (int i = 0 ; i < size ; i++){
      double sumAs = 0.0;
      for (int j = 0 ; j < size ; j++){
	int mask = (i == j) ?  0:1;
	sumAs += mask * amatBlock[i * size + j] * sol0[j];
      }
      sol[i] = (bvec[i] - sumAs) / amatBlock[i * size + i];
    }

    // compute residual |r| = |b-Ax|
    double r = 0.0;
    for (int i = 0 ; i < size ; i++){
      double sumAs = bvec[i];
      for (int j = 0 ; j < size ; j++){
	sumAs -= amatBlock[i * size + j] * sol[j];
      }
      r += sumAs * sumAs;
      sol0[i] = sol[i];
    }      
    r = std::sqrt(r);
    count++;
    if (r < tol) break;

    if (count % 1000 == 0){
      std::cout << "count=" << count << " |r|=" << r << std::endl;
    }
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << "itr=" << count << std::endl;
  std::cout << "Time cost = " << tcost << "(sec)\n";
    
  // check solution
  double r = 0.0;
  for (int i = 0; i < size; i++){
    double dr = sol[i] - xvec[i];
    r += dr * dr;
  }
  r = std::sqrt(r);
  std::cout << "|x - x0|=" << r << std::endl;
  return 0;
}
