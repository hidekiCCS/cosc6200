#include <iostream>
#include <mpi.h>
#include <time.h>       /* time */

int main(int argc,char **argv){
  // Initialize
  MPI::Init(argc, argv);

  // get myid and # of processors
  int numproc = MPI::COMM_WORLD.Get_size();
  int myid = MPI::COMM_WORLD.Get_rank();

  if (argc < 2){
    std::cout << argv[0] << " number\n";
    MPI::Finalize();
    return 0;
  }

  /* initialize random seed: */
  srand (time(NULL));
  int num;
  num = atoi(argv[1]);
  int count = 0;
  for (int i = 0 ; i < num ; i++){
    double x = static_cast<double>(rand()) / RAND_MAX;
    double y = static_cast<double>(rand()) / RAND_MAX;
    if (x * x + y * y <= 1.0){
      count++;
    }
  }

  int total_count; 
  MPI::COMM_WORLD.Reduce(&count, &total_count, 1, MPI::INT, MPI::SUM, 0);  

  if (myid == 0){
    double res = 4 * static_cast<double>(total_count) / (numproc * num);
    //double res = 4.0 * count / num;  
    std::cout << "Result=" << res << std::endl;
  }
  MPI::Finalize();		   
  return 0;
}
