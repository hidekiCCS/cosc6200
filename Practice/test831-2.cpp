#include <iostream>
#include <iomanip>

double **makeDoubleArray2D(double *&v,int s1,int s2){
  v = new double[s1 * s2];
  double **a = new double*[s1];
  for(int i = 0; i < s1; ++i)
    a[i] = v + s2 * i;

  return a;
}

void destroyDoubleArray2D(double *&v,double **m){
  delete [] v;
}

// test routine
int main(int argc, const char * argv[]) {
    const int size1 = 3;
    const int size2 = 4;
    double *vect;
    double **mat2d = makeDoubleArray2D(vect,size1,size2);
    
    // set
    for (int i = 0 ; i < size1 ; i++){
        for (int j = 0 ; j < size2 ; j++){
            mat2d[i][j] = static_cast<double>(i + 1) / (j + 1);
        }
    }
    
    // print
    std::cout << "matrix\n";
    std::cout.setf(std::ios::scientific);
    std::cout.precision(2);
    for (int i = 0 ; i < size1 ; i++){
        for (int j = 0 ; j < size2 ; j++){
            std::cout << std::setw(9) << mat2d[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << "vector\n";
    for (int i = 0 ; i < size1 * size2 ; i++){
        std::cout << vect[i] << std::endl;
    }
    
    destroyDoubleArray2D(vect,mat2d);
    return 0;
}
