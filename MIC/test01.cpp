///
/// TEST
///
#include <iostream>
#include <cmath>
#include <omp.h>

#define N 100

int main(const int argc, const char** argv) {
  omp_set_nested(1);
  int num_dev = omp_get_num_devices();
  std::cout << "number of devices " << num_dev << std::endl;
  int a[10] = { 0 };

#pragma omp parallel num_threads(num_dev + 1)
#pragma omp single
  {
    for (int dev = 0; dev < num_dev + 1; dev++) {
#pragma omp task firstprivate(dev)
      {
#pragma omp target if (dev != num_dev) device(dev) map(to:a) map(from:a[dev:dev+1])
	{
#pragma omp parallel
	  {
#pragma omp master
	    a[dev] = omp_get_num_threads();
	  }
	}
      }
    }
  }
  for (int i = 0; i < num_dev + 1; i++) {
    std::cout << a[i] << std::endl;
  }

  return 0;
}
