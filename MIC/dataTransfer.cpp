#include <iostream>
#include <omp.h>

int main( void ) {
	double a[500000];
	// static arrays are allocated on the stack; literal here is important
	int i;
#pragma omp target device(0) map(from:a)
#pragma omp parallel for
	for ( i=0; i<500000; i++ ) { 
		a[i] = (double)i;
	} 
}
