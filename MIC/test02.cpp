///
/// OMP offload test
///

#include <iostream>
#include <cmath>
#include <omp.h>

// define the number of nodes to use in test
#define N_NODES 40000
#define N_STEPS 100

//
//
#pragma omp declare target
void compute_velocity(const double *x, const double *f, const std::size_t &x_start, const std::size_t &x_end, const std::size_t &nx, const double &eps, double *u)
{
  const double eps2 = eps*eps;

  #pragma omp parallel for default(shared) //schedule(dynamic)
  for (std::size_t i=0 ; i<x_end - x_start; ++i) {
    u[3*i] = 0;
    u[3*i+1] = 0;
    u[3*i+2] = 0;

    for (std::size_t j=0; j<nx; ++j) {
      double dx = x[3*(x_start + i)] - x[3*j];
      double dy = x[3*(x_start + i)+1] - x[3*j+1];
      double dz = x[3*(x_start + i)+2] - x[3*j+2];

      double r2 = dx*dx + dy*dy + dz*dz;
      double r2p2eps = r2 + 2*eps2;
      double den = 8*M_PI*std::pow(r2+eps2, 1.5);

      double fdotr = dz*f[3*j] + dy*f[3*j+1] + dz*f[3*j+2];

      u[3*i] += (r2p2eps*f[3*j] + dz*fdotr) / den;
      u[3*i+1] += (r2p2eps*f[3*j+1] + dy*fdotr) / den;
      u[3*i+2] += (r2p2eps*f[3*j+2] + dz*fdotr) / den;
    }
  }
}
#pragma omp end declare target

//
//
//
int main(const int argc, const char** argv) {
	const size_t n_nodes = N_NODES;

	int n_threads;
#pragma omp parallel
#pragma omp master
	n_threads = omp_get_num_threads();

	std::cout << "CPU using " << n_threads << " threads " << std::endl;

	const int num_dev = omp_get_num_devices();
	std::cout << "number of devices : " << num_dev << std::endl;

	int n_device_threads[10];
	for (int dev = 0; dev < num_dev ; dev++){
#pragma omp target device(dev) map(to:n_device_threads) map(from:n_device_threads[dev:dev+1])
#pragma omp parallel
#pragma omp master
		n_device_threads[dev] = omp_get_num_threads();
	}
	for (int dev = 0; dev < num_dev ; dev++){
		std::cout << "Device " << dev << " using " << n_device_threads[dev] << " threads " << std::endl;
	}


	// declare memory for node locations and forces
	double *x = (double*) _mm_malloc(sizeof(double)*3*N_NODES, 64);
	double *u = (double*) _mm_malloc(sizeof(double)*3*N_NODES, 64);
	double *f = (double*) _mm_malloc(sizeof(double)*3*N_NODES, 64);
	double *u_mix = (double*) _mm_malloc(sizeof(double)*3*N_NODES, 64);

	// initialize all the positions to be on a circle (it doesn't really
	// matter where they are as long as they're all distinct)
	// Set u=0 f = 1;
#pragma omp parallel for default(shared)
	for (std::size_t i=0; i<N_NODES; ++i) {
		x[3*i] = std::cos(i*M_PI/((double)N_NODES));
	    x[3*i+1] = std::sin(i*M_PI/((double)N_NODES));
	    x[3*i+2] = 0;

	    for (int j=0; j<3; ++j) {
	    	f[3*i+j] = 1.0;
	    	u[3*i+j] = 0.0;
	    	u_mix[3*i+j] = 0.0;
	    }
	}

	// pick some stokeslet parameters (again these don't really matter because it's
	// a speed test ...)
	const double h = 2*M_PI/((double)N_NODES);
	const double eps = 0.7*h;
	const double eps2 = eps*eps;

	/*************************************************************************************

	                                 CPU TEST

	**************************************************************************************/

	// now compute the velocities on the host ...
	std::cout << std::endl << "Computing velocities on host" << std::endl;
	double t_host = omp_get_wtime();

	for (int k=0 ; k < N_STEPS ; ++k) {
		compute_velocity(x, f, 0, n_nodes, n_nodes, eps, u);
	}

	t_host = omp_get_wtime() - t_host;
	std::cout << "Finished host computation, time = " << t_host << std::endl;

	/*****************************************************************************************************

	                      NOW TRY TO MIX MIC AND CPU COMPUTATIONS

	 *****************************************************************************************************/
	std::cout << std::endl << "Computing velocities on devices and CPU" << std::endl;
	double t_mic_cpu = omp_get_wtime();

	const int total_devices = num_dev + 1;
	omp_set_nested(1); // nested on
#pragma omp parallel num_threads(total_devices)
#pragma omp single
	{
		for (int dev = 0; dev < total_devices ; dev++){
#pragma omp task firstprivate(dev)
			{
				const int my_start = n_nodes * dev / total_devices;
				const int my_end = n_nodes * (dev+1) / total_devices;
				//std::cout << "Device " << dev << " takes from " << my_start << " to " << my_end << std::endl;

				double *u_mix_d = (double*) _mm_malloc(sizeof(double)*3*(my_end - my_start), 64);
				std::copy(&u_mix[3*my_start],&u_mix[3*my_start]+3*(my_end - my_start),u_mix_d);

				// allocate space on devices ("dev == num_dev" is CPU)
#pragma omp target enter data if (dev != num_dev) device(dev) map(alloc:x[:3*n_nodes],f[:3*n_nodes],u_mix_d[:3*(my_end - my_start)])
				for (int k=0 ; k < N_STEPS ; ++k) {
#pragma omp target update if (dev != num_dev) device(dev) to(x[:3*n_nodes],f[:3*n_nodes])
#pragma omp target if (dev != num_dev) device(dev)
					{
						compute_velocity(x, f, my_start, my_end, n_nodes, eps, u_mix_d);
					}
#pragma omp target update if (dev != num_dev) device(dev) from(u_mix_d[:3*(my_end-my_start)])
					std::copy(u_mix_d,u_mix_d+3*(my_end - my_start),&u_mix[3*my_start]);
				}

				// free space on devices
#pragma omp target exit data if (dev != num_dev) device(dev) map(delete:x[:3*n_nodes],f[:3*n_nodes],u_mix_d[:3*(my_end - my_start)])
			}
		}
	}

	t_mic_cpu = omp_get_wtime() - t_mic_cpu;
	std::cout << "Finished mic+cpu computation, time = " << t_mic_cpu << std::endl;

	// check results
	for (std::size_t i=0; i<3*n_nodes; ++i)
		if (std::abs(u[i] -u_mix[i])/std::abs(u[i]) > 1e-12)
			std::cerr << "velocities do not agree ... i = " << i << " expected u[i] = " << u[i] << " got u[i] = " << u_mix[i] << std::endl;

	return 0;
}
