#include <iostream>
#include <omp.h>
// ------------------------------------------------------------------

#pragma omp declare target
int  successor( int   m );
void increment( int* pm );
#pragma omp end declare target

int main( void ) {
  int i;
#pragma omp target device(0)    
  {
    i = successor( 123 );
    increment( &i );
  }
 
  std::cout << "i = " << i << std::endl;

  i = successor( i );
  increment( &i );
  std::cout << "i = " << i << std::endl;

  return 0;
}

// ------------------------------------------------------------------

int successor( int m )  {
  return m+1;    // ++m works and is more compact, but seems cavalier
}

// ------------------------------------------------------------------

void increment(   int* pm )  {
  (*pm)++;
  return;
}

