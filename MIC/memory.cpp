///
/// TEST
///
#include <iostream>
#include <cmath>
#include <omp.h>

#define N 100

int main(const int argc, const char** argv) {
	omp_set_nested(1);
	int num_dev = omp_get_num_devices();
	std::cout << "number of devices " << num_dev << std::endl;

	void *data;
#pragma omp target device(0) map(from:data)
	{
		double *vdata = new double[N];	
#pragma omp parallel
		for (int i = 0 ; i < N ; i++) vdata[i]= i;
		data = (void *)vdata;
	}	

#pragma omp target device(0) map(to:data)
	{
		double *vdata = (double *)data;
		for (int i = 0 ; i < N ; i++){
			std::cout << vdata[i] << std::endl;
		}
	}
	return 0;
}
