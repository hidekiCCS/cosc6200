/*
 Large Scale Computing
 ex64.cu : CUDA Reduce

 THIS PROGRAM RUNS on GPU
 */

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cuda_runtime.h>

__global__ void reduce_sum_device(int cn, double *g_data) {
	int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x; // global ID
	int nl = threadIdx.x;   // thread ID in block
	__shared__ double s_data[512]; // use shared memory

	/* copy to shared memory */
	if (n >= cn)
		s_data[nl] = 0.0;
	else
		s_data[nl] = g_data[n];

	__syncthreads(); // wait until all threads in block copy data

	if (nl < 256)
		s_data[nl] += s_data[nl + 256];
	__syncthreads();
	if (nl < 128)
		s_data[nl] += s_data[nl + 128];
	__syncthreads();
	if (nl < 64)
		s_data[nl] += s_data[nl + 64];
	__syncthreads();
	if (nl < 32)
		s_data[nl] += s_data[nl + 32];
	__syncthreads();
	if (nl < 16)
		s_data[nl] += s_data[nl + 16];
	__syncthreads();
	if (nl < 8)
		s_data[nl] += s_data[nl + 8];
	__syncthreads();
	if (nl < 4)
		s_data[nl] += s_data[nl + 4];
	__syncthreads();
	if (nl < 2)
		s_data[nl] += s_data[nl + 2];
	__syncthreads();
	if (nl < 1)
		s_data[nl] += s_data[nl + 1];
	__syncthreads();

	/* store on global memory */
	if (nl == 0)
		g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

__host__ void reduce_sum(int num, double *g_data) {
	// num : size of array g_data[]
	// This host kernel computes $\Sigma^{num-1}_{i=0} g\_data[i]$
	// The result is stored in g_data[0]
	int n = num;
	while (n != 1) {
		int bl = n / 512 + 1;    // Number of Blocks
		int bx = min(bl, 512);    // Blocks in x-coordinate
		int by = bl / 512 + 1;   // Blocks in y-coordinate
		dim3 dimblock(bx, by, 1);  // z-coordinate is 1
		reduce_sum_device<<<dimblock,512>>>(n,g_data);

		n = bl; // each block recedes to 1
	}
}

int main(int argc, char** argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [number]\n";
		return 0;
	}
	std::cout << std::setprecision(16);
	std::cout << std::scientific;

	int num = atoi(argv[1]);
	double *dataHost = new double[num];
	std::cout << "# of data = " << num << std::endl;

	/* making data */
	for (int i = 0; i < num; i++) {
		dataHost[i] = ((double) rand()) / ((double) RAND_MAX) - 0.5;
	}
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	/// Reduction with CPU
	double ans = 0.0;
	cudaEventRecord(start);
	for (int i = 0; i < num; i++) {

		ans += dataHost[i];
	}
	cudaEventRecord(stop);

	std::cout << "CPU sum = " << ans;
	float milliseconds;
	cudaEventElapsedTime(&milliseconds, start, stop);
	std::cout << " Time = " << milliseconds << " ms\n";

	/* allocate space on GPU Device*/
	double *g_data;
	cudaMalloc((void **) &g_data, num * sizeof(double));

	/* send array data to GPU Device */
	cudaMemcpy(g_data, dataHost, num * sizeof(double), cudaMemcpyHostToDevice);

	/// Reduction with GPU
	cudaEventRecord(start);
	reduce_sum(num, g_data);
	cudaEventRecord(stop);

	/* copy data from GPU Device */
	double ret;
	cudaMemcpy(&ret, g_data, sizeof(double), cudaMemcpyDeviceToHost);

	std::cout << "GPU sum = " << ret;
	cudaEventElapsedTime(&milliseconds, start, stop);
	std::cout << " Time = " << milliseconds << " ms\n";

	cudaFree(g_data);
	delete[] dataHost;
}
