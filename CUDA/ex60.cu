/*
 Large Scale Computing
 ex60.cu : CUDA sample program

 THIS PROGRAM RUNS on GPU&CPU
 */

#include <iostream>
#include<cuda_runtime.h>

#define SIZE 10

__global__ void vecAdd(double *A, double *B, double *C) {
	int i = threadIdx.x;
	C[i] = A[i] + B[i];
}

int main(int argc, char** argv) {

	/* set vectors */
	double aHost[SIZE], bHost[SIZE], cHost[SIZE];
	for (int i = 0; i < SIZE; i++) {
		aHost[i] = i;
		bHost[i] = SIZE - i;
		cHost[i] = 0.0;
	}

	/* allocate space on GPU Device*/
	double *aDevice, *bDevice, *cDevice;
	cudaMalloc((void **) &aDevice, SIZE * sizeof(double));
	cudaMalloc((void **) &bDevice, SIZE * sizeof(double));
	cudaMalloc((void **) &cDevice, SIZE * sizeof(double));

	/* send array data to GPU Device */
	cudaMemcpy(aDevice, aHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(bDevice, bHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);

	/* call function on GPU */
	int n = SIZE;
	vecAdd<<<1, n>>>(aDevice,bDevice,cDevice);

	/* copy data from GPU Device */
	cudaMemcpy(cHost, cDevice, SIZE * sizeof(double), cudaMemcpyDeviceToHost);

	/* print */
	for (int i = 0; i < SIZE; i++) {
		std::cout << "C[" << i << "]=" << cHost[i] << std::endl;
	}

	cudaFree(aDevice);
	cudaFree(bDevice);
	cudaFree(cDevice);
}
