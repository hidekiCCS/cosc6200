/**
  Large Scale Computing
  ex72.cu : CUDA sample program

  cuBlas cuSolver test
  THIS PROGRAM RUNS on GPU&CPU
 */
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cusolverDn.h>

// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv){
	/// vector size
	const unsigned int size= 1000;

	/// Initialize cublas
	cublasHandle_t cublasH;
	cublasCreate(&cublasH);
	/// Initialize cusolver
	cusolverDnHandle_t cusolverH;
	cusolverDnCreate(&cusolverH);

	/* setup Eigen3  */
	Eigen::VectorXd eigen_bv(size);
	Eigen::MatrixXd eigen_mA(size,size);

	/* setup matrix A */
	for (int i = 0 ; i < size ; i++){
		for (int j = 0 ; j < size ; j++){
			eigen_mA(i,j) = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}

	/* setup vector b */
	for (int i = 0 ; i < size ; i++) eigen_bv(i) = i;


	/// Create matrix and vector on GPU device
	double ts = tsecond();
	double *cuda_xv;
	cudaMalloc((void **)&cuda_xv, size * sizeof(double));
	const double *c_mA = eigen_mA.data();
	double *cuda_mA;
	cudaMalloc((void **)&cuda_mA, size * size * sizeof(double));
	cudaMemcpy((void *)cuda_mA, (void *)c_mA, size * size * sizeof(double), cudaMemcpyHostToDevice);
	double *c_bv = eigen_bv.data();
	double *cuda_bv;
	cudaMalloc((void **)&cuda_bv, size * sizeof(double));
	cudaMemcpy((void *)cuda_bv, (void *)c_bv, size * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy((void *)cuda_xv, (void *)cuda_bv, size * sizeof(double), cudaMemcpyDeviceToDevice);
	double te = tsecond();
	std::cout << "Time cost for data transfer = " << 1000.0 * (te - ts)  << "(msec)\n";

	/* call Eigen solve b=Ax */
	ts = tsecond();
	Eigen::VectorXd eigen_xv = eigen_mA.fullPivLu().solve(eigen_bv);
	te = tsecond();
	eigen_bv = eigen_mA * eigen_xv - eigen_bv;
	std::cout << "|Ax-b|=" << eigen_bv.norm()  << std::endl;
	std::cout << "Time cost for Eigen [b=Ax] = " << 1000.0 * (te - ts) << "(msec)\n";


	/// solve with cusolver
	ts = tsecond();
	int bufferSize;
	cusolverDnDgetrf_bufferSize(cusolverH, size, size, cuda_mA, size, &bufferSize);
	double *buffer;
	cudaMalloc(&buffer, sizeof(double)*bufferSize);
	double *cuda_mAfact;
	cudaMalloc((void **)&cuda_mAfact, size * size * sizeof(double));
	cudaMemcpy((void *)cuda_mAfact, (void *)cuda_mA, sizeof(double)*size*size, cudaMemcpyDeviceToDevice);
	int *devIpiv;
	cudaMalloc((void **)&devIpiv, size * sizeof(int));
	int *devInfo;
	cudaMalloc((void **)&devInfo, sizeof(int));
	cusolverDnDgetrf(cusolverH, size, size, cuda_mAfact, size, buffer, devIpiv, devInfo);
	int info;
	cudaMemcpy((void *)&info, (void *)devInfo, sizeof(int), cudaMemcpyDeviceToHost);
	te = tsecond();
	if (info !=0){
		std::cout << "LU factorization failed " << info << std::endl;
		return -1;
	}
	std::cout << "Time cost for cusolver LU factorization = " << 1000.0 * (te - ts)  << "(msec)\n";

	ts = tsecond();
	cusolverDnDgetrs(cusolverH, CUBLAS_OP_N, size, 1, cuda_mAfact, size, devIpiv, cuda_xv, size,devInfo);
	cudaDeviceSynchronize();
	te = tsecond();
	cudaMemcpy((void *)&info, (void *)devInfo, sizeof(int), cudaMemcpyDeviceToHost);
	if (info == 0) std::cout << "successfully done\n";
	if (info < 0){
		std::cout << "the " << -info << "-th argument had an illegal value\n";
		return -1;
	}
	if (info > 0){
		std::cout << "U(" << info << "," << info << ") is exactly zero.\n";
		return -1;
	}
	std::cout << "Time cost for cusolver Dgetrs = " << 1000.0 * (te - ts)  << "(msec)\n";

	// check results
	double alpha = 1.0;
	double beta = -1.0;
	cublasDgemv(cublasH, CUBLAS_OP_N, size, size, &alpha, cuda_mA, size, cuda_xv, 1, &beta, cuda_bv, 1);
	double nrm;
	cublasDnrm2(cublasH, size,  cuda_bv, 1, &nrm);
	std::cout << "|Ax-b|=" << nrm << std::endl;

	cudaFree((void *)cuda_xv);
	cudaFree((void *)cuda_bv);
	cudaFree((void *)cuda_mA);
	cublasDestroy(cublasH);

	return 0;
}
