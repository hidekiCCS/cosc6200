
/**
  Large Scale Computing
  ex71.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU&CPU
 */
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <Eigen/Core>
#include <cuda_runtime.h>
#include <cublas_v2.h>

// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv){
	/// vector size
	const unsigned int size= 5000;

	/// Initialize cublas
	cublasHandle_t handle;
	cublasCreate(&handle);

	/* setup Eigen3  */
	Eigen::VectorXd eigen_xv(size);
	Eigen::VectorXd eigen_bv(size);
	Eigen::MatrixXd eigen_mA(size,size);

	/* setup matrix A */
	for (int i = 0 ; i < size ; i++){
		for (int j = 0 ; j < size ; j++){
			eigen_mA(i,j) = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}

	/* setup vector x */
	for (int i = 0 ; i < size ; i++) eigen_xv(i) = i;

	/* call Eigen */
	double ts = tsecond();
	eigen_bv = eigen_mA * eigen_xv;
	double te = tsecond();
	std::cout << "|b|=" << eigen_bv.norm()  << std::endl;
	std::cout << "Time cost for Eigen [b=Ax] = " << 1000.0 * (te - ts) << "(msec)\n";

	/// Create matrix and vector on GPU device
	ts = tsecond();
	const double *c_xv = eigen_xv.data();
	const double *cuda_xv;
	cudaMalloc((void **)&cuda_xv, size * sizeof(double));
	cudaMemcpy((void *)cuda_xv, (void *)c_xv, size * sizeof(double), cudaMemcpyHostToDevice);
	const double *c_mA = eigen_mA.data();
	const double *cuda_mA;
	cudaMalloc((void **)&cuda_mA, size * size * sizeof(double));
	cudaMemcpy((void *)cuda_mA, (void *)c_mA, size * size * sizeof(double), cudaMemcpyHostToDevice);
	double *cuda_bv;
	cudaMalloc((void **)&cuda_bv, size * sizeof(double));
	te = tsecond();
	std::cout << "Time cost for data transfer = " << 1000.0 * (te - ts)  << "(msec)\n";

	/// call cuBlas DGEMV
	double alpha = 1.0;
	double beta = 0.0;
	ts = tsecond();
	cublasDgemv(handle, CUBLAS_OP_N, size, size, &alpha, cuda_mA, size, cuda_xv, 1, &beta, cuda_bv, 1);
	te = tsecond();
	double nrm;
	cublasDnrm2(handle, size,  cuda_bv, 1, &nrm);
	std::cout << "|b|=" << nrm << std::endl;
	std::cout << "Time cost for cuBlas DGEMV = " << 1000.0 * (te - ts)  << "(msec)\n";

	cudaFree((void *)cuda_xv);
	cudaFree((void *)cuda_bv);
	cudaFree((void *)cuda_mA);
	cublasDestroy(handle);

	return 0;
}
