/*
  Large Scale Computing
  ex61.cu : CUDA sample program
  Threads test

  THIS PROGRAM RUNS on GPU&CPU
*/

#include <iostream>
#include <cuda_runtime.h>

#define NN 16
#define SIZE (1 << NN)

__global__ void vecAdd(double *A, double *B, double *C){
  int i = threadIdx.x;
  C[i] = A[i] + B[i];
}

int main( int argc, char** argv){

  /* set vectors */
  double aHost[SIZE],bHost[SIZE],cHost[SIZE];
  for (int i = 0 ; i < SIZE ; i++){
    aHost[i] = i;
    bHost[i] = SIZE - i;
    cHost[i] = 0.0;
  }
  
  /* comput A + B */
  double answer[SIZE];
  for (int i = 0 ; i < SIZE ; i++){
    answer[i] = aHost[i] + bHost[i]; 
  }

  /* allocate space on GPU Device*/
  double *aDevice,*bDevice,*cDevice;
  cudaMalloc((void **)&aDevice,SIZE * sizeof(double));
  cudaMalloc((void **)&bDevice,SIZE * sizeof(double));
  cudaMalloc((void **)&cDevice,SIZE * sizeof(double));
  
  /* send array data to GPU Device */
  cudaMemcpy(aDevice, aHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(bDevice, bHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(cDevice, cHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);
  
  /* call function on GPU */
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  int n = 2;
  for (int i = 0 ; i < NN ; i++){
    cudaEventRecord(start);
    vecAdd<<<1, n>>>(aDevice,bDevice,cDevice);
    cudaEventRecord(stop);

    /* copy data from GPU Device */
    cudaMemcpy(cHost, cDevice, SIZE * sizeof(double), cudaMemcpyDeviceToHost);    

    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << n << " threads in a block : Processing time: " << milliseconds  << "(ms)\n";

    /* Check result */
    for (int j = 0 ; j < n ; j++){
      if (cHost[j] != answer[j]){
	std::cout << "answer is wrong!!\n";
	break;
      }
    }

    /* clear */
    for (int j = 0 ; j < n ; j++){
      cHost[j] = 0;
    }
    cudaMemcpy(cDevice, cHost, SIZE * sizeof(double), cudaMemcpyHostToDevice);
    n = n * 2;
  }

  cudaFree(aDevice);
  cudaFree(bDevice);
  cudaFree(cDevice);
}
