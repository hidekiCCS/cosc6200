
/**
  Large Scale Computing
  ex70.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU&CPU
 */
#include <iostream>
#include <vector>
#include <sys/time.h>
#include <Eigen/Core>
#include <cuda_runtime.h>
#include <cublas_v2.h>

// timing method
double tsecond() {
	struct timeval tm;
	double t;
	static int base_sec = 0, base_usec = 0;

	gettimeofday(&tm, NULL);
	if (base_sec == 0 && base_usec == 0) {
		base_sec = tm.tv_sec;
		base_usec = tm.tv_usec;
		t = 0.0;
	} else {
		t = (double) (tm.tv_sec - base_sec)
				+ ((double) (tm.tv_usec - base_usec)) / 1.0e6;
	}
	return t;
}

int main(int argc, char **argv){
	/// Initialize cublas
	cublasHandle_t handle;
	cublasCreate(&handle);

	/// Create a Eigen vector
	const unsigned int size= 500000000;
	Eigen::VectorXd eigen_vec(size);
	for (int i = 0 ; i < size ; i++) eigen_vec[i] = (i % 100) / 50.0;

	/// call Eigen norm2
	double ts = tsecond();
	double nrm = eigen_vec.norm();
	double te = tsecond();
	std::cout << "Eigen3 NORM=" << nrm << std::endl;
	std::cout << "Time cost for Eigen3 norm = " << 1000.0 * (te - ts)  << "(msec)\n";

	/// Create a vector on GPU device
	ts = tsecond();
	const double *c_vec = eigen_vec.data();
	const double *cuda_vec;
	cudaMalloc((void **)&cuda_vec, size * sizeof(double));
	cudaMemcpy((void *)cuda_vec, (void *)c_vec, size * sizeof(double), cudaMemcpyHostToDevice);
	te = tsecond();
	std::cout << "Time cost for data transfer = " << 1000.0 * (te - ts)  << "(msec)\n";

	/// call cuBlas norm2
	ts = tsecond();
	cublasDnrm2(handle, size,  cuda_vec, 1, &nrm);
	te = tsecond();
	std::cout << "cuBlas NORM=" << nrm << std::endl;
	std::cout << "Time cost for cuBlas norm = " << 1000.0 * (te - ts)  << "(msec)\n";

	cudaFree((void *)cuda_vec);
	cublasDestroy(handle);

	return 0;
}
