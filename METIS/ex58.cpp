//
//  Large Scale Computing
//  METIS test
//  ex58.cpp
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <metis.h>

int main(int argc, char **argv) {
	if (argc < 3) {
		std::cout << "Usage:" << argv[0] << " [size] [k-parts]\n";
		return 0;
	}

	// get mesh size
	int num = std::atoi(argv[1]);
	idx_t nparts = std::atoi(argv[2]);

	// Make Mesh Topology Information
	idx_t *xadj = new idx_t[num * num + 1];
	idx_t *adjency = new idx_t[num * num * 4];
	int count = 0;
	for (int j = 0; j < num; j++) {
		for (int i = 0; i < num; i++) {
			int n = i + num * j;
			xadj[n] = count;
			if (i != num - 1) {
				// add east point
				adjency[count] = n + 1;
				count++;
			}
			if (i != 0) {
				// add west point
				adjency[count] = n - 1;
				count++;
			}
			if (j != num - 1) {
				// add north point
				adjency[count] = n + num;
				count++;
			}
			if (j != 0) {
				// add north point
				adjency[count] = n - num;
				count++;
			}
		}
	}
	xadj[num * num] = count;

	for (int i = 0; i < num * num; i++) {
		std::cout << i << " : ";
		for (int j = xadj[i]; j < xadj[i + 1]; j++) {
			std::cout << adjency[j] << " ";
		}
		std::cout << std::endl;
	}

	idx_t nn = num * num;
	idx_t ne = count;

	idx_t objval;
	idx_t *epart = new idx_t[ne];
	idx_t *npart = new idx_t[nn];
	int status = METIS_PartGraphKway(&nn, &ne, xadj, adjency, NULL, NULL, NULL, &nparts, NULL, NULL, NULL, &objval, npart);
	if (status != METIS_OK) {
		std::cout << "Metis returned with an error.\n";
		return 0;
	}

	for (int k = 0; k < nparts; k++) {
		std::ostringstream fileName;
		fileName << "resEx54_" << std::setfill('0') << std::setw(2) << k << ".dat";
		std::ofstream ofile;
		ofile.open((fileName.str()).c_str());

		for (int j = 0; j < num; j++) {
			for (int i = 0; i < num; i++) {
				int n = i + num * j;
				if (k == npart[n]){
					ofile << i << " " << j << std::endl;
				}
			}
		}
		ofile.close();
	}

	return 0;
}
