//
// Large Scale Computing
// 2D Heat/Mass Transfer
// Solve d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

int main(int argc, char **argv) {
  	if (argc < 3){
    		std::cout << argv[0] << " [size] [stride]\n";
   	 	return 0;
  	}

 	int size = std::atoi(argv[1]);
  	std::cout << "size=" << size << std::endl;
 	int bsize = std::atoi(argv[2]);
  	std::cout << "stride=" << bsize << std::endl;

  	// set up
	double *avec = new double[size];
	double *bvec = new double[size];
	for (int i = 0; i < size ; i++) {
		avec[i] = rand() / RAND_MAX;
		bvec[i] = rand() / RAND_MAX;
	}

	// compute
	double start = clock();
	for (int ii = 0; ii < size ; ii+=bsize) {
		for (int j = 0; j < size; j++) {
			for (int i = ii ; i < std::min(size, ii + bsize) ; i++) {
				avec[i] += bvec[j];
			}
		}
	}
//	for (int j = 0; j < size; j++) {
//	  for (int i = 0 ; i < size ; i++) {
//	    avec[i] += bvec[j];
//	  }
//	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "Time cost = " << tcost << "(sec)\n";

	std::cout << "Done\n";
	return 0;
}
