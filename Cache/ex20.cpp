/*
 * ex20.cpp
 *
 *  Created on: Sep 23, 2015
 *      Author: fuji
 */

#include <iostream>
#include <cstdlib>
#include <cmath>

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [size]\n";
		return 0;
	}

	int size = std::atoi(argv[1]);
	std::cout << "Size=" << size << std::endl;

	// allocation
	double *xvec = new double[size];
	double *bvec = new double[size];
	// Make 2D array
	double *amatBlock = new double[size * size];
	double **amat = new double*[size];
	for (int i = 0 ; i < size ; i++){
		amat[i] = amatBlock + size * i;
	}

	// setup matrix A elements
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			amat[i][j] = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}
	// setup vector x elements
	for (int i = 0; i < size; i++) {
		xvec[i] = i;
	}

	/// compute b = a*x ///
	for (int i = 0; i < size ; i++) {
		bvec[i] = 0.0;
	}
	double start = clock();
	for (int i = 0; i < size ; i++) {
		for (int j = 0; j < size ; j++) {
			bvec[i] += amat[i][j] * xvec[j];
		}
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "Time cost = " << tcost << "(sec)\n";

	delete [] xvec;
	delete [] bvec;
	delete [] amatBlock;
	delete [] amat;

	return 0;
}

