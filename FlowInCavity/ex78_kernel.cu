/*
  Large Scale Computing
  Stokeslet for 2D with CUDA
  
  Ricardo Cortez,"The Method of Regularized Stokeslets", 
  (2001), SIAM J. Sci. Comput., Vol.23, No.4, pp.1204
  
  assuming mu = 1

  Use CUDA
*/

#include <iostream>
#include <cuda_runtime.h>
#include<cublas.h>
#include<cusparse.h>

#define DIM 2
#define EPSILON 0.005 /* blob size*/

/* term1 */
__device__ double term1(double r,double ep){
  double sq;

  sq = sqrt(r * r + ep * ep);
  return log(sq + ep) - ep * (sq + 2.0 * ep) / (sq + ep) / sq;
}

/* term2 */
__device__ double term2(double r,double ep){
  double sq;
  sq = sqrt(r * r + ep * ep);
  return (sq + 2.0 * ep) / (sq + ep) / (sq + ep) / sq;
}

/* reduce */
__global__ void reduce_sum_device(int cn,double *g_data){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int nl = threadIdx.x;
  __shared__ double s_data[512];

  /* copy to shared memory */
  if (n >= cn) s_data[nl] = 0.0;
  else s_data[nl] = g_data[n];

 
  __syncthreads();
  
  if (nl < 256) s_data[nl] += s_data[nl + 256]; 
  __syncthreads();
  if (nl < 128) s_data[nl] += s_data[nl + 128];
  __syncthreads();
  if (nl < 64) s_data[nl] += s_data[nl + 64];
  __syncthreads();
  if (nl < 32) s_data[nl] += s_data[nl + 32];
  __syncthreads();
  if (nl < 16) s_data[nl] += s_data[nl + 16];
  __syncthreads();
  if (nl < 8) s_data[nl] += s_data[nl + 8];
  __syncthreads();
  if (nl < 4) s_data[nl] += s_data[nl + 4];
  __syncthreads();
  if (nl < 2) s_data[nl] += s_data[nl + 2];
  __syncthreads();
  if (nl < 1) s_data[nl] += s_data[nl + 1];
  __syncthreads();

  /* store on global memory */
  if (nl == 0) g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}



__global__ void setup_particles(int nump,int numpwidth,int numpdepth,
				double *loc_d,double *vel_d,double *foc_d){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  
  if (n < nump){
    foc_d[n * DIM] = 0.0;
    foc_d[n * DIM + 1] = 0.0;
    if ((n >= 0) && (n <= numpwidth)){ /* top */
      loc_d[n * DIM] = -0.5 + EPSILON * (double)n; /* x */
      loc_d[n * DIM + 1] = 0.0; /* y */
      vel_d[n * DIM] = 1.0;
      vel_d[n * DIM + 1] = 0.0;
    }else{
      if (n <= (numpwidth + numpdepth)){ /* right wall */      
	loc_d[n * DIM] = 0.5; /* x */
	loc_d[n * DIM + 1] = -EPSILON * (double)(n - numpwidth); /* y */	
	vel_d[n * DIM] = 0.0;
	vel_d[n * DIM + 1] = 0.0;
      }else{
	if (n <= (2 * numpwidth + numpdepth)){ /* bottom */
	  loc_d[n * DIM] = 0.5 - EPSILON 
	    * (double)(n - (numpwidth + numpdepth)); /* x */
	  loc_d[n * DIM + 1] = -EPSILON * numpdepth; /* y */
	  vel_d[n * DIM] = 0.0;
	  vel_d[n * DIM + 1] = 0.0;	  
	}else{                 /* left wall */    
	  loc_d[n * DIM] = -0.5; /* x */
	  loc_d[n * DIM + 1] = -EPSILON 
	    * (double)((2 * numpwidth + 2 * numpdepth) - n); /* y */	
	  vel_d[n * DIM] = 0.0;
	  vel_d[n * DIM + 1] = 0.0;  
	}
      }
    }
  }
}

/* make Matrix */
__global__ void slet2d_mkMatrix(int np, double *loc_d, double *mat_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  int j;
  double r;
  double dx,dy;
  double tr1,tr2;

  /* make mat */
  if (i < np){
    for (j = 0 ; j < np ; j++){
      dx = loc_d[i * DIM    ] - loc_d[j * DIM    ];
      dy = loc_d[i * DIM + 1] - loc_d[j * DIM + 1];
      
      r = sqrt(dx * dx + dy * dy);

      tr1 = term1(r,EPSILON) / (4.0 * M_PI);
      tr2 = term2(r,EPSILON) / (4.0 * M_PI); 
      
      mat_d[i * DIM +      j * DIM      * np * DIM] = -tr1 + tr2 * dx * dx;
      mat_d[i * DIM +     (j * DIM + 1) * np * DIM] =        tr2 * dx * dy;
      mat_d[i * DIM + 1 +  j * DIM      * np * DIM] =        tr2 * dx * dy;
      mat_d[i * DIM + 1 + (j * DIM + 1) * np * DIM] = -tr1 + tr2 * dy * dy;      
    }
  }
}

__global__ void slet2d_velocity_device(int np,double *loc_d, double *foc_d, 
				       double clocx, double clocy,
				       double *dvx_d, double *dvy_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  double r;
  double dx,dy;
  double tr1,tr2;

  if (i < np){
    dx = clocx - loc_d[i * DIM    ];
    dy = clocy - loc_d[i * DIM + 1];

    r = sqrt(dx * dx + dy * dy);
    
    tr1 = term1(r,EPSILON) / (4.0 * M_PI);
    tr2 = term2(r,EPSILON) / (4.0 * M_PI);       

    tr2 *= foc_d[i * DIM] * dx + foc_d[i * DIM + 1] * dy; 
      
    dvx_d[i] = -foc_d[i * DIM    ] * tr1 + tr2 * dx;
    dvy_d[i] = -foc_d[i * DIM + 1] * tr1 + tr2 * dy;
  }
}

/* compute velocity */
void slet2d_velocity(int np, double *loc_d, double *foc_d, 
		     double clocx, double clocy, double *cvelx,double *cvely){
  int ndata;
  int bl,bx,by;
  double *dvx_d;
  double *dvy_d;
  
  /* alloc working space on device */
  cudaMalloc((void **)&dvx_d, sizeof(double) * np);  
  cudaMalloc((void **)&dvy_d, sizeof(double) * np);  

  /* Determine Blocks */
  bl = np / THREADS_PER_BLOCK;
  bx = min(bl + 1, 512);
  by = bl / 512 + 1;
  dim3 dimblock(bx,by,1);
  
  slet2d_velocity_device<<<dimblock,THREADS_PER_BLOCK>>>(np, loc_d, foc_d, clocx, clocy, dvx_d, dvy_d);  

  ndata = np;
  while(1){
    /* Determine Blocks */
    bl = ndata / 512;
    bx = min(bl + 1, 512);
    by = bl / 512 + 1;
    dim3 dimblock(bx,by,1);

    /* reduce sum*/
    reduce_sum_device<<<dimblock,512>>>(ndata,dvx_d);
    reduce_sum_device<<<dimblock,512>>>(ndata,dvy_d);

    ndata = ndata / 512 + 1;
    if (ndata == 1) break;
  }
  
  cudaMemcpy(cvelx, dvx_d, sizeof(double), cudaMemcpyDeviceToHost); 
  cudaMemcpy(cvely, dvy_d, sizeof(double), cudaMemcpyDeviceToHost); 

  cudaFree(dvx_d);
  cudaFree(dvy_d);
}

int solve_cg_cuda_host(int np,double *mat,double *bDev,double *xDev){

  double *rDev;
  double *pDev;
  double *ApDev;
  double rr,pAp,rr1,alpha,beta;
  int count;
  double normb;
  double tol = 1.0e-12;
  int n = 2 * np;
  /* Initialize cublas */
  if (cublasInit() != CUBLAS_STATUS_SUCCESS) {
    std::cout << "!!!! CUBLAS initialization error\n";
    return -1;
  }
  
  /* Check if the solution is trivial. */
  normb = cublasDnrm2(n,bDev,1);
  if (normb <= 0.0){
    cublasDscal (n, 0.0, xDev, 1);
    return 0;
  }  

  
  /* alloc spase on device */
  cudaMalloc((void **)&rDev, n * sizeof(double));
  cudaMalloc((void **)&pDev, n * sizeof(double));
  cudaMalloc((void **)&ApDev, n * sizeof(double));


  /* compute r0 = b - Ax */
  cublasDcopy(n, bDev, 1, rDev, 1); /* r = b */
  cublasDgemv('N', n, n, -1.0, mat, n, xDev, 1, 1.0, rDev, 1);
  
  rr = 0.0;
  cublasDcopy(n,rDev,1,pDev,1); /* p = r */
  rr = cublasDdot(n,rDev,1,rDev,1); /* rr = r.r */ 

  /* cg iteration */
  count = 0;
  while(rr  > tol * tol * normb * normb){        
    /* Ap = A*p */
    cublasDgemv('N', n, n, 1.0, mat, n, pDev, 1, 0.0, ApDev, 1);
     
    /* alpha = r.r / p.Ap */
    pAp = cublasDdot(n,pDev,1,ApDev,1); /* pAp = p.Ap */     
    alpha = rr / pAp;
    
    /* Beta */
    rr1 = 0.0;
    cublasDaxpy(n, alpha, pDev, 1, xDev, 1); /* x += alpha * p */
    cublasDaxpy(n,-alpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
    rr1 = cublasDdot(n, rDev, 1, rDev, 1); /* rr1 = r.r */     
    beta = rr1 / rr;


    /* p = r + beta * p  no blas routine :( */
    cublasDscal (n, beta, pDev, 1);  /* p *= beta */
    cublasDaxpy(n, 1.0, rDev, 1, pDev, 1); /* p += r */    
    
    rr = rr1;
    count++;
  }

  /* Deallocate Working Spaces */
  cudaFree(rDev);
  cudaFree(pDev);
  cudaFree(ApDev);

  return count;
}
