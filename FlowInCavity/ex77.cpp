//
//  Large Scale Computing
//  Stokes Flow in a Cavity
//  ex77.cpp
//
//
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <new>
#include <mpi.h>
#include "stokeslet2d_dist.h"

#define INTGRID 51    /* # of x-grid lines for internal velocity */

int main(int argc, char **argv) {
	// Initialize
	MPI::Init(argc, argv);

	// get myid and # of processors 
	int numproc = MPI::COMM_WORLD.Get_size();
	int myid = MPI::COMM_WORLD.Get_rank();

	if (argc < 2) {
		std::cout << "Usage: " << argv[0] << " [Depth of Cavity]\n";
		return -1;
	}

	/* get inputed depth */
	double dp = std::atof(argv[1]);

	/* EPSILON = 0.005 defined by 'stokeslet2d.h' */
	int numpdepth = (dp / EPSILON + 0.5); // # of particles in depth 
	int numpwidth = (1.0 / EPSILON + 0.5); // # of particles in width 
	int numparticles = numpdepth * 2 + numpwidth * 2; // total # od particles 
	if (myid == 0) {
		std::cout << "Total # of Particles= " << numparticles << std::endl;
	}

	// Allocate Space 
	// Create an insetance of object Rstokeslet2D
	Rstokeslet2D_dist *slet = new Rstokeslet2D_dist(numparticles);

	// get each arrays
	double *loc = slet->getLocationArray();
	double *vel = slet->getVelocityArray();
	int myStart = slet->myStart;
	int myEnd = slet->myEnd;

	/* set location & velocity of particles */
	for (int i = myStart; i < myEnd; i++) {
		int li = i - myStart;
		if ((i >= 0) && (i <= numpwidth)) { /* top */
			loc[li * DIM] = -0.5 + EPSILON * i; /* x */
			loc[li * DIM + 1] = 0.0; /* y */
			vel[li * DIM] = 1.0;
			vel[li * DIM + 1] = 0.0;
		} else {
			if (i <= (numpwidth + numpdepth)) { /* right wall */
				loc[li * DIM] = 0.5; /* x */
				loc[li * DIM + 1] = -EPSILON * (i - numpwidth); /* y */
				vel[li * DIM] = 0.0;
				vel[li * DIM + 1] = 0.0;
			} else {
				if (i <= (2 * numpwidth + numpdepth)) { /* bottom */
					loc[li * DIM] = 0.5
							- EPSILON * (i - (numpwidth + numpdepth)); /* x */
					loc[li * DIM + 1] = -EPSILON * numpdepth; /* y */
					vel[li * DIM] = 0.0;
					vel[li * DIM + 1] = 0.0;
				} else { /* left wall */
					loc[li * DIM] = -0.5; /* x */
					loc[li * DIM + 1] = -EPSILON
							* (double) ((2 * numpwidth + 2 * numpdepth) - i); /* y */
					vel[li * DIM] = 0.0;
					vel[li * DIM + 1] = 0.0;
				}
			}
		}
	}

	/* make 2Nx2N Matrix */
	double start = clock();
	slet->mkMatrix();
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	if (myid == 0) {
		std::cout << "Time cost for setup Matrix = " << tcost << "(sec)\n";
	}

	/* Sovle linear ststem */
	start = clock();
	slet->solve();
	tcost = (clock() - start) / CLOCKS_PER_SEC;
	if (myid == 0) {
		std::cout << "Time cost to solve linear system = " << tcost
				<< "(sec)\n";
	}
	/* 
	 compute internal velocity 
	 */
	int nyg = (EPSILON * (numpdepth * (INTGRID - 1)));
	double *cvel = new double[INTGRID * nyg * DIM];
	double *cloc = new double[INTGRID * nyg * DIM];

	/* setting grid */
	for (int j = 0; j < nyg; j++) {
		for (int i = 0; i < INTGRID; i++) {
			cloc[DIM * (i + INTGRID * j)] = -0.5
					+ static_cast<double>(i) / (INTGRID - 1);
			cloc[DIM * (i + INTGRID * j) + 1] = -static_cast<double>(j)
					/ (INTGRID - 1);
		}
	}

	/* compute velocities */
	start = clock();
	slet->getVelocities(INTGRID * nyg, cloc, cvel);
	tcost = (clock() - start) / CLOCKS_PER_SEC;
	if (myid == 0) {
		std::cout << "Time cost to compute interior velocity = " << tcost
				<< "(sec)\n";
	}

	/* out velocities */
	if (myid == 0) {
		std::ofstream ofile;
		ofile.open("resEx77.dat");
		ofile << std::setprecision(16);
		ofile << std::scientific;
		for (int j = 0; j < nyg; j++) {
			for (int i = 0; i < INTGRID; i++) {
				ofile << cloc[DIM * (i + INTGRID * j)] << " "
						<< cloc[DIM * (i + INTGRID * j) + 1] << " "
						<< cvel[DIM * (i + INTGRID * j)] << " "
						<< cvel[DIM * (i + INTGRID * j) + 1] << std::endl;
			}
		}
		ofile.close();
	}
	/* free */
	delete[] cloc;
	delete[] cvel;
	delete slet;

	MPI::Finalize();
	return 0;
