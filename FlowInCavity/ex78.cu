/*
  Large Scale Computing
  Stokes Flow in a Cavity
  ex72.cu

  CUDA version
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>

#define INTGRID 51    /* # of x-grid lines for internal velocity */
#define THREADS_PER_BLOCK 256

#include"ex70_kernel.cu"

int main(int argc, char **argv){
  double *loc_d; /* particle location */
  double *vel_d; /* particle velocity */
  double *foc_d; /* particle force */
  double *mat_d; /* 2Nx2N dense matrix */
  double *cloc; /* array for internal points */
  double *cvel; /* array for internal velocity */
  int i,j;
  int nyg;

  if (argc < 2){
    std::cout << "Usage: " << argv[0] << " [Depth of Cavity]\n";
    return -1;
  }

  /* get inputed depth */
  double dp = atof(argv[1]);

  /* # of particles in depth */
  int numpdepth = (int)(dp / EPSILON + 0.5);
  
  /* # of particles in width */
  int numpwidth = (int)(1.0 / EPSILON + 0.5);
  
  /* total # od particles */
  int numparticles = numpdepth * 2 + numpwidth * 2;
  printf("Total # of Particles=%d\n",numparticles);

  /* Determine Blocks */
  int bl = numparticles / THREADS_PER_BLOCK + 1;
  int bx = min(bl, 512);
  int by = bl / 512 + 1;
  dim3 dimblock(bx,by,1);


  /* Allocate Space on Device */
  cudaMalloc((void **)&loc_d, sizeof(double) * numparticles * DIM);  
  cudaMalloc((void **)&vel_d, sizeof(double) * numparticles * DIM);  
  cudaMalloc((void **)&foc_d, sizeof(double) * numparticles * DIM);  
  cudaMalloc((void **)&mat_d, sizeof(double) * numparticles * DIM * numparticles * DIM);  
    
  
  /* set location & velocity of particles */
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);
  setup_particles<<<dimblock,THREADS_PER_BLOCK>>>(numparticles,numpwidth,numpdepth,
						  loc_d,vel_d,foc_d);
  cudaEventRecord(stop);
  float milliseconds;
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "setting particle " << milliseconds << " ms\n";
  
  /* make 2Nx2N Matrix */
  cudaEventRecord(start);
  slet2d_mkMatrix<<<dimblock,THREADS_PER_BLOCK>>>(numparticles,loc_d,mat_d);
  cudaEventRecord(stop);
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "setting matrix " << milliseconds << " ms\n";

  /* Sovle linear ststem */
  double tstart = clock();
  solve_cg_cuda_host(numparticles,mat_d,vel_d,foc_d);
  double tcost = (clock() - tstart) / CLOCKS_PER_SEC;
  std::cout << "Time cost to solve linear system = " << tcost << "(sec)\n";


  /* 
     compute internal velocity 
  */
  nyg = (int)(EPSILON * (double)(numpdepth * (INTGRID - 1)));
  cvel = new double[INTGRID * nyg * DIM];
  cloc = new double[INTGRID * nyg * DIM];

  /* setting location */
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      cloc[DIM * (i + INTGRID * j)    ] = -0.5 + (double)i / (double)(INTGRID - 1);
      cloc[DIM * (i + INTGRID * j) + 1] = -(double)j / (double)(INTGRID - 1);
    }
  }
  
  /* compute velocities */
  tstart = clock();
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      slet2d_velocity(numparticles, loc_d, foc_d,
		      cloc[DIM * (i + INTGRID * j)    ], cloc[DIM * (i + INTGRID * j) + 1],
		      &cvel[DIM * (i + INTGRID * j)   ], &cvel[DIM * (i + INTGRID * j) + 1]);  
    }
  }
  tcost = (clock() - tstart) / CLOCKS_PER_SEC;
  std::cout << "Time cost to compute interior velocity = " << tcost
	    << "(sec)\n";

  /* out velocities */
  std::ofstream ofile;
  ofile.open("resEx72.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int j = 0; j < nyg; j++) {
    for (int i = 0; i < INTGRID; i++) {
      ofile << cloc[DIM * (i + INTGRID * j)] << " "
	    << cloc[DIM * (i + INTGRID * j) + 1] << " "
	    << cvel[DIM * (i + INTGRID * j)] << " "
	    << cvel[DIM * (i + INTGRID * j) + 1] << std::endl;
    }
  }
  ofile.close();


  /* free */
  cudaFree(mat_d);

  delete [] cloc;
  delete [] cvel;

  cudaFree(loc_d);
  cudaFree(vel_d);
  cudaFree(foc_d);
}
