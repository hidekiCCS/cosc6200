//
// Large Scale Computing
// Vectorize Loop Test
// ex19.cpp 
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [integer number]\n";
		return -1;
	}
	int num = std::atoi(argv[1]); // Get a number from commandline

	double *a = new double[num];
	double *b = new double[num];
	double *c = new double[num];
	double *d = new double[num];
	double *e = new double[num];

	// this is vectorizable loop
	for (int n = 0; n < num; n++) {
		a[n] = n;
		b[n] = num - n;
		c[n] = n;
		d[n] = 0;
		e[n] = num - n;
	}
	// set output format
	std::cout << std::setprecision(16);
	std::cout << std::scientific;

	// this is not vectorizable loop (control flow in the loop)
	for (int n = 1; n < num; n++) {
		a[n] = b[n] + c[n];
		d[n] = e[n] + a[n - 1];
		if (d[n] > num)
			break;
	}

	double start = clock();
	// this is not vectorizable loop
	for (int n = 1; n < num; n++) {
		d[n] = e[n] + a[n - 1];
		a[n] = b[n] + c[n];
	}
	std::cout << (clock() - start) / CLOCKS_PER_SEC << std::endl;

	start = clock();
	// this is vectorizable loop
	for (int n = 1; n < num; n++) {
		a[n] = b[n] + c[n];
		d[n] = e[n] + a[n - 1];
	}
	std::cout << (clock() - start) / CLOCKS_PER_SEC << std::endl;

	return 0;
}
