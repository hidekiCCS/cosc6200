/*
 * simdMask.h
 *
 *  Created on: Nov 24, 2014
 *      Author: fuji
 */

#ifndef SIMDMASK_H_
#define SIMDMASK_H_

#include <immintrin.h>

namespace CCS {

#ifdef __MIC__
static const int strideDouble = 8;
typedef __mmask mmMask;
#else
#ifdef __AVX__
static const int strideDouble = 4;
typedef __m256d mmMask;
#else
#ifdef __SSE__
static const int strideDouble = 2;
typedef __m128d mmMask;
#endif
#endif
#endif

class simdDoubleMask {
protected:
#ifdef __MIC__
	__mmask m;
#else
#ifdef __AVX__
	__m256d m;
#else
#ifdef __SSE__
	__m128d m;
#endif
#endif
#endif

public:
	simdDoubleMask() {
#ifdef __MIC__
		m = 0xffff;
#else
#ifdef __AVX__
		m = _mm256_set1_pd(0.0);
		m = _mm256_cmp_pd(m,m,_CMP_EQ_OQ);
#else
#ifdef __SSE__
		m = _mm_set1_pd(0.0);
		m = _mm_cmpeq_pd(m,m);
#endif
#endif
#endif
	}

	simdDoubleMask(mmMask mm) {
		m = mm;
	}

	simdDoubleMask(int mm) {
#ifdef __MIC__
		m = _mm512_int2mask(mm);
#else
#ifdef __AVX__
		m = _mm256_set1_pd((double)mm);
#else
#ifdef __SSE__
		std::cout << mm << "here\n";
		m = _mm_set1_pd((double)mm);
#endif
#endif
#endif
	}

#ifdef __MIC__
	simdDoubleMask(bool b7, bool b6, bool b5, bool b4, bool b3, bool b2, bool b1, bool b0){
		m = _mm512_int2mask((b7 ? 0x80:0) + (b6 ? 0x40:0) + (b5 ? 0x20:0) + (b4 ? 0x10:0) + (b3 ? 0x08:0) + (b2 ? 0x04:0) + (b1 ? 0x02:0) + (b0 ? 0x01:0));
	}
#else
#ifdef __AVX__
	simdDoubleMask(bool b3, bool b2, bool b1, bool b0){
		m = _mm256_set_pd(b3 ? (-0.0f):0.0, b2 ? (-0.0f):0.0, b1 ? (-0.0f):0.0, b0 ? (-0.0f):0.0);
	}
#else
#ifdef __SSE__
	simdDoubleMask(bool b1, bool b0){
		m = _mm_set_pd(b1 ? (-0.0f):0.0, b0 ? (-0.0f):0.0);
	}
#endif
#endif
#endif

	operator mmMask() const {
		return m;
	}

	simdDoubleMask operator &=(simdDoubleMask a) {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_kand(m,a);
#else
#ifdef __AVX__
		return *this = (simdDoubleMask) _mm256_and_pd(m, a);
#else
#ifdef __SSE__
		return *this = (simdDoubleMask) _mm_and_pd(m, a);
#endif
#endif
#endif
	}

	simdDoubleMask operator |=(simdDoubleMask a) {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_kor(m,a);
#else
#ifdef __AVX__
		return *this = (simdDoubleMask) _mm256_or_pd(m,a);
#else
#ifdef __SSE__
		return *this = (simdDoubleMask) _mm_or_pd(m,a);
#endif
#endif
#endif
	}

	simdDoubleMask operator ^=(simdDoubleMask a) {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_kxor(m,a);
#else
#ifdef __AVX__
		return *this = (simdDoubleMask) _mm256_xor_pd(m,a);
#else
#ifdef __SSE__
		return *this = (simdDoubleMask) _mm_xor_pd(m,a);
#endif
#endif
#endif
	}

	simdDoubleMask operator ~() {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_knot(m);
#else
#ifdef __AVX__
		simdDoubleMask a;
		return *this = (simdDoubleMask) _mm256_andnot_pd(m,a);
#else
#ifdef __SSE__
		simdDoubleMask a;
		return *this = (simdDoubleMask) _mm_andnot_pd(m,a);
#endif
#endif
#endif
	}

	simdDoubleMask andn(simdDoubleMask a) {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_kandn(m,a);
#else
#ifdef __AVX__
		return *this = (simdDoubleMask) _mm256_andnot_pd(m,a);
#else
#ifdef __SSE__
		return *this = (simdDoubleMask) _mm_andnot_pd(m,a);
#endif
#endif
#endif
	}

	simdDoubleMask andnr(simdDoubleMask a) {
#ifdef __MIC__
		return *this = (simdDoubleMask) _mm512_kandnr(m,a);
#else
#ifdef __AVX__
		return *this = (simdDoubleMask) _mm256_andnot_pd(a,m);
#else
#ifdef __SSE__
		return *this = (simdDoubleMask) _mm_andnot_pd(a,m);
#endif
#endif
#endif
	}

	friend std::ostream& operator<<(std::ostream &os, const simdDoubleMask &a) {
#ifdef __MIC__
		unsigned int *us = (unsigned int *)&a;
#else
#ifdef __AVX__
		unsigned int usa = (unsigned int )_mm256_movemask_pd(a);
		unsigned int *us = &usa;
#else
#ifdef __SSE__
		unsigned int usa = (unsigned int )_mm_movemask_pd(a);
		unsigned int *us = &usa;
#endif
#endif
#endif
		for (int i = strideDouble - 1; i >= 0; i--) {
			unsigned int mm = 1 << i;
			os << (*us & mm) / mm;
		}
		return os;
	}
};

////////////////////////////////////////////////////
inline simdDoubleMask operator &(simdDoubleMask a, simdDoubleMask b) {
#ifdef __MIC__
	return _mm512_kand(a,b);
#else
#ifdef __AVX__
		return _mm256_and_pd(a, b);
#else
#ifdef __SSE__
		return _mm_and_pd(a, b);
#endif
#endif
#endif
}

inline simdDoubleMask operator |(simdDoubleMask a, simdDoubleMask b) {
#ifdef __MIC__
	return _mm512_kor(a,b);
#else
#ifdef __AVX__
		return _mm256_or_pd(a, b);
#else
#ifdef __SSE__
		return _mm_or_pd(a, b);
#endif
#endif
#endif
}

inline simdDoubleMask operator ^(simdDoubleMask a, simdDoubleMask b) {
#ifdef __MIC__
	return _mm512_kxor(a,b);
#else
#ifdef __AVX__
		return _mm256_xor_pd(a, b);
#else
#ifdef __SSE__
		return _mm_xor_pd(a, b);
#endif
#endif
#endif

}

} /* namespace CCS */

#endif /* SIMDMASK_H_ */
