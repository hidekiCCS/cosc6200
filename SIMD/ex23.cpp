//
// Large Scale Computing
// Unroll Loop Test
// ex20.cpp 
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>

int main(int argc, char **argv){
  if (argc < 2){
    std::cout << argv[0] << " [integer number]\n";
    return -1;
  }
  int num = std::atoi(argv[1]); // Get a number from commandline
  
  double *a = new double[num];
  double *b = new double[num];
  double *c = new double[num];
  double *d = new double[num];

  for (int n = 0 ; n < num ; n++){
    a[n] = n;
    b[n] = num - n;
    c[n] = 0;
    d[n] = 0;
  }
  // set output format
  std::cout << std::setprecision(16);
  std::cout << std::scientific;

  // ordinary loop
  double start = clock();
  for (int n = 0 ; n < num ; n++){
    d[n] = a[n] - b[n];
  }
  std:: cout << (clock() - start)/ CLOCKS_PER_SEC << std::endl; 

  // unrolled loop
  start = clock();
  int repeat = num / 8;
  int left = num % 8;
  //  for (int n = 0 ; n < num ; n+=8){
  int n = 0;
  while(repeat-- > 0){
    c[n] = a[n] - b[n];
    c[n+1] = a[n+1] - b[n+1];
    c[n+2] = a[n+2] - b[n+2];
    c[n+3] = a[n+3] - b[n+3];
    c[n+4] = a[n+4] - b[n+4];
    c[n+5] = a[n+5] - b[n+5];
    c[n+6] = a[n+6] - b[n+6];
    c[n+7] = a[n+7] - b[n+7];
    n+=8;
  }
  n = num - left;
  switch(left){
  case 7: 
    c[n+6] = a[n+6] - b[n+6];
  case 6: 
    c[n+5] = a[n+5] - b[n+5];
  case 5: 
    c[n+4] = a[n+4] - b[n+4];
  case 4: 
    c[n+3] = a[n+3] - b[n+3];
  case 3: 
    c[n+2] = a[n+2] - b[n+2];
  case 2: 
    c[n+1] = a[n+1] - b[n+1];
  case 1: 
    c[n] = a[n] - b[n];    
  case 0: ; 
  }
  std:: cout << (clock() - start)/ CLOCKS_PER_SEC << std::endl; 

  double err = 0.0;
  for (int n = 0 ; n < num ; n++){
    err += (c[n] - d[n]) * (c[n] - d[n]);
  }
  std::cout << "err=" << err << std::endl;

  return 0;
}
