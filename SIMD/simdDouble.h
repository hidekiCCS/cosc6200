/*
 * simdDouble.h
 *
 *  Created on: Nov 24, 2014
 *      Author: fuji
 */

#ifndef SIMDDOUBLE_H_
#define SIMDDOUBLE_H_

#include <immintrin.h>
#include "simdDoubleMask.h"

namespace CCS {

#ifdef __MIC__
typedef __m512d mmDouble;
#define SETA_PD(x)     _mm512_load_pd((x))
#else
#ifdef __AVX__
typedef __m256d mmDouble;
#define SETA_PD(x)     _mm256_load_pd((x))
#else
#ifdef __SSE__
typedef __m128d mmDouble;
# define SETA_PD(x)     _mm_load_pd((x))
#endif
#endif
#endif

class F64vec8;

class simdDouble {
protected:
#ifdef __MIC__
	__m512d vec;
#else
#ifdef __AVX__
	__m256d vec;
#else
#ifdef __SSE__
	__m128d vec;
#endif
#endif
#endif
public:
	simdDouble() {
	}

	simdDouble(mmDouble m) {
		vec = m;
	}

	simdDouble(double d[strideDouble]) {
		this->vec = SETA_PD(d);
	}

#ifdef __MIC__
	simdDouble(const double d7, const double d6, const double d5, const double d4, const double d3, const double d2, const double d1, const double d0) {
		vec = _mm512_set_pd(d7, d6, d5, d4, d3, d2, d1, d0);
	}
#else
#ifdef __AVX__
	simdDouble(const double d3, const double d2, const double d1, const double d0) {
		vec = _mm256_set_pd(d3, d2, d1, d0);
	}
#else
#ifdef __SSE__
	simdDouble(const double d1, const double d0) {
		vec = _mm_set_pd(d1, d0);
	}
#endif
#endif
#endif

	explicit simdDouble(const double d) {
#ifdef __MIC__
		vec = _mm512_set1_pd(d);
#else
#ifdef __AVX__
		vec = _mm256_set1_pd(d);
#else
#ifdef __SSE__
		vec = _mm_set1_pd(d);
#endif
#endif
#endif
	}
	explicit simdDouble(const float d) {
#ifdef __MIC__
		vec = _mm512_set1_pd((double)d);
#else
#ifdef __AVX__
		vec = _mm256_set1_pd((double)d);
#else
#ifdef __SSE__
		vec = _mm_set1_pd((double) d);
#endif
#endif
#endif
	}

#ifdef __MIC__
	operator __m512d() const {
		return vec;
	}
#else
#ifdef __AVX__
	operator __m256d() const {
		return vec;
	}
#else
#ifdef __SSE__
	operator __m128d() const {
		return vec;
	}
#endif
#endif
#endif

	/* Arithmetic Operators */
	friend simdDouble operator +(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_add_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_add_pd(a,b);
#else
#ifdef __SSE__
		return _mm_add_pd(a, b);
#endif
#endif
#endif
	}

	friend simdDouble operator +(const simdDouble &a, const double b) {
#ifdef __MIC__
		return _mm512_add_pd(a,_mm512_set1_pd(b));
#else
#ifdef __AVX__
		return _mm256_add_pd(a,_mm256_set1_pd(b));
#else
#ifdef __SSE__
		return _mm_add_pd(a, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	friend simdDouble operator -(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_sub_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_sub_pd(a,b);
#else
#ifdef __SSE__
		return _mm_sub_pd(a, b);
#endif
#endif
#endif
	}

	friend simdDouble operator -(const simdDouble &a, const double b) {
#ifdef __MIC__
		return _mm512_sub_pd(a,_mm512_set1_pd(b));
#else
#ifdef __AVX__
		return _mm256_sub_pd(a,_mm256_set1_pd(b));
#else
#ifdef __SSE__
		return _mm_sub_pd(a, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	friend simdDouble operator *(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_mul_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_mul_pd(a,b);
#else
#ifdef __SSE__
		return _mm_mul_pd(a, b);
#endif
#endif
#endif
	}

	friend simdDouble operator *(const simdDouble &a, const double b) {
#ifdef __MIC__
		return _mm512_mul_pd(a,_mm512_set1_pd(b));
#else
#ifdef __AVX__
		return _mm256_mul_pd(a,_mm256_set1_pd(b));
#else
#ifdef __SSE__
		return _mm_mul_pd(a, _mm_set1_pd(b));
#endif
#endif
#endif
	}


	friend simdDouble operator /(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_div_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_div_pd(a,b);
#else
#ifdef __SSE__
		return _mm_div_pd(a, b);
#endif
#endif
#endif
	}

	friend simdDouble operator /(const simdDouble &a, const double b) {
#ifdef __MIC__
		return _mm512_div_pd(a,_mm512_set1_pd(b));
#else
#ifdef __AVX__
		return _mm256_div_pd(a,_mm256_set1_pd(b));
#else
#ifdef __SSE__
		return _mm_div_pd(a, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	simdDouble& operator +=(const simdDouble &b) {
#ifdef __MIC__
		return *this = _mm512_add_pd(vec, b);
#else
#ifdef __AVX__
		return *this = _mm256_add_pd(vec, b);
#else
#ifdef __SSE__
		return *this = _mm_add_pd(vec, b);
#endif
#endif
#endif
	}

	simdDouble& operator +=(const double b) {
#ifdef __MIC__
		return *this = _mm512_add_pd(vec, _mm512_set1_pd(b));
#else
#ifdef __AVX__
		return *this = _mm256_add_pd(vec, _mm256_set1_pd(b));
#else
#ifdef __SSE__
		return *this = _mm_add_pd(vec, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	simdDouble& operator -=(const simdDouble &b) {
#ifdef __MIC__
		return *this = _mm512_sub_pd(vec, b);
#else
#ifdef __AVX__
		return *this = _mm256_sub_pd(vec, b);
#else
#ifdef __SSE__
		return *this = _mm_sub_pd(vec, b);
#endif
#endif
#endif
	}

	simdDouble& operator -=(const double b) {
#ifdef __MIC__
		return *this = _mm512_sub_pd(vec, _mm512_set1_pd(b));
#else
#ifdef __AVX__
		return *this = _mm256_sub_pd(vec, _mm256_set1_pd(b));
#else
#ifdef __SSE__
		return *this = _mm_sub_pd(vec, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	simdDouble& operator *=(const simdDouble &b) {
#ifdef __MIC__
		return *this = _mm512_mul_pd(vec, b);
#else
#ifdef __AVX__
		return *this = _mm256_mul_pd(vec, b);
#else
#ifdef __SSE__
		return *this = _mm_mul_pd(vec, b);
#endif
#endif
#endif
	}

	simdDouble& operator *=(const double b) {
#ifdef __MIC__
		return *this = _mm512_mul_pd(vec, _mm512_set1_pd(b));
#else
#ifdef __AVX__
		return *this = _mm256_mul_pd(vec, _mm256_set1_pd(b));
#else
#ifdef __SSE__
		return *this = _mm_mul_pd(vec, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	simdDouble& operator /=(const simdDouble &b) {
#ifdef __MIC__
		return *this = _mm512_div_pd(vec, b);
#else
#ifdef __AVX__
		return *this = _mm256_div_pd(vec, b);
#else
#ifdef __SSE__
		return *this = _mm_div_pd(vec, b);
#endif
#endif
#endif
	}

	simdDouble& operator /=(const double b) {
#ifdef __MIC__
		return *this = _mm512_div_pd(vec, _mm512_set1_pd(b));
#else
#ifdef __AVX__
		return *this = _mm256_div_pd(vec, _mm256_set1_pd(b));
#else
#ifdef __SSE__
		return *this = _mm_div_pd(vec, _mm_set1_pd(b));
#endif
#endif
#endif
	}

	/* Unary '-' */
	simdDouble operator -() const {
		return simdDouble(0.0) - *this;
	}
	simdDouble operator -() {
		return simdDouble(0.0) - *this;
	}

	/* Min and Max */
	friend simdDouble min(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_min_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_min_pd(a,b);
#else
#ifdef __SSE__
		return _mm_min_pd(a, b);
#endif
#endif
#endif
	}
	friend simdDouble max(const simdDouble &a, const simdDouble &b) {
#ifdef __MIC__
		return _mm512_max_pd(a,b);
#else
#ifdef __AVX__
		return _mm256_max_pd(a,b);
#else
#ifdef __SSE__
		return _mm_max_pd(a, b);
#endif
#endif
#endif
	}

	/* Reduce functions */
	friend double reduce_add(const simdDouble &a) {
#ifdef __MIC__
		return _mm512_reduce_add_pd(a);
#else
#ifdef __AVX__
		__attribute__((aligned(16))) double d[4];
		_mm256_store_pd(d, a);
		return d[3] + d[2] + d[1] + d[0];
#else
#ifdef __SSE__
		__attribute__((aligned(16))) double d[2];
		_mm_store_pd(d, a);
		return d[1] + d[0];
#endif
#endif
#endif
	}

	friend double reduce_mul(const simdDouble &a) {
#ifdef __MIC__
		return _mm512_reduce_mul_pd(a);
#else
#ifdef __AVX__
		__attribute__((aligned(16))) double d[4];
		_mm256_store_pd(d, a);
		return d[3] * d[2] * d[1] * d[0];
#else
#ifdef __SSE__
		__attribute__((aligned(16))) double d[2];
		_mm_store_pd(d, a);
		return d[1] * d[0];
#endif
#endif
#endif
	}

	friend double reduce_max(const simdDouble &a) {
#ifdef __MIC__
		return _mm512_reduce_max_pd(a);
#else
#ifdef __AVX__
		__attribute__((aligned(16))) double d[4];
		_mm256_store_pd(d, a);
		return std::max(std::max(d[3],d[2]), std::max(d[1],d[0]));
#else
#ifdef __SSE__
		__attribute__((aligned(16))) double d[2];
		_mm_store_pd(d, a);
		return std::max(d[1], d[0]);
#endif
#endif
#endif
	}
	friend double reduce_min(const simdDouble &a) {
#ifdef __MIC__
		return _mm512_reduce_min_pd(a);
#else
#ifdef __AVX__
		__attribute__((aligned(16))) double d[4];
		_mm256_store_pd(d, a);
		return std::min(std::min(d[3],d[2]), std::min(d[1],d[0]));
#else
#ifdef __SSE__
		__attribute__((aligned(16))) double d[2];
		_mm_store_pd(d, a);
		return std::min(d[1], d[0]);
#endif
#endif
#endif
	}

	/* Square Root */
	friend simdDouble sqrt(const simdDouble& a) {
#ifdef __MIC__
		return _mm512_sqrt_pd(a);
#else
#ifdef __AVX__
		return _mm256_sqrt_pd(a);
#else
#ifdef __SSE__
		return _mm_sqrt_pd(a);
#endif
#endif
#endif
	}

	/* Compare */
#ifdef __AVX__
	friend CCS::simdDoubleMask _mm256_cmpeq_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_EQ_OQ);}
	friend CCS::simdDoubleMask _mm256_cmplt_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_LT_OS);}
	friend CCS::simdDoubleMask _mm256_cmple_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_LE_OS);}
	friend CCS::simdDoubleMask _mm256_cmpunord_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_UNORD_Q);}
	friend CCS::simdDoubleMask _mm256_cmpneq_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_NEQ_UQ);}
	friend CCS::simdDoubleMask _mm256_cmpnlt_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_NLT_US);}
	friend CCS::simdDoubleMask _mm256_cmpnle_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_NLE_US);}
	friend CCS::simdDoubleMask _mm256_cmpord_pd(const simdDouble &a, const simdDouble &b){return _mm256_cmp_pd(a, b, _CMP_ORD_Q);}
#endif

#ifdef __MIC__
#define mmDouble_COMP(op) \
		friend CCS::simdDoubleMask cmp##op (const simdDouble &a, const simdDouble &b){ return _mm512_cmp##op##_pd_mask(a,b); }
#else
#ifdef __AVX__
#define mmDouble_COMP(op) \
		friend CCS::simdDoubleMask cmp##op (const simdDouble &a, const simdDouble &b){ return _mm256_cmp##op##_pd(a,b); }
#else
#ifdef __SSE__
#define mmDouble_COMP(op) \
		friend CCS::simdDoubleMask cmp##op (const simdDouble &a, const simdDouble &b){ return _mm_cmp##op##_pd(a,b); }
#endif
#endif
#endif
	mmDouble_COMP(eq)
    mmDouble_COMP(lt)
	mmDouble_COMP(le)
	mmDouble_COMP(unord)
	mmDouble_COMP(neq)
	mmDouble_COMP(nlt)
	mmDouble_COMP(nle)
	mmDouble_COMP(ord)
#undef mmDOuble_COMP

	/* Math */
#ifdef __INTEL_COMPILER
#ifdef __MIC__
#define mmDouble_MATH(func) \
		friend simdDouble func(const simdDouble& a) \
        { return  _mm512_##func##_pd(a);}
#else
#ifdef __AVX__
#define mmDouble_MATH(func) \
		friend simdDouble func(const simdDouble& a) \
        { return  _mm256_##func##_pd(a);}
#else
#ifdef __SSE__
#define mmDouble_MATH(func) \
		friend simdDouble func(const simdDouble& a) \
        { return  _mm_##func##_pd(a);}
#endif
#endif
#endif
	mmDouble_MATH(acos)
	mmDouble_MATH(acosh)
	mmDouble_MATH(asin)
	mmDouble_MATH(asinh)
	mmDouble_MATH(atan)
	mmDouble_MATH(atanh)
	mmDouble_MATH(cbrt)
	mmDouble_MATH(ceil)
	mmDouble_MATH(cos)
	mmDouble_MATH(cosh)
	mmDouble_MATH(erf)
	mmDouble_MATH(erfc)
	mmDouble_MATH(erfinv)
	mmDouble_MATH(exp2)
	mmDouble_MATH(exp)
	mmDouble_MATH(floor)
	mmDouble_MATH(invsqrt)
	mmDouble_MATH(log10)
	mmDouble_MATH(log2)
	mmDouble_MATH(log)
//	mmDouble_MATH(nearbyint)
//	mmDouble_MATH(rint)
	mmDouble_MATH(svml_round)
	mmDouble_MATH(sin)
	mmDouble_MATH(sinh)
	mmDouble_MATH(tan)
	mmDouble_MATH(tanh)
	mmDouble_MATH(trunc)
#undef mmDouble_MATH


#ifdef __MIC__
#define mmDouble_MATH2(func) \
	    friend simdDouble func(const simdDouble& a, const simdDouble& b) \
	        { return  _mm512_##func##_pd(a, b);}
#else
#ifdef __AVX__
#define mmDouble_MATH2(func) \
	    friend simdDouble func(const simdDouble& a, const simdDouble& b) \
	        { return  _mm256_##func##_pd(a, b);}
#else
#ifdef __SSE__
#define mmDouble_MATH2(func) \
	    friend simdDouble func(const simdDouble& a, const simdDouble& b) \
	        { return  _mm_##func##_pd(a, b);}
#endif
#endif
#endif
	mmDouble_MATH2(atan2)
	mmDouble_MATH2(hypot)
	mmDouble_MATH2(pow)
#undef mmDouble_MATH2
#endif

	friend std::ostream& operator<<(std::ostream &os, const simdDouble &a) {
		double *fp = (double*) &a;
		os << "{" <<
#ifdef __MIC__
		*(fp + 7) << ", " << *(fp + 6) << ", " << *(fp + 5) << ", " << *(fp + 4) << ", " <<
#endif
#if defined (__AVX__) || defined (__MIC__)
		*(fp + 3) << ", " << *(fp + 2) << ", " <<
#endif
#if defined (__SSE__) || defined (__MIC__)
		*(fp + 1) << ", " << *(fp + 0) <<
#endif
		"}";
		return os;
	}

	/* Element Access Only, no modifications to elements*/
	const double& operator[](const int i) const {
		double *fp = (double*) &vec;
		return *(fp + i);
	}

	/* Element Access and Modification*/
	double& operator[](const int i) {
		/* Assert enabled only during debug /DDEBUG */
		double *fp = (double*) &vec;
		return *(fp + i);
	}
};

inline simdDouble simdMask_mov(const simdDouble &vec_false, const CCS::simdDoubleMask &mask, const simdDouble &vec_true) {
#ifdef __MIC__
	return _mm512_mask_mov_pd(vec_false,mask,vec_true);
#else
#ifdef __AVX__
	return _mm256_blendv_pd(vec_false,vec_true,mask);
#else
#if defined (__SSE4_2__) || defined (__SSE4_1__)
	return _mm_blendv_pd(vec_false,vec_true,mask);
#else
#ifdef __SSE__
	return _mm_add_pd(_mm_and_pd(vec_true, mask), _mm_andnot_pd(mask, vec_false));
#endif
#endif
#endif
#endif
}

inline simdDouble operator *(const double b, const simdDouble &a) {
	return a * b;
}

inline simdDouble vec_abs(const simdDouble &vec_p) {
	simdDouble vec_np = -vec_p;
	return max(vec_np, vec_p);
}

inline simdDouble vec_sign(const simdDouble &vec_p) {
	simdDouble vec_zero(0.0);
	simdDouble vec_psone(1.0);
	simdDouble vec_ngone(-1.0);
	simdDoubleMask mask = cmplt(vec_p, vec_zero);

	return simdMask_mov(vec_psone, mask, vec_ngone);
}
} /* namespace CCS */

#endif /* SIMDDOUBLE_H_ */
