/*
 * main.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: fuji
 */
#pragma offload_attribute (push, target(mic))
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "simdDoubleMask.h"
#include "simdDouble.h"
#pragma offload_attribute (pop)

int main(int argc, char **argv) {

#pragma offload target(mic)
	{
#ifdef __MIC__
		CCS::simdDoubleMask a = CCS::simdDoubleMask(false,true,false,true,false,true,false,true);
#else
#ifdef __AVX__
		CCS::simdDoubleMask a = CCS::simdDoubleMask(false,true,false,true);
#else
#ifdef __SSE__
		CCS::simdDoubleMask a = CCS::simdDoubleMask(false, true);
#endif
#endif
#endif
		CCS::simdDoubleMask b;
		std::cout << "a=" << a << std::endl;
		std::cout << "b=" << b << std::endl;
		std::cout << "a & b=" << (a & b) << std::endl;
		std::cout << "a | b=" << (a | b) << std::endl;
		std::cout << "a ^ b=" << (a ^ b) << std::endl;
		std::cout << "~a=" << (~a) << std::endl;

		CCS::simdDoubleMask c;
		c &= a;
		std::cout << "c&=a " << c << std::endl;
		c |= a;
		std::cout << "c|=a " << c << std::endl;
		c ^= a;
		std::cout << "c^=a " << c << std::endl;

		std::cout << "a andnot b=" << (a.andn(b)) << std::endl;
		std::cout << "a andnotr b=" << (a.andnr(b)) << std::endl;
	}

	const unsigned int size= 50000000;
	double *xvec;
	double *yvec;
	double *zvec;
	posix_memalign((void **)&xvec, 64, (size + CCS::strideDouble) * sizeof(double));
	posix_memalign((void **)&yvec, 64, (size + CCS::strideDouble) * sizeof(double));
	posix_memalign((void **)&zvec, 64, (size + CCS::strideDouble) * sizeof(double));
	for (int i = 0 ; i < size ; i++){
		xvec[i] = (i % 100) / 50.0;
		yvec[i] = (i % 200) / 100.0;
	}

#pragma offload target(mic) in(size), in(xvec:length(size)),in(yvec:length(size)),out(zvec:length(size))
	{
		for (int i = 0 ; i < size ; i+= CCS::strideDouble){
			CCS::simdDouble x = &xvec[i];
			CCS::simdDouble y = &yvec[i];
			*(CCS::simdDouble *)&(zvec[i]) =  x + y;
		}
	}
	double err = 0.0;
	for (int i = 0 ; i < size ; i++){
		err += xvec[i] + yvec[i] - zvec[i];
	}
	std::cout << "err=" << err << std::endl;
	free(xvec);
	free(yvec);
	free(zvec);

#pragma offload target(mic)
	{
		__attribute__((aligned(64))) double data[8] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8};
		CCS::simdDouble vec_dat = &data[0];

		std::cout << "vec_dat=" << vec_dat << std::endl;
		std::cout << "vec_dat + vec_dat=" << vec_dat + vec_dat << std::endl;
		std::cout << "vec_dat - vec_dat=" << vec_dat - vec_dat << std::endl;
		std::cout << "vec_dat * vec_dat=" << vec_dat * vec_dat << std::endl;
		std::cout << "vec_dat * 2=" << vec_dat * 2 << std::endl;
		std::cout << "-vec_dat=" << -vec_dat << std::endl;
		std::cout << "2 * vec_dat=" << 2 * vec_dat << std::endl;
		std::cout << "vec_dat/2=" << vec_dat / 2 << std::endl;
		std::cout << "vec_dat/vec_dat=" << vec_dat / vec_dat << std::endl;
		std::cout << "sum vec_dat=" << reduce_add(vec_dat) << std::endl;
		std::cout << "mul vec_dat=" << reduce_mul(vec_dat) << std::endl;
#ifdef __INTEL_COMPILER
		std::cout << "sin(vec_dat)=" << sin(vec_dat) * 2 << std::endl;
#endif
		CCS::simdDouble vec_zt(0.2);
		CCS::simdDoubleMask mask = cmplt(vec_dat, vec_zt);
		std::cout << "<0.2 : " << mask << std::endl;
		CCS::simdDouble vec_dat2 = simdMask_mov(vec_dat, mask, vec_zt);
		std::cout << "vec_dat2=" << vec_dat2 << std::endl;
		CCS::simdDouble vec_dat3 = vec_dat - vec_zt;
		std::cout << "vec_dat3=" << vec_dat3 << std::endl;
		std::cout << "|vec_dat3|=" << vec_abs(vec_dat3) << std::endl;
	}

	return 0;
}

