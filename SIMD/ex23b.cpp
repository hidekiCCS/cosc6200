//
// Large Scale Computing
// SIMD Test
// ex21.cpp 
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <emmintrin.h>

int main(int argc, char **argv){
  if (argc < 2){
    std::cout << argv[0] << " [integer number]\n";
    return -1;
  }
  int num = std::atoi(argv[1]); // Get a number from commandline

  int size = (num + 1) / 2 * 2 * sizeof(double);
  double *a = (double *)_mm_malloc(size, 16);
  double *b = (double *)_mm_malloc(size, 16);
  double *c = (double *)_mm_malloc(size, 16);
  double *d = (double *)_mm_malloc(size, 16);

  for (int n = 0 ; n < num ; n++){
    a[n] = n;
    b[n] = num - n;
    c[n] = n;
    d[n] = n;
  }
  // set output format
  std::cout << std::setprecision(16);
  std::cout << std::scientific;

  // ordinary loop
  double start = clock();
  for (int n = 0 ; n < num ; n++){
    d[n] = a[n] * b[n];
  }
  std:: cout << "NORMAL:" << (clock() - start)/ CLOCKS_PER_SEC << " SEC" << std::endl;

  //// SIMD
  start = clock();
  int ne = (num + 1 ) / 2 * 2;
  for (int i = 0; i < ne; i += 2) {
	  _mm_store_pd(&c[i], _mm_mul_pd(_mm_load_pd(&a[i]), _mm_load_pd(&b[i])));
  }
  std:: cout << "SIMD :" << (clock() - start)/ CLOCKS_PER_SEC << " SEC" << std::endl;

  double err = 0.0;
  for (int n = 0 ; n < num ; n++){
    err += (c[n] - d[n]) * (c[n] - d[n]);
  }
  std::cout << "err=" << err << std::endl;

  return 0;
}
