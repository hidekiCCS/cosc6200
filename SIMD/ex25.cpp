/*
 * ex25.cpp
 *
 *  Created on: Sep 29, 2015
 *      Author: fuji
 */

#include <iostream>
#include <cstdlib>
#include <cmath>
#include "simdDoubleMask.h"
#include "simdDouble.h"

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << argv[0] << " [size]\n";
		return 0;
	}

	int size = std::atoi(argv[1]);
	std::cout << "Size=" << size << std::endl;

	// allocation
	int asize = ((size + CCS::strideDouble - 1) / CCS::strideDouble) * CCS::strideDouble;
	double *xvec;
	posix_memalign((void **)&xvec, 64, asize * sizeof(double));
	double *bvec;
	posix_memalign((void **)&bvec, 64, asize * sizeof(double));
	// Make 2D array
	double **amat = new double*[size];
	for (int i = 0 ; i < size ; i++){
		posix_memalign((void **)&amat[i], 64, asize * sizeof(double));
	}

	// setup matrix A elements
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			amat[i][j] = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}
	// setup vector x elements
	for (int i = 0; i < size; i++) {
		xvec[i] = i;
	}

	/// compute b = a*x ///
	for (int i = 0; i < size ; i++) {
		bvec[i] = 0.0;
	}
	double start = clock();
	for (int i = 0; i < size ; i++) {
		for (int j = 0; j < size ; j++) {
			bvec[i] += amat[i][j] * xvec[j];
		}
	}
	double tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "Time cost = " << tcost << "(sec)\n";

	/// compute b = a*x again ///
	for (int i = 0; i < size ; i++) {
		bvec[i] = 0.0;
	}
	start = clock();
	for (int i = 0; i < size ; i++) {
		CCS::simdDouble bvec_v(0.0);
		for (int j = 0; j < size ; j+=CCS::strideDouble) {
			CCS::simdDouble xvec_v = &xvec[j];
			CCS::simdDouble amt_v = &amat[i][j];
			bvec_v += amt_v * xvec_v;
		}
		bvec[i] = reduce_add(bvec_v);
	}
	tcost = (clock() - start) / CLOCKS_PER_SEC;
	std::cout << "Time cost = " << tcost << "(sec)\n";

	delete [] xvec;
	delete [] bvec;
	for (int i = 0 ; i < size ; i++){
		delete [] amat[i];
	}
	delete [] amat;

	return 0;
}

