%
% PI/4=sum(-1)^i/(2i+1)
% computePI.m
%
%
function computePI(n)
%
% In command syntax, n is a string
if ischar(n) == 1
    disp('n needs conversion')
    n = str2double(n);  % convert string to double
end
tic;
%
x=0.0;
for i = 0:2:n
    x = x + 1 / (2 * i + 1);
    x = x - 1 / (2 * i + 3);
end
toc;
str = sprintf('n=%d Pi=%1.16f\n',n,4.0 * x);
disp(str);

