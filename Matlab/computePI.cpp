/*
  CPP sample
  Approximate PI
 */
#include <iostream>
#include <iomanip>
#include <cstdlib>

int main (int argc, char *argv[]) {
  int i,n;
  double x;

  if (argc < 2){
    std::cout << argv[0] << " [number of terms]\n";
    exit(-1);
  }

  n = atoi(argv[1]);
  std::cout << "Start n=" << n << std::endl;

  double start = clock();
  x=0.0;
  for (i = 0 ; i < n ; i+=2){
    x += 1.0 / (double)(2 * i + 1);
    x -= 1.0 / (double)(2 * i + 3);
  }
  double tcost = (clock() - start) / CLOCKS_PER_SEC;
  std::cout << std::setprecision(16);
  std::cout << std::scientific;
  std::cout << "n=" << n <<  " Pi=" << 4.0 * x << std::endl;
  std::cout << "Time cost = " << tcost << "(sec)\n";
}
