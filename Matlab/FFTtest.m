%
% FFT test "FFTtest.m"
%
LASTN = maxNumCompThreads(str2num(getenv('OMP_NUM_THREADS')));
nth = maxNumCompThreads;
fprintf('Number of Threads = %d.\n',nth);

N=2^(14);
A = randn(N);
st = cputime;
tic;
B = fft2(A);
realT = toc;
cpuT = cputime -st;
fprintf('Real Time = %f(sec)\n',realT);
fprintf('CPU Time = %f(sec)\n',cpuT);
fprintf('CPU/Real = %f\n',cpuT / realT);
