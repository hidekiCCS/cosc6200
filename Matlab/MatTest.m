%
% Matrix test "MatTest.m"
%
LASTN = maxNumCompThreads(str2num(getenv('OMP_NUM_THREADS')));
nth = maxNumCompThreads;
fprintf('Number of Threads = %d.\n',nth);

A = rand(10000, 10000);
B = rand(10000, 10000);
st = cputime;
tic;
C = A * B;
realT = toc;
cpuT = cputime -st;
fprintf('Real Time = %f(sec)\n',realT);
fprintf('CPU Time = %f(sec)\n',cpuT);
fprintf('CPU/Real = %f\n',cpuT / realT);
