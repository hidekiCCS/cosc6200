//
//  Large Scale Computing
//  Convective Heat/Mass Transfer
//  ex42.cpp : use Lapack 
//
//  D d2phi/dx2 + x = 0
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <sys/time.h>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
extern "C" {
#include <cblas.h>
	int dgtsv_(int *, int *, double *, double *, double *, double *, int *, int *);
}
#endif
#endif


int main(int argc, char **argv) {

#ifdef MKL_ILP64
  MKL_INT num = 7; // number of points is 7
#else
  int num = 7;
#endif
  const double a = 1; // phi at left end is 1
  const double b = 2; // phi at right end is 2
  const double d = 0.1; // Diffusion coefficient is 0.1
  
  // Memory Allocation
#ifdef MKL_ILP64
  double *phi = (double *)mkl_malloc(num * sizeof(double),64);
#else
  double *phi = new double[num];
#endif
  // Setup
  double dx = 1.0 / (num - 1);
  for (int i = 0; i < num; i++) {
    phi[i] = 0.0;
  }
  phi[0] = a;
  phi[num-1] = b;
  
  /* making tri-digonal matrix */
#ifdef MKL_ILP64
  MKL_INT size = num - 2; // number of inner points is 5
  double *diag = (double *)mkl_malloc(size * sizeof(double),64);
  double *udiag = (double *)mkl_malloc((size - 1) * sizeof(double),64);
  double *ldiag = (double *)mkl_malloc((size - 1) * sizeof(double),64);
  double *sol = (double *)mkl_malloc(size * sizeof(double),64);	
#else
  int size = num - 2;
  double *diag = new double[size];
  double *udiag = new double[size - 1];
  double *ldiag = new double[size - 1];
  double *sol = new double[size];
#endif

  // set up Matrix and RHS
  sol[0] = -dx * dx / d - a;
  sol[1] = -dx * dx / d;
  sol[2] = -dx * dx / d;
  sol[3] = -dx * dx / d;
  sol[4] = -dx * dx / d - b;
  diag[0] = -2.0;
  diag[1] = -2.0;
  diag[2] = -2.0;
  diag[3] = -2.0;
  diag[4] = -2.0;
  udiag[0] = 1.0;
  udiag[1] = 1.0;
  udiag[2] = 1.0;
  udiag[3] = 1.0;
  ldiag[0] = 1.0;
  ldiag[1] = 1.0;
  ldiag[2] = 1.0;
  ldiag[3] = 1.0;

#ifdef MKL_ILP64
  MKL_INT nrhs = 1; /* # of RHS = 1*/
  MKL_INT info;
#else
  int nrhs = 1; /* # of RHS = 1*/
  int info;
#endif
  dgtsv_(&size, &nrhs, ldiag, diag, udiag, sol, &size, &info);

  if (info == 0) {
    std::cout << "successfully done\n";
  }
  if (info < 0) {
    std::cout << "the " << -info << "-th argument had an illegal value\n";
    return -1;
  }
  if (info > 0) {
    std::cout << "U(" << info << "," << info << ") is exactly zero.\n";
    return -1;
  }

  // restore solution
  for (int i = 1; i < num - 1; i++) {
    phi[i] = sol[i-1];
  }

  std::ofstream ofile;
  ofile.open("result.dat");
  ofile << std::setprecision(16);
  ofile << std::scientific;
  for (int i = 0; i < num; i++) {
    ofile << dx * i << " " << phi[i] << std::endl;
  }
  ofile.close();

  // clean up
#ifdef MKL_ILP64
  mkl_free(phi);
  mkl_free(sol);
  mkl_free(diag);
  mkl_free(udiag);
  mkl_free(ldiag);
#else
  delete[] phi;
  delete[] sol;
  delete[] diag;
  delete[] udiag;
  delete[] ldiag;
#endif 

  std::cout << "Done\n";
  return 0;
}
