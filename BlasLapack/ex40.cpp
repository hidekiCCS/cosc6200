/*
 * ex40.cpp
 * Blas Level 3 : dgemm test
 *  Created on: Oct 21, 2015
 *      Author: fuji
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <sys/time.h>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#else
extern "C" {
#include <cblas.h>
}
#endif
#endif
#include <Eigen/Core>

#ifdef _OPENMP
#include <omp.h>
#endif

#define MTSIZE 2000

// timing method
double tsecond() {
  struct timeval tm;
  double t;
  static int base_sec = 0, base_usec = 0;

  gettimeofday(&tm, NULL);
  if (base_sec == 0 && base_usec == 0) {
    base_sec = tm.tv_sec;
    base_usec = tm.tv_usec;
    t = 0.0;
  } else {
    t = (double) (tm.tv_sec - base_sec) + ((double) (tm.tv_usec - base_usec)) / 1.0e6;
  }
  return t;
}

/* my simple dgemm */
void mydgemm(int m, int n, double al, double *a, double *b, double bt,	double *c) {
  /* compute C := alpha * AB + beta * C */
  // Matrix Elements stored in Column Major
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      c[i * n + j] *= bt;
      for (int jj = 0; jj < n; jj++) {
	c[i * n + j] += al * a[i * n + jj] * b[jj * n + j];
      }
    }
  }
}

int main(int argc, char **argv){
#ifdef MKL_ILP64
  const MKL_INT size= MTSIZE;
#else
  const unsigned int size= MTSIZE;
#endif

  /* alloc vector and matrix */
#ifdef MKL_ILP64
  double *cmat = (double *)mkl_malloc(size * size * sizeof(double), 64);
  double *bmat = (double *)mkl_malloc(size * size * sizeof(double), 64);
  double *amat = (double *)mkl_malloc(size * size * sizeof(double), 64);
#else
  double *cmat = new double[size * size];
  double *bmat = new double[size * size];
  double *amat = new double[size * size];
#endif

  double alpha, beta;

  /* setup matrix A B C*/
  // Row Major
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      amat[i * size + j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
      bmat[i * size + j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
      cmat[i * size + j] = 0.0;
    }
  }

  double ts, te;

  /* calculate bvec = alpha*amat*bmat + beta*cmat */
  alpha = 1.0;
  beta = 0.0;

  /* call my dgemm */
  ts = tsecond();
  mydgemm(size, size, alpha, amat, bmat, beta, cmat);
  te = tsecond();
  std::cout << "|C|=" << cblas_dnrm2(size * size, cmat, 1) << std::endl;
  std::cout << "Time : my dgemm = " << 1.0 * (te - ts) << "(sec)\n";

  /* call blas dgemm */
  std::fill(cmat, cmat + size * size, 0.0);
  ts = tsecond();
  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1.0, amat, size, bmat, size, 0.0, cmat, size);
  te = tsecond();
  std::cout << "|C|=" << cblas_dnrm2(size * size, cmat, 1) << std::endl;
  std::cout << "Time : cblas dgemm = " << 1.0 * (te - ts) << "(sec)\n";

  // cleanup
#ifdef MKL_ILP64
  mkl_free(cmat);
  mkl_free(bmat);
  mkl_free(amat);
#else
  delete[] cmat;
  delete[] bmat;
  delete[] amat;
#endif

  /* setup Eigen3  */
  Eigen::MatrixXd mA(size,size);
  Eigen::MatrixXd mB(size,size);
  Eigen::MatrixXd mC(size,size);

  /* setup matrix A B C */
  for (int i = 0 ; i < size ; i++){
    for (int j = 0 ; j < size ; j++){
      mA(i,j) = i	+ 100.0 / (1.0 + fabs((double) i - (double) j));
      mB(i,j) = i	+ 100.0 / (1.0 + fabs((double) i - (double) j));
    }
  }

  ts =  tsecond();
  mC = mA * mB;
  te = tsecond();
  std::cout << "|C|=" << mC.lpNorm<2>()  << std::endl;
  std::cout << "Time cost for Eigen3 dgemm = " << 1.0 * (te - ts) << "(sec)\n";
}

