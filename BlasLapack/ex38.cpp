//
//  Large Scale Computing
//  
//  Blas Level 1 : norm2 test
//  ex38.cpp
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <sys/time.h>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#else
extern "C" {
#include <cblas.h>
}
#endif
#endif
#include <Eigen/Core>

#ifdef _OPENMP
#include <omp.h>
#endif

#define BIGNUMBER 500000000

// timing method
double tsecond() {
  struct timeval tm;
  double t;
  static int base_sec = 0, base_usec = 0;

  gettimeofday(&tm, NULL);
  if (base_sec == 0 && base_usec == 0) {
    base_sec = tm.tv_sec;
    base_usec = tm.tv_usec;
    t = 0.0;
  } else {
    t = (double) (tm.tv_sec - base_sec) + ((double) (tm.tv_usec - base_usec)) / 1.0e6;
  }
  return t;
}

/* my norm2 */
double mynorm2(unsigned int n,double *x){
  double nrm = 0.0;
  for (unsigned int i = 0 ; i < n ; i++){
    nrm +=  x[i] * x[i];
  }
  return std::sqrt(nrm);
}

int main(int argc, char **argv){
#ifdef MKL_ILP64
  const MKL_INT size= BIGNUMBER;
#else
  const unsigned int size= BIGNUMBER;
#endif
  double nrm;

  std::cout << std::setprecision(16);
	
  /* alloc vector and setting */
#ifdef MKL_ILP64
  double *xvec = (double *)mkl_malloc(size * sizeof(double), 64);
#else
  double *xvec = new double[size];
#endif
  for (int i = 0 ; i < size ; i++) xvec[i] = (i % 100) / 50.0;  

  double	 ts,te;
  /* call my norm 2*/
  ts = tsecond();
  nrm = mynorm2(size,xvec);
  te = tsecond();
  std::cout << "MY NORM=" << nrm << std::endl;
  std::cout << "Time cost for my norm = " << 1000.0 * (te - ts)  << "(msec)\n";
 
  /* call blas norm2 */
  ts = tsecond();
  nrm = cblas_dnrm2(size,xvec,1); 
  te = tsecond();
  std::cout << "BLAS NORM=" << nrm << std::endl;
  std::cout << "Time cost for cblas norm = " << 1000.0 * (te - ts) << "(msec)\n";
  
  // cleanup
#ifdef MKL_ILP64
  mkl_free(xvec);
#else
  delete [] xvec;
#endif
  /* call Eigen3 norm2 */
  Eigen::VectorXd vec(size);
  for (int i = 0 ; i < size ; i++) vec[i] = (i % 100) / 50.0;  
  ts = tsecond();
  nrm = vec.norm(); 
  te = tsecond();
  std::cout << "Eigen3 NORM=" << nrm << std::endl;
  std::cout << "Time cost for Eigen3 norm = " << 1000.0 * (te - ts) << "(msec)\n";
}


