# BLAS
## Compile with ATLAS
module load atlas 
module load eigen
g++ -O3 -o ex38 ex38.cpp -lcblas -latlas

## Intel C++ compiler and MKL
module load eigen
module load intel-psxe
icpc -O3 -o ex38 ex38.cpp -mkl=sequential -DMKL_ILP64

### Parallel
icpc -O3 -o ex38 ex38.cpp -mkl=parallel -openmp -DMKL_ILP64


# LAPACK
## Compile with ATLAS
module load atlas 
icpc -O3 -o ex41 ex41.cpp /share/apps/atlas/3.10.2/lib/liblapack.a /share/apps/atlas/3.10.2/lib/libf77blas.a -lcblas -latlas -lgfortran

## Intel C++ compiler and MKL
module load intel-psxe
icpc -O3 -o ex41 ex41.cpp -mkl=sequential -DMKL_ILP64

### Parallel
icpc -O3 -o ex41 ex41.cpp -mkl=parallel -openmp -DMKL_ILP64


