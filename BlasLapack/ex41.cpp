//
//  Large Scale Computing
//  
//  lapack : dgesv test
//  solve Ax=b : A is a dense matrix
//  ex41.cpp
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#define FORTRAN_LAPACK
#else
#ifdef MKL_ILP64
#include <mkl.h>
#define FORTRAN_LAPACK
#else
extern "C" {
#include <cblas.h>
#include <clapack.h>
}
#endif
#endif

#define MMSIZE 5000


int main(int argc, char **argv){
#ifdef MKL_ILP64
	MKL_INT size= MMSIZE;
#else
	int size= MMSIZE;
#endif


	/* alloc vector and matrix */
#ifdef MKL_ILP64
	double *xvec = (double *)mkl_malloc(size * sizeof(double), 64);
	double *bvec = (double *)mkl_malloc(size * sizeof(double), 64);
	double *amat = (double *)mkl_malloc(size * size * sizeof(double), 64);
#else
	double *xvec = new double[size];
	double *bvec = new double[size];
	double *amat = new double[size * size];
#endif

	/* setup matrix A */
	for (int i = 0 ; i < size ; i++){
		for (int j = 0 ; j < size ; j++){
			amat[i + size * j] = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}	
	/* setup vector x */
	for (int i = 0 ; i < size ; i++) xvec[i] = i; 

	double ts,te;
	
	/* setting RHS b */
	/* calculate bvec = alpha*amat*xvec + beta*bvec */
	cblas_dgemv(CblasColMajor,CblasNoTrans,size,size,1.0,amat,size,xvec,1,0.0,bvec,1);

	/*solve Ax=b */
	/* A (amat) is size x size matrix */
#ifdef MKL_ILP64
	MKL_INT *ipiv = new MKL_INT[size];
	MKL_INT nrhs=1;
	MKL_INT info;
#else
	int *ipiv = new int[size];
	int nrhs=1;
	int info;
#endif
	ts = std::clock();	
#ifdef FORTRAN_LAPACK 
	dgesv_(&size, &nrhs, amat, &size, ipiv, bvec, &size, &info);
#else
	info = clapack_dgesv(CblasColMajor, size, nrhs, amat, size, ipiv, bvec, size);
#endif
	te = std::clock();
	cblas_daxpy(size, -1.0, xvec, 1, bvec, 1.0);

	std::cout << "|x-x0|=" << cblas_dnrm2(size,bvec,1) << std::endl;
	std::cout << "Time cost for Lapack dgesv = " << (te - ts) / CLOCKS_PER_SEC << "(sec)\n";

	if (info == 0) std::cout << "successfully done\n";
	if (info < 0){
		std::cout << "the " << -info << "-th argument had an illegal value\n";
		return -1;
	}
	if (info > 0){
		std::cout << "U(" << info << "," << info << ") is exactly zero.\n";
		return -1;
	}
	/*
    info 
    = 0:  successful exit
    < 0:  if INFO = -i, the i-th argument had an illegal value
    > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization
    has been completed, but the factor U is exactly
    singular, so the solution could not be computed.
	 */

#ifdef MKL_ILP64
	mkl_free(xvec);
	mkl_free(bvec);
	mkl_free(amat);
#else
	delete [] xvec;
	delete [] bvec;
	delete [] amat;
#endif
	return 0;
}
