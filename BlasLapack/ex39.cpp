//
//  Large Scale Computing
//  
//  Blas Level 2 : dgemv test
//  ex39.cpp
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <sys/time.h>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#else
extern "C" {
#include <cblas.h>
}
#endif
#endif
#include <Eigen/Core>

#ifdef _OPENMP
#include <omp.h>
#endif

#define MTSIZE 10000

// timing method
double tsecond() {
  struct timeval tm;
  double t;
  static int base_sec = 0, base_usec = 0;

  gettimeofday(&tm, NULL);
  if (base_sec == 0 && base_usec == 0) {
    base_sec = tm.tv_sec;
    base_usec = tm.tv_usec;
    t = 0.0;
  } else {
    t = (double) (tm.tv_sec - base_sec) + ((double) (tm.tv_usec - base_usec)) / 1.0e6;
  }
  return t;
}

/* my simple dgemv */
void mydgemv(int m,int n,double al,double *a,double *x,double bt,double *b){
  /* compute b = al*a*x + bt*b */
  // a is Row major
  for (int i = 0 ; i < m ; i++){
    b[i] *= bt;
    for (int j = 0 ; j < n ; j++){
      b[i] += al * a[i * n + j] * x[j];
    }
  }
}

int main(int argc, char **argv){
#ifdef MKL_ILP64
  const MKL_INT size= MTSIZE;
#else
  const unsigned int size= MTSIZE;
#endif

  /* alloc vector and matrix */
#ifdef MKL_ILP64
  double *xvec = (double *)mkl_malloc(size * sizeof(double), 64);
  double *bvec = (double *)mkl_malloc(size * sizeof(double), 64);
  double *amat = (double *)mkl_malloc(size * size * sizeof(double), 64);
#else
  double *xvec = new double[size];
  double *bvec = new double[size];
  double *amat = new double[size * size];
#endif

  /* setup matrix A (Row Major)*/
  for (int i = 0 ; i < size ; i++){
    for (int j = 0 ; j < size ; j++){
      amat[i * size + j] = i + 100.0 / (1.0 + std::abs(i - j));
    }
  }
  
  /* setup vector x */
  for (int i = 0 ; i < size ; i++) xvec[i] = i; 
  std::fill(bvec,bvec + size,0.0);

  double ts,te;

  /* call my dgemv */
  ts = tsecond();
  mydgemv(size, size, 1.0, amat, xvec, 0.0, bvec);
  te = tsecond();
  std::cout << "|b|=" << cblas_dnrm2(size,bvec,1) << std::endl;
  std::cout << "Time cost for my dgemv = " << 1000.0 * (te - ts) << "(msec)\n";
  

  /* call blas dgemv */
  std::fill(bvec,bvec + size,0.0);
  ts = tsecond();	
  cblas_dgemv(CblasRowMajor,CblasNoTrans,size,size,1.0,amat,size,xvec,1,0.0,bvec,1);
  te = tsecond();
  std::cout << "|b|=" << cblas_dnrm2(size,bvec,1) << std::endl;
  std::cout << "Time cost for cblas dgemv = " << 1000.0 * (te - ts) << "(msec)\n";
	
#ifdef MKL_ILP64
  mkl_free(xvec);
  mkl_free(bvec);
  mkl_free(amat);
#else
  delete [] xvec;
  delete [] bvec;
  delete [] amat;
#endif
  /* setup Eigen3  */
  Eigen::VectorXd xv(size);
  Eigen::VectorXd bv(size);
  Eigen::MatrixXd mA(size,size);

  /* setup matrix A */
  for (int i = 0 ; i < size ; i++){
    for (int j = 0 ; j < size ; j++){
      mA(i,j) = i + 100.0 / (1.0 + std::abs(i - j));
    }
  }
  
  /* setup vector x */
  for (int i = 0 ; i < size ; i++) xv(i) = i; 
  bv.setZero();
	
  /* call Eigen3 norm2 */
  ts = tsecond();
  bv = mA * xv;
  te = tsecond();
  std::cout << "|b|=" << bv.norm()  << std::endl;
  std::cout << "Time cost for Eigen3 b=Ax = " << 1000.0 * (te - ts) << "(msec)\n";
}

