//
//  HW_A.cpp
//
//

#include <iostream>
#include "triangle.h"

using namespace std;

int main(int argc, const char * argv[]){
  // define three vertices
  vector2d a(2.0,1.0);
  vector2d b(4.0,5.0);
  vector2d c(1.0,2.0);

  // define a triangle
  triangle tr1(a,b,c);

  // print vertices
  for (int i = 0 ; i < 3 ; i++){
    a = tr1.get_vertex(i);
    cout << "(" << a.get_x() << "," << a.get_y() << ")" << endl;
  }

  cout << "area=" << tr1.area() << endl;
  cout << "perimeter=" << tr1.perimeter() << endl;
}

