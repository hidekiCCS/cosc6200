//
// C++ sample
// vector2d.cpp
//
//

#include "vector2d.h"

vector2d::vector2d(){
  x = 0.0;
  y = 0.0;
}

vector2d::vector2d(double xi,double yi){
  x = xi;
  y = yi;
}

vector2d::~vector2d(){
  //cout << "destructor was called" << endl;
}

void vector2d::set_xy(double xi,double yi){
  x = xi;
  y = yi;    
}

double vector2d::get_x(void ){
    return x;
}

double vector2d::get_y(void ){
  return y;
}

double vector2d::norm(){
  double d;

  d = x * x + y * y;
  
  return sqrt(d);
}


// Operators
vector2d vector2d::operator+(vector2d p){
  // define + operator
  vector2d p0;  
  p0.x = this->x + p.x;
  p0.y = this->y + p.y;
  // 'this' points itself

  return p0;
}

vector2d vector2d::operator-(vector2d p){
  // define - operator
  vector2d p0;

  p0.x = this->x - p.x;
  p0.y = this->y - p.y;

  return p0;
}

void vector2d::operator+=(vector2d p){
  // define += operator
  this->x += p.x;
  this->y += p.y;
}

void vector2d::operator-=(vector2d p){
  // define -= operator
  this->x -= p.x;
  this->y -= p.y;
}

vector2d vector2d::operator*(double d){
  // define * operator when a 'double' variable in right side
  vector2d p0;

  p0.x = this->x * d;
  p0.y = this->y * d;

  return p0;
}

double vector2d::operator*(vector2d p){
  // define * operator when a 'vector2d' variable in right side
  double d;
  // compute dot product
  d = this->x * p.x + this->y * p.y;
  
  return d;
}

vector2d vector2d::operator/(double d){
  // define / operator when a 'double' variable in right side
  vector2d p0;

  p0.x = this->x / d;
  p0.y = this->y / d;

  return p0;
}
void vector2d::operator*=(double d){
  // define *= operator when a 'double' variable in right side
  this->x *= d;
  this->y *= d;
}

void vector2d::operator/=(double d){
  this->x /= d;
  this->y /= d;  
}

// Frientd function
vector2d operator*(double d, vector2d p){
  // define * operator when a 'double' variable in left side
  vector2d p0;
  
  p0.x = p.x * d;
  p0.y = p.y * d;

  return p0;
}

