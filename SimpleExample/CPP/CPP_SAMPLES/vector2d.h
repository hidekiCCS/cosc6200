//
// C++ sample
// vector2d.h
//
//

#ifndef __vector2d_h
#define __vector2d_h

#include <iostream>
#include <cmath>

using namespace std;

class vector2d{
  
public:
  // constructor
  vector2d();
  vector2d(double x,double y);

  // destructor
  ~ vector2d();

  // public member functions
  void set_xy(double xi,double yi);

  double get_x(void );
  double get_y(void );

  double norm(); // vector norm

  /// overloading operators
  vector2d operator+(vector2d p); // addition
  vector2d operator-(vector2d p); // subtraction
  void operator+=(vector2d p);    // addition & assign
  void operator-=(vector2d p);    // subraction & assign
  vector2d operator*(double d);   // multiplication
  double operator*(vector2d p);   // dot product
  vector2d operator/(double d);   // division
  void operator*=(double d);      // multiplication & assign
  void operator/=(double d);      // division & assign

  // friend function
  friend vector2d operator*(double d, vector2d p); // multiplication

private:
  // private member variables
  double x;
  double y;
};

#endif // __vector2d_h
