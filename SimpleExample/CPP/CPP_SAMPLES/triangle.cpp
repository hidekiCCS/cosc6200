//
// C++ 
// triangle.cpp
//
//

#include "triangle.h"

  // constructor
triangle::triangle(vector2d a,vector2d b,vector2d c){
  vertices[0] = a;
  vertices[1] = b;
  vertices[2] = c;
}

// destructor
triangle::~triangle(){
  // do nothing
}

// get vertex of index i
vector2d triangle::get_vertex(int i){
  // i should be 0,1,or 2
  if ((i < 0) && (i >=3)){
    cout << "illegal index" << endl;
    exit(-1);
  }
  return vertices[i];
}

void triangle::set_vertex(int i,vector2d x){
  // i should be 0,1,or 2
  if ((i < 0) && (i >=3)){
    cout << "illegal index" << endl;
    return;
  }
  vertices[i] = x;
}
