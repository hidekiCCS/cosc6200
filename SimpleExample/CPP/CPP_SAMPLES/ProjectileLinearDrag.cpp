//
//  ProjectileLinearDrag.cpp
//  ProjectileNoDrag
//
//  Created by Hideki Fujioka on 4/23/13.
//  Copyright (c) 2013 Tulane University. All rights reserved.
//

#include <iostream>
#include "ProjectileLinearDrag.h"

// constructor
ProjectileLinearDrag::ProjectileLinearDrag(vector2d icord,vector2d ivel,double cdrag, double dt)
    :Projectile(icord,ivel,dt),drag_coef(cdrag){
    
    update_acceleration();
}

// destructor
ProjectileLinearDrag::~ProjectileLinearDrag(){
    // do nothing
}

// update acceleration, velocity and coordinate
void ProjectileLinearDrag::update(){
    update_acceleration();
    this->Projectile::update();
}


// update acceleration
void ProjectileLinearDrag::update_acceleration(){
    acceleration.set_xy(0.0,-gravity);
    acceleration -= (drag_coef / weight) * velocity;
}
