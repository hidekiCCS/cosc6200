//
// C++ 
// triangle.h
//
//

#ifndef __triangle_h
#define __triangle_h

#include <iostream>
#include <cmath>
#include "vector2d.h"

using namespace std;

class triangle{
  
public:
  // constructor
  triangle(vector2d a,vector2d b,vector2d c);

  // destructor
  ~triangle();

  // public member functions
  vector2d get_vertex(int i);
  void set_vertex(int i,vector2d x);

private:
  // private member variables
  vector2d vertices[3];
};  

#endif // __triangle_h
