//
//  Projectile.h
//  ProjectileNoDrag
//
//  Created by Hideki Fujioka on 4/18/13.
//  Copyright (c) 2013 Tulane University. All rights reserved.
//

#ifndef ProjectileNoDrag_Projectile_h
#define ProjectileNoDrag_Projectile_h

#include "vector2d.h"

class Projectile{
public:
    // constructor
    Projectile(vector2d icord,vector2d ivel,double dt);
    
    // destructor
    ~Projectile();
    
    // update velocity and coordinate
    virtual void update();
    
    // get acceleration;
    vector2d get_acceleration();
    
    // get velocity;
    vector2d get_velocity();
    
    // get_coordinate
    vector2d get_coordinate();
    
protected:
    vector2d acceleration;
    vector2d velocity;
    vector2d cord;
    
    double dt;
    
    // gravitational coefficient
    double gravity;
    
    // Weight
    double weight;
};
#endif
