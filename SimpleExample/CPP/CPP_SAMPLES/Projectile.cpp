//
//  Projectile.cpp
//  ProjectileNoDrag
//
//  Created by Hideki Fujioka on 4/18/13.
//  Copyright (c) 2013 Tulane University. All rights reserved.
//

#include <iostream>
#include "Projectile.h"

// constructor
Projectile::Projectile(vector2d icord,vector2d ivel,double dt):cord(icord),velocity(ivel),dt(dt){
    
    // gravitational coefficient
    gravity = 9.8;
    
    // Weight
    weight = 1.0;
    
    // Acceleration
    acceleration.set_xy(0.0,-gravity);
}

// destructor
Projectile::~Projectile(){
    // Do nothing
}

// update velocity and coordinate
void Projectile::update(){
    
    // update coordinate
    cord += velocity * dt + 0.5 * acceleration * dt * dt;
    
    // update velocity
    velocity += acceleration * dt;
}

// get acceleration;
vector2d Projectile::get_acceleration(){
    return acceleration;
}

// get velocity;
vector2d Projectile::get_velocity(){
    return velocity;
}

// get_coordinate
vector2d Projectile::get_coordinate(){
    return cord;
}
