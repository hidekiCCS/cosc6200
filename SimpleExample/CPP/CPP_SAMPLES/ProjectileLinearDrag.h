//
//  ProjectileLinearDrag.h
//  ProjectileNoDrag
//
//  Created by Hideki Fujioka on 4/23/13.
//  Copyright (c) 2013 Tulane University. All rights reserved.
//

#ifndef ProjectileNoDrag_ProjectileLinearDrag_h
#define ProjectileNoDrag_ProjectileLinearDrag_h
#include "Projectile.h"

class ProjectileLinearDrag:public Projectile{
public:
    // constructor
    ProjectileLinearDrag(vector2d icord,vector2d ivel,double cdrag, double dt);

    // destructor
    ~ProjectileLinearDrag();
    
    // update accerelation, velocity and coordinate
    void update();

private:
    // update acceleration
    void update_acceleration();
    
    // drag coefficient
    double drag_coef;
};
#endif
