//
//  hello_mpi.cpp: display a message on the screen
//
#include <iostream>
#include <mpi.h>

int main (int argc, char *argv[]) {
  // Initialize   
  MPI_Init(&argc,&argv);
  
  // get myid and # of processors 
  int myid,numproc;
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  std::cout << "hello from " << myid << std::endl;
  
  // wait until all processors come here 
  MPI_Barrier (MPI_COMM_WORLD);

  if ( myid == 0 ) {
    // only myid = 0 do this
    std::cout << numproc << " processors said hello!" << std::endl;
  } 

  MPI_Finalize();   
  return 0;
}
