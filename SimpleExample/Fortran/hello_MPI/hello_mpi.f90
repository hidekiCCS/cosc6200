!hello_mpi.f90: display a message on the screen 
program helloworld
  include'mpif.h'
  !
  integer::myid, numproc, ierr
  ! Initialize  
  CALL MPI_INIT( ierr )  
  !
  ! get myid and # of processors 
  CALL MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
  CALL MPI_COMM_SIZE( MPI_COMM_WORLD, numproc, ierr )

  print *,"hello from ", myid

  ! wait until all processors come here 
  CALL MPI_Barrier (MPI_COMM_WORLD,ierr);
  
  if (myid == 0) then
     ! only myid = 0 do this 
     print *, numproc," processors said hello!"
  endif
  
  CALL  MPI_Finalize(ierr);
end program helloworld
