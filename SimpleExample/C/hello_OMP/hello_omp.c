/*
  hellp_omp.c: display a message on the screen
*/
#include <stdio.h>
#include <omp.h>

int main () {
  int id, nthreads;
  printf("C Start\n");
#pragma omp parallel private(id)
  {
    id = omp_get_thread_num();
    printf("hello from %d\n", id);
#pragma omp barrier
    if ( id == 0 ) {
      nthreads = omp_get_num_threads();
      printf("%d threads said hello!\n",nthreads);
    }
  }
  printf("End\n");
}
