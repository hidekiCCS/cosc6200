//
//  Large Scale Computing
//  MPI Sample ex48.cpp
//
#include <iostream>
#include <cmath>
#include <iomanip>
#include <mpi.h>

int main(int argc, char *argv[]) {
	// Initialize
	MPI_Init(&argc, &argv);

	// get myid and # of processors 
	int numproc;
	int myid;
	MPI_Comm_size(MPI_COMM_WORLD,&numproc);
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);

	/* set a value */
	int a;
	if (myid == 0) {
		a = 1234567;
	} else {
		a = 0;
	}

	std::cout << myid << " has a=" << a << std::endl;

	/* Send/Receive */
	MPI_Status status;
	if (myid == 0){
		MPI_Send( &a, 1, MPI_INT, 1, 21, MPI_COMM_WORLD);
	}
	if(myid == 1){
		MPI_Recv( &a, 1, MPI_INT, 0, 21, MPI_COMM_WORLD,&status);
	}

	// wait until all processors come here 
	MPI_Barrier(MPI_COMM_WORLD);
	std::cout << "Now " << myid << " has a=" << a << std::endl;

	MPI_Finalize();
	return 0;
}
