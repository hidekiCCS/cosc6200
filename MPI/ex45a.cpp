//
//  Large Scale Computing
//  MPI Sample ex45.cpp
//
#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[]) {
	// Initialize
	MPI_Init(&argc,&argv);

	// get myid and # of processors 
	int numproc;
	int myid;
	MPI_Comm_size(MPI_COMM_WORLD,&numproc);
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);

	int a = myid + 1;
	std::cout << "Process" << myid << " has a=" << a << std::endl;

	/* sum up */
	int sum;
	MPI_Reduce(&a,&sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	/* only rank0 do this */
	if (myid == 0) {
		std::cout << "End sum a=" << sum << std::endl;
	}
	MPI_Finalize();
	return 0;
}
