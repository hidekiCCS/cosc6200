//
//  Large Scale Computing
//  MPI Sample ex47.cpp
//
#include <iostream>
#include <cmath>
#include <iomanip>
#include <mpi.h>

int main(int argc, char *argv[]) {
	// Initialize
	MPI::Init(argc, argv);

	// get myid and # of processors 
	int numproc = MPI::COMM_WORLD.Get_size();
	int myid = MPI::COMM_WORLD.Get_rank();

	/* set a value */
	int a;
	if (myid == 0) {
		a = 1234567;
	} else {
		a = 0;
	}

	std::cout << myid << " has a=" << a << std::endl;

	/* Broadcast */
	MPI::COMM_WORLD.Bcast(&a, 1, MPI::INT, 0);

	std::cout << "Now " << myid << " has a=" << a << std::endl;

	MPI::Finalize();
	return 0;
}
