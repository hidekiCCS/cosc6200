//
//  Large Scale Computing
//  MPI Sample ex45.cpp
//
#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[]) {
	// Initialize
	MPI::Init(argc, argv);

	// get myid and # of processors 
	int numproc = MPI::COMM_WORLD.Get_size();
	int myid = MPI::COMM_WORLD.Get_rank();

	int a = myid + 1;
	std::cout << "Process" << myid << " has a=" << a << std::endl;

	/* sum up */
	int sum;
	MPI::COMM_WORLD.Reduce(&a, &sum, 1, MPI::INT, MPI::SUM, 0);

	/* only rank0 do this */
	if (myid == 0) {
		std::cout << "End sum a=" << sum << std::endl;
	}
	MPI::Finalize();
	return 0;
}
