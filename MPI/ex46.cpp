//
//  Large Scale Computing
//  MPI Sample ex46.cpp
//
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <mpi.h>

int main(int argc, char *argv[]) {
	// Initialize
	MPI::Init(argc, argv);

	// get myid and # of processors 
	int numproc = MPI::COMM_WORLD.Get_size();
	int myid = MPI::COMM_WORLD.Get_rank();

	if (argc < 2) {
		if (myid == 0) {
			std::cout << argv[0] << " [number of terms]\n";
		}
		MPI::Finalize();
		return 0;
	}

	/* get # of terms */
	int n = std::atoi(argv[1]);
	if (myid == 0) {
		std::cout << "Start n=" << n << std::endl;
	}

	/* divide loop */
	int mystart = (n / numproc) * myid;
	int myend;
	if (n % numproc > myid) {
		mystart += myid;
		myend = mystart + (n / numproc) + 1;
	} else {
		mystart += n % numproc;
		myend = mystart + (n / numproc);
	}
	std::cout << "CPU" << myid << ":" << mystart << "~" << myend << std::endl;

	/* compute PI */
	double x = 0.0;
	for (int i = mystart; i < myend; i++) {
		x += std::pow(-1.0, i) / (2 * i + 1);
	}

	/* sum up */
	double pi4 = 0.0;
	MPI::COMM_WORLD.Reduce(&x, &pi4, 1, MPI::DOUBLE, MPI::SUM, 0);

	if (myid == 0) {
		std::cout << std::setprecision(16);
		std::cout << std::scientific;
		std::cout << "Pi=" << 4.0 * pi4 << std::endl;
	}
	MPI::Finalize();
	return 0;
}
